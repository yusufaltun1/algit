package com.altnsft.algit.customerside.dto;

import com.altnsft.algit.models.dtos.MenuDTO;

import java.util.Date;

public class TakeAwayObject
{
    private long minMinutes;
    private MenuDTO menuItemDTO;
    private Date minSettableDate;

    public Date getMinSettableDate() {
        return minSettableDate;
    }

    public void setMinSettableDate(Date minSettableDate) {
        this.minSettableDate = minSettableDate;
    }

    public long getMinMinutes() {
        return minMinutes;
    }

    public void setMinMinutes(long minMinutes) {
        this.minMinutes = minMinutes;
    }

    public MenuDTO getMenuItemDTO() {
        return menuItemDTO;
    }

    public void setMenuItemDTO(MenuDTO menuItemDTO) {
        this.menuItemDTO = menuItemDTO;
    }
}
