package com.altnsft.algit.customerside.services;

import com.altnsft.algit.customerside.model.IyzicoErrorModel;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.HashMap;

/**
 * Created by yusufaltun on 15.09.2018.
 */
@Service
public class ErrorCodes
{
    private static final HashMap<String, IyzicoErrorModel> ERRORS = new HashMap<>();

    public IyzicoErrorModel getErrorModel(String code)
    {
        if(ERRORS.containsKey(code))
        {
            ERRORS.get(code);
        }

        return null;
    }

    @PostConstruct
    public void setup()
    {
        ERRORS.put("10051", new IyzicoErrorModel("10051", "Kart limiti yetersiz, yetersiz bakiye", "NOT_SUFFICIENT_FUNDS"));
        ERRORS.put("10005", new IyzicoErrorModel("10005", "İşlem onaylanmadı", ""));
        ERRORS.put("10012", new IyzicoErrorModel("10012", "Geçersiz işlem", ""));
        ERRORS.put("10041", new IyzicoErrorModel("10041", "Kayıp kart, karta el koyunuz", ""));
        ERRORS.put("10043", new IyzicoErrorModel("10043", "Çalıntı kart, karta el koyunuz", ""));
        ERRORS.put("10054", new IyzicoErrorModel("10054", "Vadesi dolmuş kart", ""));
        ERRORS.put("10084", new IyzicoErrorModel("10084", "CVC2 bilgisi hatalı", ""));
        ERRORS.put("10057", new IyzicoErrorModel("10057", "Kart sahibi bu işlemi yapamaz", ""));
        ERRORS.put("10058", new IyzicoErrorModel("10058", "Kart sahibi bu işlemi yapamaz", ""));
        ERRORS.put("10034", new IyzicoErrorModel("10034", "Terminalin bu işlemi yapmaya yetkisi yok", ""));
        ERRORS.put("10093", new IyzicoErrorModel("10093", "Dolandırıcılık şüphesi", ""));
        ERRORS.put("10201", new IyzicoErrorModel("10201", "Kartınız e-ticaret işlemlerine kapalıdır. Bankanızı arayınız.", ""));
        ERRORS.put("10202", new IyzicoErrorModel("10202", "Kart, işleme izin vermedi", ""));
        ERRORS.put("10203", new IyzicoErrorModel("10203", "Ödeme işlemi esnasında genel bir hata oluştu", ""));
        ERRORS.put("10204", new IyzicoErrorModel("10204", "Önceden onaylanan işlem", ""));
        ERRORS.put("10205", new IyzicoErrorModel("10205", "Ödeme işlemi esnasında genel bir hata oluştu", ""));
        ERRORS.put("10206", new IyzicoErrorModel("10206", "E-posta geçerli formata değil", ""));
        ERRORS.put("10207", new IyzicoErrorModel("10207", "CVC uzunluğu geçersiz", ""));
        ERRORS.put("10208", new IyzicoErrorModel("10208", "Bankanızdan onay alınız", ""));
        ERRORS.put("10209", new IyzicoErrorModel("10209", "Üye işyeri kategori kodu hatalı", ""));
        ERRORS.put("10210", new IyzicoErrorModel("10210", "Bloke statülü kart", ""));
        ERRORS.put("10211", new IyzicoErrorModel("10211", "Hatalı CAVV bilgisi", ""));
        ERRORS.put("10212", new IyzicoErrorModel("10212", "Hatalı ECI bilgisi", ""));
        ERRORS.put("10213", new IyzicoErrorModel("10213", "CVC2 yanlış girme deneme sayısı aşıldı", ""));
        ERRORS.put("10214", new IyzicoErrorModel("10214", "BIN bulunamadı", ""));
        ERRORS.put("10215", new IyzicoErrorModel("10215", "İletişim veya sistem hatası", ""));
        ERRORS.put("10216", new IyzicoErrorModel("10216", "Geçersiz kart numarası", ""));
        ERRORS.put("10217", new IyzicoErrorModel("10217", "Bankası bulunamadı", ""));
        ERRORS.put("10218", new IyzicoErrorModel("10218", "Banka kartları sadece 3D Secure işleminde kullanılabilir", ""));
        ERRORS.put("10219", new IyzicoErrorModel("10219", "Banka kartları ile taksit yapılamaz", ""));
        ERRORS.put("10220", new IyzicoErrorModel("10220", "Bankaya gönderilen istek zaman aşımına uğradı", ""));
        ERRORS.put("10221", new IyzicoErrorModel("10221", "Ödeme alınamadı", ""));
        ERRORS.put("10222", new IyzicoErrorModel("10222", "Terminal yurtdışı kartlara kapalı", ""));
        ERRORS.put("10223", new IyzicoErrorModel("10223", "Terminal taksitli işleme kapalı", ""));
        ERRORS.put("10224", new IyzicoErrorModel("10224", "Gün sonu yapılmalı", ""));
        ERRORS.put("10225", new IyzicoErrorModel("10225", "Para çekme limiti aşılmış", ""));
        ERRORS.put("10226", new IyzicoErrorModel("10226", "Kısıtlı kart", ""));
        ERRORS.put("10227", new IyzicoErrorModel("10227", "İzin verilen PIN giriş sayısı aşılmış", ""));
        ERRORS.put("10228", new IyzicoErrorModel("10228", "Geçersiz PIN", ""));
        ERRORS.put("10229", new IyzicoErrorModel("10229", "Banka veya terminal işlem yapamıyor", ""));
        ERRORS.put("10230", new IyzicoErrorModel("10230", "Son kullanma tarihi geçersiz", ""));
        ERRORS.put("10231", new IyzicoErrorModel("10231", "İstek bankadan hata aldı", ""));
        ERRORS.put("10232", new IyzicoErrorModel("10232", "Satış tutarı ödül puanından düşük olamaz", ""));
        ERRORS.put("10233", new IyzicoErrorModel("10233", "Satış tutarı ödül puanından düşük olamaz", ""));
        ERRORS.put("10234", new IyzicoErrorModel("10234", "Geçersiz tutar", ""));
        ERRORS.put("10235", new IyzicoErrorModel("10235", "Geçersiz kart tipi", ""));
        ERRORS.put("10236", new IyzicoErrorModel("10236", "Yetersiz ödül puanı", ""));
    }
}
