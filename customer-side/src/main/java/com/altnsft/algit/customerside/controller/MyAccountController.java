package com.altnsft.algit.customerside.controller;

import com.altnsft.algit.customerside.forms.EditAddressForm;
import com.altnsft.algit.customerside.services.AddressService;
import com.altnsft.algit.customerside.services.OrderService;
import com.altnsft.algit.models.dtos.*;
import com.altnsft.algit.payment.request.CreateCancelRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;

/**
 * Created by yusufaltun on 15.07.2018.
 */
@Controller
@RequestMapping("/my-account")
public class MyAccountController extends BaseController
{
    @Autowired
    private OrderService orderService;

    @Autowired
    private AddressService addressService;

    @GetMapping("/order-list")
    public String gotoOrderListingPage(Model model)
    {
        if(isGuest())
        {
            return "redirect:/404";
        }

        CustomerDTO customerDTO = getCustomer();
        List<OrderDTO> orders = orderService.getAllOrdersByCustomer(customerDTO);

        model.addAttribute("orders", orders);

        return "order-listing";
    }

    @GetMapping("/order-detail/{order-id}")
    public String gotoOrderDetailPage(@PathVariable(name = "order-id") long orderId, Model model)
    {
        if(isGuest())
        {
            return "redirect:/404";
        }

        OrderDTO order = orderService.getOrderById(orderId);

        model.addAttribute("order", order);

        return "order-detail";
    }

    @GetMapping("/create/cancel-request/{order-id}")
    public String createCancelRequest(@PathVariable(name = "order-id") long orderId, HttpServletRequest request)
    {
        CreateCancelRequestDTO createCancelRequestDTO = new CreateCancelRequestDTO();
        createCancelRequestDTO.setOrderId(orderId);

        orderService.createCancelRequest(createCancelRequestDTO);

        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

    @GetMapping("/edit-address")
    public String gotoAddressEditPage(Model model)
    {
        AddressDTO addressDTO = getCustomer().getAddressDTO();
        if(addressDTO != null)
        {
            EditAddressForm editAddressForm = new EditAddressForm();
            editAddressForm.setAddress(addressDTO.getAddress());
            editAddressForm.setCity(addressDTO.getCity());
            editAddressForm.setFirstName(addressDTO.getFirstName());
            editAddressForm.setLastName(addressDTO.getLastName());
            editAddressForm.setPhone(addressDTO.getPhone());
            editAddressForm.setPostalCode(addressDTO.getZipCode());

            model.addAttribute("address", editAddressForm);
        }
        else
        {
            model.addAttribute("address", new EditAddressForm());
        }

        return "edit-payment-address";
    }

    @PostMapping("/edit-address")
    public String gotoAddressEditPage(@ModelAttribute EditAddressForm address, HttpServletRequest request)
    {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setAddress(address.getAddress());
        addressDTO.setCity(address.getCity());
        addressDTO.setFirstName(address.getFirstName());
        addressDTO.setLastName(address.getLastName());
        addressDTO.setPhone(address.getPhone());
        addressDTO.setZipCode(address.getPostalCode());

        CreateAddressDTO createAddressDTO = new CreateAddressDTO();
        createAddressDTO.setCustomerId(getCustomer().getId());
        createAddressDTO.setAddressDTO(addressDTO);
        CustomerDTO customerDTO = addressService.createAddressService(createAddressDTO);

        setCustomer(customerDTO);

        String referer = request.getHeader("Referer");
        return "redirect:"+ referer;
    }

}
