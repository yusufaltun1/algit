package com.altnsft.algit.customerside.controller;

import com.altnsft.algit.customerside.forms.CommentForm;
import com.altnsft.algit.customerside.services.CartService;
import com.altnsft.algit.customerside.services.StoreService;
import com.altnsft.algit.models.dtos.AddMenuToCartDTO;
import com.altnsft.algit.models.dtos.StoreDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;

/**
 * Created by yusufaltun on 3.07.2018.
 */
@Controller
@RequestMapping("/store")
public class StoreDetailController extends BaseController
{
    @Autowired
    private StoreService storeService;
    @Autowired
    private CartService cartService;

    @GetMapping(value = "/{store-id}")
    public String gotoAddToCartPage(@PathVariable("store-id") int storeId, Model model, HttpSession session)
    {
        StoreDTO store = storeService.getStoreById(storeId);
        if(session.getAttribute("cart") == null)
        {
            if (!model.containsAttribute("cart"))
            {
//                CartDTO cart = cartService.getCart(132);
//                model.addAttribute("cart", cart);
//                session.setAttribute("cart", cart);
            }
            model.addAttribute("showPopup", false);
        }
        else
        {
            if(getCart().getStore().getStoreId() != storeId)
            {
                model.addAttribute("showPopup", true);
            }

            model.addAttribute("cart", getCart());
        }

        model.addAttribute("store", store);
        model.addAttribute("addMenuEntry", new AddMenuToCartDTO());
        return "add-to-cart";
    }

    @GetMapping(value = "/detail/{store-id}")
    public String gotoStoreDetailPage(@PathVariable("store-id") int storeId, Model model, HttpSession session)
    {
        StoreDTO store = storeService.getStoreById(storeId);
        if(session.getAttribute("cart") == null)
        {
            if (!model.containsAttribute("cart"))
            {
//
            }
        }
        else
        {
            model.addAttribute("cart", getCart());
        }

        model.addAttribute("showAddCommentButton", isRegistered());

        CommentForm form = new CommentForm();
        form.setStore(storeId);

        model.addAttribute("rateObject", form);
        model.addAttribute("store", store);
        return "store-detail";
    }

    @PostMapping(value = "/rate-store")
    public String addComment(@ModelAttribute("rateObject") CommentForm commentForm)
    {
        int id = getCustomer().getId();
        commentForm.setCustomer(id);

        storeService.addComment(commentForm);

        return "redirect:/store/detail/"+ commentForm.getStore();
    }

}
