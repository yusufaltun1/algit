package com.altnsft.algit.customerside.services;

import com.altnsft.algit.models.dtos.CreateAddressDTO;
import com.altnsft.algit.models.dtos.CustomerDTO;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * Created by yusufaltun on 16.09.2018.
 */
@Service
public class AddressService extends BaseService
{
    public CustomerDTO createAddressService(CreateAddressDTO createAddressDTO)
    {
        ResponseEntity<CustomerDTO> response = getRestTemplate().postForEntity(getBaseURL() +"/address/save", createAddressDTO, CustomerDTO.class);
        return response.getBody();
    }
}
