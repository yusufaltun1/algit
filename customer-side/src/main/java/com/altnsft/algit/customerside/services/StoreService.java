package com.altnsft.algit.customerside.services;

import com.altnsft.algit.customerside.forms.CommentForm;
import com.altnsft.algit.models.dtos.CommentDTO;
import com.altnsft.algit.models.dtos.GetStoresRequestData;
import com.altnsft.algit.models.dtos.StoreDTO;
import com.altnsft.algit.models.dtos.StoreWrapper;
import com.google.common.collect.Lists;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by yusufaltun on 3.07.2018.
 */
@Service
public class StoreService extends BaseService
{
    public StoreDTO getStoreById(int storeId)
    {
        ResponseEntity<StoreDTO> store = getRestTemplate().getForEntity(getBaseURL()+ "/store/"+ storeId, StoreDTO.class);

        return store.getBody();
    }

    public List<StoreDTO> getStoresByDistance(GetStoresRequestData data)
    {
        ResponseEntity<StoreWrapper> stores = getRestTemplate().postForEntity(getBaseURL()+ "/store/by/distance", data, StoreWrapper.class);

        return Lists.newArrayList(stores.getBody().getStores());
    }

    public void addComment(CommentForm form)
    {
        CommentDTO commentDTO = new CommentDTO();
        commentDTO.setMessage(form.getMessage());
        commentDTO.setCourtesyPoint(form.getCourtesyPoint());
        commentDTO.setCustomer(form.getCustomer());
        commentDTO.setFoodQualityPoint(form.getFoodQualityPoint());
        commentDTO.setPricePoint(form.getPricePoint());
        commentDTO.setPunctualityPoint(form.getPunctualityPoint());

        commentDTO.setStore(form.getStore());

        ResponseEntity<Void> response = getRestTemplate().postForEntity(getBaseURL()+ "/store/add-comment", commentDTO, Void.class);
    }
}
