package com.altnsft.algit.customerside.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by yusufaltun on 4.07.2018.
 */
@Controller
@RequestMapping("/")
public class IndexController
{
    @GetMapping
    public String gotoIndex()
    {
        return "index";
    }
}
