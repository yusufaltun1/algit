package com.altnsft.algit.customerside.exceptions;

import com.altnsft.algit.customerside.dto.TakeAwayObject;

/**
 * Created by yusufaltun on 14.07.2018.
 */
public class TakeAwayException extends RuntimeException
{
    private TakeAwayObject takeAwayObject;

    public TakeAwayException(TakeAwayObject takeAwayObject) {
        this.takeAwayObject = takeAwayObject;
    }

    public TakeAwayObject getTakeAwayObject()
    {
        return takeAwayObject;
    }
}
