package com.altnsft.algit.customerside.forms;

import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by yusufaltun on 7.07.2018.
 */
public class RegisterForm
{
    @NotNull
    @Size(min=3, max=30)
    private String name;
    @NotNull
    @Size(min=3, max=30)
    private String lastName;
    @NotNull
    @Size(min=3, max=30)
    private String password;

    @Email
    private String email;
    @NotNull
    @Size(min=3, max=30)
    private String repeatPassword;
    @NotNull
    @Size(min=3, max=30)
    private String phone;

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getRepeatPassword() {
        return repeatPassword;
    }

    public void setRepeatPassword(String repeatPassword) {
        this.repeatPassword = repeatPassword;
    }
}
