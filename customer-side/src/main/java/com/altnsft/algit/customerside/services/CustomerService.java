package com.altnsft.algit.customerside.services;

import com.altnsft.algit.customerside.forms.LoginForm;
import com.altnsft.algit.customerside.forms.RegisterForm;
import com.altnsft.algit.models.dtos.*;
import com.altnsft.algit.models.exception.CustomerAlreadyRegisteredException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.HttpServerErrorException;

import java.io.IOException;

/**
 * Created by yusufaltun on 7.07.2018.
 */
@Service
public class CustomerService extends BaseService
{
    public CustomerDTO createCustomer(RegisterForm registerForm)
    {
        CustomerDTO customerDTO = new CustomerDTO();
        customerDTO.setEmail(registerForm.getEmail());
        customerDTO.setName(registerForm.getName());
        customerDTO.setPhoneNumber(registerForm.getPhone());
        customerDTO.setLastName(registerForm.getLastName());
        customerDTO.setPassword(registerForm.getPassword());
        try
        {
            ResponseEntity<CustomerDTO> response = getRestTemplate().postForEntity(getBaseURL() +"/customer/create", customerDTO, CustomerDTO.class);
            return response.getBody();
        }
        catch (Exception e)
        {
            if(e instanceof HttpServerErrorException)
            {
                String body = ((HttpServerErrorException) e).getResponseBodyAsString();
                ObjectMapper mapper = new ObjectMapper();

                try {
                    ExceptionResponse obj = mapper.readValue(body, ExceptionResponse.class);
                    if(obj.getDetails().equals(MessageCodes.CUSTOMER_ALREADY_REGISTERED))
                    {
                        throw new CustomerAlreadyRegisteredException(registerForm.getEmail());
                    }
                } catch (IOException e1) {
                    e1.printStackTrace();
                }
            }
        }

        return null;
    }

    public CustomerDTO login(LoginForm loginForm)
    {
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setEmail(loginForm.getEmail());
        loginDTO.setPassword(loginForm.getPassword());

        ResponseEntity<CustomerDTO> response = getRestTemplate().postForEntity(getBaseURL() +"/customer/login", loginDTO, CustomerDTO.class);
        return response.getBody();
    }
}
