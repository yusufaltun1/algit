package com.altnsft.algit.customerside.services;

import com.altnsft.algit.models.dtos.FilteredCategoriesWrapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

/**
 * Created by yusufaltun on 20.08.2018.
 */
@Service
public class FilterService extends BaseService
{
    public FilteredCategoriesWrapper getAllCategories()
    {
        ResponseEntity<FilteredCategoriesWrapper> response = getRestTemplate().getForEntity(getBaseURL()+ "/filter/all-category", FilteredCategoriesWrapper.class);

        return response.getBody();
    }
}
