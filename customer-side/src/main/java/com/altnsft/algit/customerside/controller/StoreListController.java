package com.altnsft.algit.customerside.controller;

import com.altnsft.algit.customerside.services.FilterService;
import com.altnsft.algit.customerside.services.StoreService;
import com.altnsft.algit.models.dtos.FilteredCategoriesWrapper;
import com.altnsft.algit.models.dtos.GetStoresRequestData;
import com.altnsft.algit.models.dtos.StoreDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by yusufaltun on 6.07.2018.
 */
@Controller
@RequestMapping("/store-list")
public class StoreListController extends BaseController
{

    @Autowired
    private StoreService storeService;

    @Autowired
    private FilterService filterService;
    @GetMapping
    public String listStores(Model model)
    {
        FilteredCategoriesWrapper wrapper = filterService.getAllCategories();
        model.addAttribute("categories", wrapper.getCategories());

        return "list-stores-on-map";
    }

    @GetMapping("/my/near/{lat}/{lng}/{min}/{max}/{selectedCategories}")
    @ResponseBody
    public List<StoreDTO> listStores(@PathVariable float lat, @PathVariable float lng, @PathVariable long min, @PathVariable long max, @PathVariable String selectedCategories)
    {
        GetStoresRequestData requestData = new GetStoresRequestData();
        requestData.setMin(min);
        requestData.setMax(max);
        requestData.setLat(lat);
        requestData.setLng(lng);
        requestData.setSelectedCategories(selectedCategories);

        return storeService.getStoresByDistance(requestData);
    }
}
