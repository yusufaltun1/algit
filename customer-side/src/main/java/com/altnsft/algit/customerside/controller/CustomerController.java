package com.altnsft.algit.customerside.controller;

import com.altnsft.algit.customerside.forms.LoginForm;
import com.altnsft.algit.customerside.forms.RegisterForm;
import com.altnsft.algit.customerside.services.CartService;
import com.altnsft.algit.customerside.services.CustomerService;
import com.altnsft.algit.models.dtos.CartDTO;
import com.altnsft.algit.models.dtos.CustomerDTO;
import com.altnsft.algit.models.exception.CustomerAlreadyRegisteredException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpSession;
import javax.validation.Valid;

/**
 * Created by yusufaltun on 7.07.2018.
 */
@Controller
@RequestMapping("/customer")
public class CustomerController extends BaseController
{
    @Autowired
    private CustomerService customerService;

    @Autowired
    private CartService cartService;

    @PostMapping("/register")
    public String doRegister(@ModelAttribute("register") @Valid RegisterForm registerForm, BindingResult bindingResult, HttpSession session)
    {
        if(bindingResult.hasErrors())
        {
            return "register-page";
        }

        if(!registerForm.getPassword().equals(registerForm.getRepeatPassword()))
        {
            bindingResult.addError(new ObjectError("repeatPassword", "Sifre tekrari sifre ile ayni olmalidir"));
            return "register-page";
        }

        try
        {
            CustomerDTO customerDTO = customerService.createCustomer(registerForm);
            setCustomer(customerDTO);

            CartDTO cart = getCart();
            if(cart != null)
            {
                cart = cartService.setUserToCart(customerDTO, cart);
                setCart(cart);
            }
        }
        catch (CustomerAlreadyRegisteredException e)
        {
            bindingResult.addError(new ObjectError("email", "Bu email sisteme kayitlidir"));
        }

        return "register-page";
    }

    @GetMapping("/login")
    public String loginPage( Model model)
    {
        LoginForm loginForm = new LoginForm();

        Object redirectUrl = model.asMap().get("redirectUrl");
        if(redirectUrl != null)
        {
            loginForm.setRedirectUrl((String )redirectUrl);
        }

        model.addAttribute("login", loginForm);
        return "login-page";
    }

    @PostMapping("/login")
    public String doLogin(@ModelAttribute("login") @Valid LoginForm loginForm, Model model)
    {
        CustomerDTO customerDTO = customerService.login(loginForm);
        setCustomer(customerDTO);

        CartDTO cart = getCart();
        if(cart != null)
        {
            cart = cartService.setUserToCart(customerDTO, cart);
            setCart(cart);
        }
        if (loginForm.getRedirectUrl() == null || loginForm.getRedirectUrl().equals(""))
        {
            return "redirect:/";
        }
        else
        {
            return "redirect:"+ loginForm.getRedirectUrl();
        }

    }

    @GetMapping("/register")
    public String openRegisterPage(Model model)
    {
        model.addAttribute("register", new RegisterForm());
        return "register-page";
    }
}
