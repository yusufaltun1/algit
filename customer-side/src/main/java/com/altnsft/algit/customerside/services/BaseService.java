package com.altnsft.algit.customerside.services;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

/**
 * Created by yusufaltun on 3.07.2018.
 */
public class BaseService
{
    private RestTemplate restTemplate;

    @Value("${algit.webservice.baseurl}")
    private String baseURL;

    public BaseService ()
    {
        restTemplate = new RestTemplate();
    }

    protected RestTemplate getRestTemplate()
    {
        return restTemplate;
    }

    protected String getBaseURL()
    {
        return baseURL;
    }

}
