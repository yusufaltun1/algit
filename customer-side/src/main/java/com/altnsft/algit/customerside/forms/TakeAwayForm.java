package com.altnsft.algit.customerside.forms;


/**
 * Created by yusufaltun on 9.07.2018.
 */
public class TakeAwayForm
{
    private String  date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }
}
