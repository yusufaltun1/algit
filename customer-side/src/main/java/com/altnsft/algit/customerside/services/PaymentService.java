package com.altnsft.algit.customerside.services;

import com.altnsft.algit.customerside.exceptions.PaymentException;
import com.altnsft.algit.customerside.forms.PaymentForm;
import com.altnsft.algit.models.dtos.*;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.model.*;
import com.altnsft.algit.payment.request.CreatePaymentRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by yusufaltun on 12.09.2018.
 */
@Service
public class PaymentService extends BaseService
{
    public PaymentTransactionDTO checkout(CartDTO cartDTO, CustomerDTO customerDTO, PaymentForm paymentForm)
    {
        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

        AddressDTO addressDTO = customerDTO.getAddressDTO();

        Options options = new Options();

        options.setApiKey("sandbox-KCw1P0nst2aAhDw0lmeggWRYWyWcNZDH");
        options.setSecretKey("sandbox-wHgzDKuYkT5XMWjY00IALBpn6tHI7gFz");
        options.setBaseUrl("https://sandbox-api.iyzipay.com");

        CreatePaymentRequest request = new CreatePaymentRequest();
        request.setLocale(Locale.TR.getValue());
        request.setConversationId(String.valueOf(cartDTO.getId()));
        request.setPrice(new BigDecimal(cartDTO.getTotalPrice()));
        request.setPaidPrice(new BigDecimal(cartDTO.getTotalPrice()));
        request.setCurrency(Currency.TRY.name());
        request.setInstallment(1);
        request.setBasketId(String.valueOf(cartDTO.getId()));
        request.setPaymentChannel(PaymentChannel.WEB.name());
        request.setPaymentGroup(PaymentGroup.PRODUCT.name());

        PaymentCard paymentCard = new PaymentCard();
        paymentCard.setCardHolderName(paymentForm.getName());
        paymentCard.setCardNumber(paymentForm.getCardNumber());
        paymentCard.setExpireMonth(paymentForm.getExpireMonth());
        paymentCard.setExpireYear(paymentForm.getExpireYear());
        paymentCard.setCvc(paymentForm.getCcv());
        paymentCard.setRegisterCard(0);
        request.setPaymentCard(paymentCard);

        Buyer buyer = new Buyer();
        buyer.setId(String.valueOf(cartDTO.getCustomerDTO().getId()));
        buyer.setName(customerDTO.getName());
        buyer.setSurname(customerDTO.getLastName());
        buyer.setGsmNumber(customerDTO.getPhoneNumber());
        buyer.setEmail(customerDTO.getEmail());
        buyer.setIdentityNumber("74300864791");
        buyer.setLastLoginDate(dateFormat.format(customerDTO.getLastLoginDate()));

        buyer.setRegistrationDate(dateFormat.format(customerDTO.getRegistrationDate()));
        buyer.setRegistrationAddress(addressDTO.getAddress());
        buyer.setIp("85.34.78.112");
        buyer.setCity(addressDTO.getCity());
        buyer.setCountry("Turkey");
        buyer.setZipCode(addressDTO.getZipCode());
        request.setBuyer(buyer);

        Address shippingAddress = new Address();
        shippingAddress.setContactName(addressDTO.getFirstName() + " "+ addressDTO.getLastName());
        shippingAddress.setCity(addressDTO.getCity());
        shippingAddress.setCountry("Turkey");
        shippingAddress.setAddress(addressDTO.getAddress());
        shippingAddress.setZipCode(addressDTO.getZipCode());
        request.setShippingAddress(shippingAddress);

        Address billingAddress = new Address();
        billingAddress.setContactName(addressDTO.getFirstName() + " "+ addressDTO.getLastName());
        billingAddress.setCity(addressDTO.getCity());
        billingAddress.setCountry("Turkey");
        billingAddress.setAddress(addressDTO.getAddress());
        billingAddress.setZipCode(addressDTO.getZipCode());
        request.setBillingAddress(billingAddress);

        List<BasketItem> basketItems = new ArrayList<BasketItem>();

        for (CartEntryDTO cartEntryDTO : cartDTO.getCartEntries())
        {
            BasketItem firstBasketItem = new BasketItem();
            firstBasketItem.setId(String.valueOf(cartEntryDTO.getId()));
            firstBasketItem.setName(cartEntryDTO.getMenuDTO().getName());
            firstBasketItem.setCategory1(cartEntryDTO.getMenuDTO().getCategoryName());
            firstBasketItem.setCategory2("");
            firstBasketItem.setItemType(BasketItemType.PHYSICAL.name());
            firstBasketItem.setPrice(new BigDecimal(cartEntryDTO.getPrice()));
            basketItems.add(firstBasketItem);
        }

        request.setBasketItems(basketItems);

        Payment payment = Payment.create(request, options);

        if(Status.SUCCESS.getValue().equals(payment.getStatus()))
        {
            PaymentTransactionDTO dto = new PaymentTransactionDTO();
            dto.setConversationId(payment.getConversationId());
            dto.setErrorCode(payment.getErrorCode());
            dto.setErrorMessage(payment.getErrorMessage());
            dto.setLocale(payment.getLocale());
            dto.setSystemTime(new Date(payment.getSystemTime()));
            dto.setStatus(payment.getStatus());
            dto.setPaymentId(payment.getPaymentId());

            return dto;
        }
        else
        {
            PaymentException paymentException = new PaymentException();
            throw paymentException;
        }
    }

    public void createPaymentTransaction(PaymentTransactionDTO paymentTransactionDTO)
    {
        ResponseEntity<Void> response = getRestTemplate().postForEntity(getBaseURL() +"/payment-transaction/create", paymentTransactionDTO,  Void.class);
    }
}
