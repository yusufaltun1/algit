package com.altnsft.algit.customerside.controller;

import com.altnsft.algit.customerside.forms.RegisterForm;
import com.altnsft.algit.models.dtos.CartDTO;
import com.altnsft.algit.models.dtos.CustomerDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpSession;

/**
 * Created by yusufaltun on 7.07.2018.
 */
public class BaseController
{
    protected void setupPage(Model model)
    {
        model.addAttribute("register", new RegisterForm());
    }

    protected boolean isRegistered()
    {
        return !isGuest();
    }

    protected boolean isGuest()
    {
        CustomerDTO customer = getCustomer();
        if(customer == null)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    protected CustomerDTO getCustomer()
    {
        return (CustomerDTO) getSession().getAttribute("customer");
    }


    protected CartDTO getCart()
    {
        return (CartDTO) getSession().getAttribute("cart");
    }

    protected void setCustomer(CustomerDTO customer)
    {
        getSession().setAttribute("customer", customer);
    }
    protected void setCart(CartDTO cart)
    {
        getSession().setAttribute("cart", cart);
    }

    protected void removeCart()
    {
        getSession().removeAttribute("cart");
    }

    protected HttpSession getSession(){
        ServletRequestAttributes attr = (ServletRequestAttributes) RequestContextHolder.currentRequestAttributes();
        HttpSession session = attr.getRequest().getSession();
        return session;
    }
}
