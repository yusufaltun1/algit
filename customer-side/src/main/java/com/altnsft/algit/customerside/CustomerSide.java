package com.altnsft.algit.customerside;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan({"com.altnsft.algit.customerside.controller", "com.altnsft.algit.customerside.services",  "com.altnsft.algit.customerside.security"})
public class CustomerSide {

	public static void main(String[] args)
	{
		SpringApplication.run(CustomerSide.class, args);
	}
}
