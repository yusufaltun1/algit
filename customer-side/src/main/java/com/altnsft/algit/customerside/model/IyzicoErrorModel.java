package com.altnsft.algit.customerside.model;

/**
 * Created by yusufaltun on 15.09.2018.
 */
public class IyzicoErrorModel
{
    private String errorCode;
    private String errorMessage;
    private String errorGroup;

    public IyzicoErrorModel(String errorCode, String errorMessage, String errorGroup) {
        this.errorCode = errorCode;
        this.errorMessage = errorMessage;
        this.errorGroup = errorGroup;
    }

    public String getErrorCode() {
        return errorCode;
    }

    public void setErrorCode(String errorCode) {
        this.errorCode = errorCode;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorGroup() {
        return errorGroup;
    }

    public void setErrorGroup(String errorGroup) {
        this.errorGroup = errorGroup;
    }
}
