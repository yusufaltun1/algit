package com.altnsft.algit.customerside.controller;

import com.altnsft.algit.customerside.dto.TakeAwayObject;
import com.altnsft.algit.customerside.exceptions.PaymentException;
import com.altnsft.algit.customerside.exceptions.TakeAwayException;
import com.altnsft.algit.customerside.forms.PaymentForm;
import com.altnsft.algit.customerside.forms.TakeAwayForm;
import com.altnsft.algit.customerside.services.CartService;
import com.altnsft.algit.customerside.services.PaymentService;
import com.altnsft.algit.models.dtos.CartDTO;
import com.altnsft.algit.models.dtos.OrderDTO;
import com.altnsft.algit.models.dtos.PaymentMethod;
import com.altnsft.algit.models.dtos.PaymentTransactionDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.text.ParseException;

/**
 * Created by yusufaltun on 8.07.2018.
 */
@Controller
@RequestMapping("/payment")
public class PaymentPageController extends BaseController
{
    @Autowired
    private CartService cartService;

    @Autowired
    private PaymentService paymentService;

    @GetMapping
    public String goPaymentPage(Model model)
    {
        model.addAttribute("cart", getCart());
        model.addAttribute("paymentForm", new PaymentForm());

        return "payment-page";
    }

    @PostMapping
    public String doPayment(Model model)
    {
        model.addAttribute("cart", getCart());
        return "payment-page";
    }

    @GetMapping("/take-away")
    public String gotoTakeAwayPage(Model model)
    {
        model.addAttribute("cart", getCart());
        model.addAttribute("takeAwayForm", new TakeAwayForm());
        return "take-away";
    }

    @PostMapping("/set-take-away")
    public String setTakeAway(@ModelAttribute("takeAwayForm") @Valid TakeAwayForm takeAwayForm, Model model, BindingResult bindingResult, HttpServletRequest req, RedirectAttributes redirectAttributes) throws ParseException
    {
        try
        {
            CartDTO cartDTO = cartService.setTakeAwayTime(takeAwayForm, getCart());
            setCart(cartDTO);

            if(isGuest())
            {
                redirectAttributes.addFlashAttribute("redirectUrl", "/payment");
                return "redirect:/customer/login";
            }
            else
            {
                return "redirect:/payment";
            }
        }
        catch (TakeAwayException e)
        {
            TakeAwayObject takeAwayObject = e.getTakeAwayObject();
            model.addAttribute("cart", getCart());
            model.addAttribute("takeAwayForm", new TakeAwayForm());
            redirectAttributes.addFlashAttribute("showPopup", true);
            return "redirect:/payment/take-away";
        }
    }

    @PostMapping("/checkout/credit-card")
    public String checkout(@ModelAttribute("paymentForm") @Valid PaymentForm paymentForm, Model model, RedirectAttributes redirectAttributes) throws ParseException
    {
        try
        {
            PaymentTransactionDTO paymentTransactionDTO = paymentService.checkout(getCart(), getCustomer(), paymentForm);
            OrderDTO dto = cartService.checkout(getCart(), getCustomer(), paymentTransactionDTO, PaymentMethod.CARD);
            getSession().removeAttribute("cart");

            redirectAttributes.addFlashAttribute("order", dto);
            return "redirect:/payment/confirmation";
        }catch (PaymentException e)
        {
            return "redirect:/payment";
        }
    }

    @PostMapping("/checkout/cash")
    public String checkout(Model model, RedirectAttributes redirectAttributes) throws ParseException
    {
        OrderDTO dto = cartService.checkout(getCart(), getCustomer(), null, PaymentMethod.CASH);
        getSession().removeAttribute("cart");

        redirectAttributes.addFlashAttribute("order", dto);
        return "redirect:/payment/confirmation";
    }

    @GetMapping("/confirmation")
    public String orderConfirmationPage()
    {
        return "order-confirmation";
    }
}
