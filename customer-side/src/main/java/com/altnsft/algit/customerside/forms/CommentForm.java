package com.altnsft.algit.customerside.forms;


/**
 * Created by yusufaltun on 29.07.2018.
 */
public class CommentForm
{
    private int store;

    private int customer;

    private int foodQualityPoint;

    private int pricePoint;

    private int punctualityPoint;

    private int courtesyPoint;

    private String message;

    public int getStore() {
        return store;
    }

    public void setStore(int store) {
        this.store = store;
    }

    public int getCustomer() {
        return customer;
    }

    public void setCustomer(int customer) {
        this.customer = customer;
    }

    public int getFoodQualityPoint() {
        return foodQualityPoint;
    }

    public void setFoodQualityPoint(int foodQualityPoint) {
        this.foodQualityPoint = foodQualityPoint;
    }

    public int getPricePoint() {
        return pricePoint;
    }

    public void setPricePoint(int pricePoint) {
        this.pricePoint = pricePoint;
    }

    public int getPunctualityPoint() {
        return punctualityPoint;
    }

    public void setPunctualityPoint(int punctualityPoint) {
        this.punctualityPoint = punctualityPoint;
    }

    public int getCourtesyPoint() {
        return courtesyPoint;
    }

    public void setCourtesyPoint(int courtesyPoint) {
        this.courtesyPoint = courtesyPoint;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
