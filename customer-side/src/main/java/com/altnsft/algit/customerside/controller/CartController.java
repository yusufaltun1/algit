package com.altnsft.algit.customerside.controller;

import com.altnsft.algit.customerside.forms.AddMenuForm;
import com.altnsft.algit.customerside.services.CartService;
import com.altnsft.algit.models.dtos.AddMenuToCartDTO;
import com.altnsft.algit.models.dtos.CartDTO;
import com.altnsft.algit.models.dtos.SelectedOptionDTO;
import com.altnsft.algit.models.dtos.SelectedOptionValuDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by yusufaltun on 3.07.2018.
 */
@Controller
@RequestMapping("/carts")
public class CartController extends BaseController
{
    @Autowired
    private CartService cartService;


    @GetMapping(value = "/remove-entry/{entry-id}")
    public String removeEntry(@PathVariable("entry-id") int entryId, HttpServletRequest request, HttpSession session)
    {
        String referer = request.getHeader("Referer");
        CartDTO cartDTO = getCart();
        cartDTO = cartService.removeEntry(entryId, cartDTO.getId());

        if(cartDTO.getCartEntries().size() > 0)
        {
            session.setAttribute("cart", cartDTO);
        }
        else{
            setCart(null);
        }
        return "redirect:"+ referer;
    }

    @GetMapping(value = "/delete")
    public String removeCart(HttpServletRequest request, HttpSession session)
    {
        String referer = request.getHeader("Referer");
        CartDTO cartDTO = getCart();

        this.removeCart();

        session.removeAttribute("cart");
        cartService.removeCart(cartDTO.getId());
        return "redirect:"+ referer;
    }

    @RequestMapping(method = RequestMethod.POST, consumes = MediaType.APPLICATION_FORM_URLENCODED_VALUE, headers = {"content-type=application/x-www-form-urlencoded"})
    public String addMenu(HttpServletRequest request,  HttpSession session)
    {
        CartDTO cartDTO = null;
        String storeId = request.getParameterMap().get("store")[0];
        if(getCart() == null)
        {
            if(isGuest())
            {
                cartDTO = cartService.createCartForGuest(Long.valueOf(storeId));
                session.setAttribute("cart", cartDTO);
            }
            else
            {
                cartDTO = cartService.createCartForCustomer(getCustomer(), Long.valueOf(storeId));
            }
        }
        else
        {
            cartDTO = (CartDTO) session.getAttribute("cart");
        }

        HashMap<Integer, List<Integer>> options = getOptionIndexes(request);
        AddMenuToCartDTO addMenuToCartDTO = new AddMenuToCartDTO();
        String menuId = request.getParameterMap().get("menuId")[0];
        String quantity = request.getParameterMap().get("quantity")[0];

        addMenuToCartDTO.setCartId(cartDTO.getId());
        addMenuToCartDTO.setMenuId(Long.valueOf(menuId));
        addMenuToCartDTO.setQuantity(Long.valueOf(quantity));
        addMenuToCartDTO.setSelectedOptions(new ArrayList<>());

        for (Integer integer : options.keySet())
        {
            String optionIdKey = request.getParameterMap().get("selectedOptions["+ integer +"].optionId")[0];

            SelectedOptionDTO selectedOptionDTO = new SelectedOptionDTO();
            selectedOptionDTO.setOptionId(Long.valueOf(optionIdKey));
            selectedOptionDTO.setSelectedValues(new ArrayList<>());

            List<Integer> valuesKeys = options.get(integer);
            for (Integer valuesKey : valuesKeys) {
                String valueId = request.getParameterMap().get("selectedOptions["+ integer +"].selectedValues["+ valuesKey +"].selectedValue")[0];
                SelectedOptionValuDTO selectedOptionValuDTO = new SelectedOptionValuDTO();
                selectedOptionValuDTO.setSelectedValue(Long.valueOf(valueId));
                selectedOptionDTO.getSelectedValues().add(selectedOptionValuDTO);
            }

            addMenuToCartDTO.getSelectedOptions().add(selectedOptionDTO);
        }

        cartDTO = cartService.addMenuEntry(addMenuToCartDTO);

        String referer = request.getHeader("Referer");

        session.setAttribute("cart", cartDTO);
        //cartService.removeEntry(entryId);
        return "redirect:"+ referer;
    }

    private HashMap<Integer, List<Integer>> getOptionIndexes(HttpServletRequest request)
    {
        HashMap<Integer, List<Integer>> options = new HashMap<>();

        for (String key : request.getParameterMap().keySet())
        {
            if(key.contains(".selectedValue"))
            {
                Pattern p = Pattern.compile("-?\\d+");
                Matcher m = p.matcher(key);

                int count = 0;
                int optionIndex = 0;
                int valueIndex = 0;
                while (m.find())
                {
                    if(count == 0)
                    {
                        optionIndex = Integer.valueOf(m.group());
                    }
                    else
                    {
                        valueIndex = Integer.valueOf(m.group());
                    }
                    count ++;
                }

                if(options.containsKey(optionIndex))
                {
                    options.get(optionIndex).add(valueIndex);
                }
                else
                {
                    options.put(optionIndex, new ArrayList<>());
                    options.get(optionIndex).add(valueIndex);
                }
            }
        }

        return options;
    }
}
