package com.altnsft.algit.customerside.services;

import com.altnsft.algit.customerside.dto.TakeAwayObject;
import com.altnsft.algit.customerside.exceptions.TakeAwayException;
import com.altnsft.algit.customerside.forms.TakeAwayForm;
import com.altnsft.algit.models.dtos.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.temporal.TemporalField;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by yusufaltun on 3.07.2018.
 */
@Service
public class CartService extends BaseService
{
    static final long ONE_MINUTE_IN_MILLIS=60000;//millisecs

    public CartDTO getCart(int cartId)
    {
        ResponseEntity<CartDTO> cart = getRestTemplate().getForEntity(getBaseURL() +"/carts/"+ cartId, CartDTO.class);

        return cart.getBody();
    }

    public CartDTO removeEntry(int entryId, long cartId)
    {
        RemoveCartEntryDTO removeCartEntry = new RemoveCartEntryDTO();
        removeCartEntry.setCartEntryId(Long.valueOf(entryId));
        removeCartEntry.setCartId(cartId);
        ResponseEntity<CartDTO> cart = getRestTemplate().postForEntity(getBaseURL() +"/carts/remove-cart-entry", new HttpEntity<RemoveCartEntryDTO>(removeCartEntry), CartDTO.class);

        return cart.getBody();
    }

    public CartDTO addMenuEntry(AddMenuToCartDTO addMenuToCartDTO)
    {
        ResponseEntity<CartDTO> cart = getRestTemplate().postForEntity(getBaseURL() +"/carts/add-menu", new HttpEntity<AddMenuToCartDTO>(addMenuToCartDTO), CartDTO.class);
        return cart.getBody();
    }

    public CartDTO createCartForGuest(long storeId)
    {
        ResponseEntity<CartDTO> cart = getRestTemplate().postForEntity(getBaseURL() +"/carts/create/guest/"+ storeId, null, CartDTO.class);
        return cart.getBody();
    }

    public CartDTO createCartForCustomer(CustomerDTO customer, long storeId)
    {
        ResponseEntity<CartDTO> cart = getRestTemplate().postForEntity(getBaseURL() +"/carts/"+ customer.getId() +"/create/"+ storeId, null, CartDTO.class);
        return cart.getBody();
    }

    public CartDTO setUserToCart(CustomerDTO customer, CartDTO cart)
    {
        SetUserToCartDTO update = new SetUserToCartDTO();
        update.setCartId(cart.getId());
        update.setUserId(customer.getId());

        ResponseEntity<CartDTO> response = getRestTemplate().postForEntity(getBaseURL() +"/carts/set-user", update, CartDTO.class);
        return response.getBody();
    }

    public CartDTO setTakeAwayTime(@Valid TakeAwayForm takeAwayTime, CartDTO cart) throws ParseException {
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        Date takeAwayDate = formatter.parse(takeAwayTime.getDate().replace("T", " "));
        TakeAwayObject object = calculateMinDate(cart);

        if(takeAwayDate.getTime() < object.getMinSettableDate().getTime())
        {
            throw new TakeAwayException(object);
        }

        SetTakeAwayTimeDTO setTakeAwayTimeDTO = new SetTakeAwayTimeDTO();
        setTakeAwayTimeDTO.setCartId(cart.getId());
        setTakeAwayTimeDTO.setTakeAwayTime(takeAwayDate);
        ResponseEntity<CartDTO> response = getRestTemplate().postForEntity(getBaseURL() +"/carts/set-take-away-time", setTakeAwayTimeDTO, CartDTO.class);

        return response.getBody();
    }

    private TakeAwayObject calculateMinDate(CartDTO cart)
    {
        TakeAwayObject takeAwayObject = new TakeAwayObject();
        long maxDate = 0l;

        for (CartEntryDTO cartEntryDTO : cart.getCartEntries()) {
            if(cartEntryDTO.getMenuDTO().getMaxMakeTime() > maxDate)
            {
                maxDate = cartEntryDTO.getMenuDTO().getMaxMakeTime();
                takeAwayObject.setMenuItemDTO(cartEntryDTO.getMenuDTO());
            }
        }

        takeAwayObject.setMinMinutes(maxDate);



        Calendar date = Calendar.getInstance();
        long t = date.getTimeInMillis();
        Date afterAddingTenMins = new Date(t + (maxDate * ONE_MINUTE_IN_MILLIS));

        takeAwayObject.setMinSettableDate(afterAddingTenMins);

        return takeAwayObject;
    }

    public void removeCart(long cartId)
    {
        RemoveCartDTO removeCartDTO = new RemoveCartDTO();
        removeCartDTO.setCartId(cartId);

        ResponseEntity<Void> response = getRestTemplate().postForEntity(getBaseURL() +"/carts/remove", removeCartDTO, Void.class);
    }

    public OrderDTO checkout(CartDTO cart, CustomerDTO customer, PaymentTransactionDTO paymentTransactionDTO, PaymentMethod paymentMethod) {

        CreateOrderDTO createOrderDTO = new CreateOrderDTO();
        createOrderDTO.setCartId(cart.getId());
        createOrderDTO.setPaymentMethod(paymentMethod);
        createOrderDTO.setPaymentTransactionDTO(paymentTransactionDTO);
        ResponseEntity<OrderDTO> response = getRestTemplate().postForEntity(getBaseURL() +"/order/create", createOrderDTO, OrderDTO.class);
        return response.getBody();
    }
}
