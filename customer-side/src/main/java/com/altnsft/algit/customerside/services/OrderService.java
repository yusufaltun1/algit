package com.altnsft.algit.customerside.services;

import com.altnsft.algit.models.dtos.CreateCancelRequestDTO;
import com.altnsft.algit.models.dtos.CustomerDTO;
import com.altnsft.algit.models.dtos.OrderDTO;
import com.altnsft.algit.models.dtos.OrderWrapper;
import com.altnsft.algit.payment.request.CreateCancelRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by yusufaltun on 15.07.2018.
 */
@Service
public class OrderService extends BaseService
{
    public List<OrderDTO> getAllOrdersByCustomer(CustomerDTO customerDTO)
    {
        ResponseEntity<OrderWrapper> response = getRestTemplate().getForEntity(getBaseURL() +"/order/"+ customerDTO.getId(), OrderWrapper.class);
        return response.getBody().getOrders();
    }

    public OrderDTO getOrderById(long orderId)
    {
        ResponseEntity<OrderDTO> response = getRestTemplate().getForEntity(getBaseURL() +"/order/order-detail/"+ orderId, OrderDTO.class);
        return response.getBody();
    }

    public void createCancelRequest(CreateCancelRequestDTO createCancelRequestDTO)
    {
        ResponseEntity<Void> response = getRestTemplate().postForEntity(getBaseURL() +"/order/create/cancel-request", createCancelRequestDTO, Void.class);
    }
}
