$( document ).ready(function() {
     var html = $('#warning-popup').html();
     var autoclosable =  $('#warning-popup').data('autoclosable');

    //var autoclosable =  $('#autoclosable-element').val();
     var popup = new $.popup({
         title       : 'Uyari',
         content         :  html,
         html: true,
         autoclose   : false,
         closeOverlay:autoclosable,
         closeEsc: autoclosable,
         buttons     : false
     });
 });
