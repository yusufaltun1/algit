// (function (A) {
//
//     if (!Array.prototype.forEach)
//         A.forEach = A.forEach || function (action, that) {
//                 for (var i = 0, l = this.length; i < l; i++)
//                     if (i in this)
//                         action.call(that, this[i], i, this);
//             };
//
// })(Array.prototype);
// var map = new google.maps.Map(
//     document.getElementById('map_listing'),
//     {
//         center: {
//             lat: 40.078815,
//             lng: 28.988600
//         }
//     }
// );
// // Create a new directionsService object.


var map, infoWindow;
var pos;
var directionsDisplay = null;

/**
 * Created by yusufaltun on 23.06.2018.
 */
$(document).ready(function(){

    $('#driving-mode').on('click', function () {
        updateDirections('DRIVING');
    });

    $('#bicycling-mode').on('click', function () {
        updateDirections('BICYCLING');
    });

    $('#transit-mode').on('click', function () {
        updateDirections('TRANSIT');
    });

    $('#walking-mode').on('click', function () {
        updateDirections('WALKING');
    });

    var s = document.createElement("script");
    s.type = "text/javascript";
    s.src  = "https://maps.googleapis.com/maps/api/js?key=AIzaSyB_31_zbWz5kMAY6ai-oG2pCfNtT089TLo&callback=gmap_draw";
    window.gmap_draw = function()
    {
        if (navigator.geolocation)
        {
            navigator.geolocation.getCurrentPosition(function(position)
            {
                pos = {
                    lat: position.coords.latitude,
                    lng: position.coords.longitude
                };

                focusMap();
                fillInfo();
            });
        }
    };
    $("#mapDiv").append(s);

    function focusMap()
    {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: pos.lat, lng: pos.lng},
            zoom: 12
        });

        updateDirections('DRIVING');
    }

    function updateDirections(mode)
    {
        if(directionsDisplay != null)
        {
            directionsDisplay.setDirections({routes: []});
        }
        var lat = $('#map_lat').val();
        var lng = $('#map_lng').val();
        var directionsService = new google.maps.DirectionsService;
        directionsService.route({
            origin: pos.lat +','+ pos.lng,
            destination: parseFloat(lat) +','+ parseFloat(lng),
            travelMode: mode,
        }, function(response, status) {
            if (status === google.maps.DirectionsStatus.OK) {
                directionsDisplay = new google.maps.DirectionsRenderer({
                    suppressMarkers: true,
                    map: map,
                    directions: response,
                    draggable: false,
                    suppressPolylines: false,
                    // IF YOU SET `suppressPolylines` TO FALSE, THE LINE WILL BE
                    // AUTOMATICALLY DRAWN FOR YOU.
                });

                // IF YOU WISH TO APPLY USER ACTIONS TO YOUR LINE YOU NEED TO CREATE A
                // `polyLine` OBJECT BY LOOPING THROUGH THE RESPONSE ROUTES AND CREATING A
                // LIST
                // pathPoints = response.routes[0].overview_path.map(function (location) {
                //     return {lat: location.lat(), lng: location.lng()};
                // });
                //
                // var assumedPath = new google.maps.Polyline({
                //     path: pathPoints, //APPLY LIST TO PATH
                //     geodesic: true,
                //     strokeColor: '#708090',
                //     strokeOpacity: 0.7,
                //     strokeWeight: 2.5
                // });

                //assumedPath.setMap(map);
            }
        });
    }

    function fillInfo()
    {
        infoWindow = new google.maps.InfoWindow;
        $.get("https://maps.googleapis.com/maps/api/geocode/json?latlng="+ pos.lat +","+ pos.lng +"&key=AIzaSyB_31_zbWz5kMAY6ai-oG2pCfNtT089TLo", function(data, status)
        {
            console.log("Data: " + data.results[0].formatted_address + "\nStatus: " + status);

            infoWindow.setPosition(pos);
            infoWindow.setContent(data.results[0].formatted_address);
            infoWindow.open(map);
        });

        map.setCenter(pos);
    }


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#selectedImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image").change(function(){
        readURL(this);
    });
});

