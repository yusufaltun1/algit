var
    mapObject,
    markers = [],
    markersData =  [];

var pos;

(function (A) {

    if (!Array.prototype.forEach)
        A.forEach = A.forEach || function (action, that) {
                for (var i = 0, l = this.length; i < l; i++)
                    if (i in this)
                        action.call(that, this[i], i, this);
            };
    $('.search-overlay-menu-btn').click(function (e) {
        fillList();
    });

})(Array.prototype);

if (navigator.geolocation) {
    navigator.geolocation.getCurrentPosition(function (position) {
        pos = {
            lat: position.coords.latitude,
            lng: position.coords.longitude
        };

        fillList();
    });
}

function fillList()
{
    $( ".strip_list" ).remove();
    var min = $('#ion-from-hidden').val();
    var max = $('#ion-to-hidden').val();

    var values = '';
    $('input:checkbox.category-check').each(function () {
        values += (this.checked ? $(this).val() +"," : "");
    });

    $.get( "/store-list/my/near/"+ pos.lat +"/"+ pos.lng +"/"+ min +"/"+ max +"/"+ values, function( data )
    {
        $.each(data, function( index, value ) {
            addStore(value);
            var obj = {
                id: value.storeId,
                name: value.name,
                location_latitude: parseFloat(value.storeInfo.mapLat),
                location_longitude: parseFloat(value.storeInfo.mapLng),
                map_image_url: 'data:image/png;base64,'+ value.storeInfo.image,
                name_point: value.name,
                type_point: value.name,
                description_point: value.storeInfo.address +'<br><strong>Acilis Kapanis</strong>: 09am-10pm.',
                url_point: 'detail_page.html'
            }

            markersData.push(obj);
        });

        var marker;
        mapObject = new google.maps.Map(document.getElementById('map_listing'), mapOptions);

        markersData.forEach(function (item) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(item.location_latitude, item.location_longitude),
                map: mapObject,
                icon: '/img/pins/Chinese.png',
            });

            if ('undefined' === typeof markers[item.id])
                markers[item.id] = [];

            markers[item.id].push(marker);
            google.maps.event.addListener(marker, 'click', (function () {
                closeInfoBox();
                getInfoBox(item).open(mapObject, this);
                mapObject.setCenter(new google.maps.LatLng(item.location_latitude, item.location_longitude));
            }));

        });

        var infoWindow = new google.maps.InfoWindow;
        infoWindow.setPosition(pos);
        infoWindow.setContent('Suan Buradasiniz !');
        infoWindow.open(mapObject);

        mapObject.setCenter(pos);
        $('#status').fadeOut();
        $('#preloader').delay(250).fadeOut('slow');
        $( "map_listing_header" ).addClass( "sticky" );

    });

    var mapOptions = {
        zoom: 14,
        center: new google.maps.LatLng(pos.lat, pos.lng),
        mapTypeId: google.maps.MapTypeId.ROADMAP,

        mapTypeControl: false,
        mapTypeControlOptions: {
            style: google.maps.MapTypeControlStyle.DROPDOWN_MENU,
            position: google.maps.ControlPosition.LEFT_CENTER
        },
        panControl: false,
        panControlOptions: {
            position: google.maps.ControlPosition.TOP_RIGHT
        },
        zoomControl: true,
        zoomControlOptions: {
            style: google.maps.ZoomControlStyle.LARGE,
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        scrollwheel: false,
        scaleControl: false,
        scaleControlOptions: {
            position: google.maps.ControlPosition.TOP_LEFT
        },
        streetViewControl: true,
        streetViewControlOptions: {
            position: google.maps.ControlPosition.RIGHT_BOTTOM
        },
        styles: [{
            "featureType": "landscape",
            "stylers": [{"hue": "#FFBB00"}, {"saturation": 43.400000000000006}, {"lightness": 37.599999999999994}, {"gamma": 1}]
        }, {
            "featureType": "road.highway",
            "stylers": [{"hue": "#FFC200"}, {"saturation": -61.8}, {"lightness": 45.599999999999994}, {"gamma": 1}]
        }, {
            "featureType": "road.arterial",
            "stylers": [{"hue": "#FF0300"}, {"saturation": -100}, {"lightness": 51.19999999999999}, {"gamma": 1}]
        }, {
            "featureType": "road.local",
            "stylers": [{"hue": "#FF0300"}, {"saturation": -100}, {"lightness": 52}, {"gamma": 1}]
        }, {
            "featureType": "water",
            "stylers": [{"hue": "#0078FF"}, {"saturation": -13.200000000000003}, {"lightness": 2.4000000000000057}, {"gamma": 1}]
        }, {
            "featureType": "poi",
            "stylers": [{"hue": "#00FF6A"}, {"saturation": -1.0989010989011234}, {"lightness": 11.200000000000017}, {"gamma": 1}]
        }]

    };
}

function hideAllMarkers() {
    for (var key in markers)
        markers[key].forEach(function (marker) {
            marker.setMap(null);
        });
};

function closeInfoBox() {
    $('div.infoBox').remove();
};

function addStore(store)
{
    var content = "";
    content += '<div class="strip_list">'
    +'    <!--<div class="ribbon_1">-->'
    +'        <!--Popular-->'
    +'    <!--</div>-->'
    +'    <div class="row">'
    +'        <div class="col-md-9 col-sm-9">'
    +'            <div class="desc">'
    +'                <div class="thumb_strip">'
    +'                    <a href="detail_page.html"><img src="data:image/png;base64,'+ store.storeInfo.image +'" alt=""></a>'
    +'                </div>'
    +'                <div class="rating">';

    var votes = '';

    for(var i=0; i < store.reviewSummary.totalAvarage; i++ )
    {
        votes += '<i class="icon_star voted"></i>';
    }

    for(var i=0; i < 5 - store.reviewSummary.totalAvarage; i++ )
    {
        votes += '<i class="icon_star"></i>';
    }

    content += votes;
    content += '</div>'
    +'                <h3 >'+ store.name +'</h3>'
    +'                <div class="type" >'+ store.name
    +'                </div>'
    +'                <div class="location" >' + store.storeInfo.address
    +'                    <span class="opening">Opens at 17:00.</span> Minimum order: $15'
    +'                </div>'
    +'                <!--<ul>-->'
    +'                    <!--<li>Take away<i class="icon_check_alt2 ok"></i></li>-->'
    +'                    <!--<li>Delivery<i class="icon_check_alt2 ok"></i></li>-->'
    +'                <!--</ul>-->'
    +'            </div>'
    +'        </div>'
    +'        <div class="col-md-3 col-sm-3">'
    +'            <div class="go_to">'
    +'                <div>'
    +'                    <a href="/store/'+ store.storeId+ '" class="btn_1">Siparis Ver</a>'
    +'                    <a onclick="onHtmlClick('+ store.storeId +')" class="btn_listing">Haritada Goster</a>'
    +'                </div>'
    +'            </div>'
    +'        </div>'
    +'    </div><!-- End row-->'
    +'</div>';

    $('#store-list').append(content);
}
function getInfoBox(item) {
    return new InfoBox({
        content: '<div class="marker_info" id="marker_info">' +
        '<img src="' + item.map_image_url + '" alt="" width="80" height="80"/>' +
        '<h3>' + item.name_point + '</h3>' +
        '<em>' + item.type_point + '</em>' +
        '<span>' + item.description_point + '</span>' +
        '<a href=" /store/'+ item.id +'" class="btn_1">Siparis Ver</a>' +
        '</div>',
        disableAutoPan: false,
        maxWidth: 0,
        pixelOffset: new google.maps.Size(10, 110),
        closeBoxMargin: '5px -20px 2px 2px',
        closeBoxURL: "http://www.google.com/intl/en_us/mapfiles/close.gif",
        isHidden: false,
        alignBottom: true,
        pane: 'floatPane',
        enableEventPropagation: true
    });
};
function onHtmlClick(location_type) {
    google.maps.event.trigger(markers[location_type][0], "click");
};