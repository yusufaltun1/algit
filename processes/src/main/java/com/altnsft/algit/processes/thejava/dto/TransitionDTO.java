package com.altnsft.algit.processes.thejava.dto;

/**
 * Created by yusufaltun on 19.07.2018.
 */
public class TransitionDTO
{
    private long id;
    private String transitionCode;
    private String toBeanName;

    public long getId()
    {
        return id;
    }

    public void setId(long id)
    {
        this.id = id;
    }

    public String getTransitionCode()
    {
        return transitionCode;
    }

    public void setTransitionCode(String transitionCode)
    {
        this.transitionCode = transitionCode;
    }

    public String getToBeanName()
    {
        return toBeanName;
    }

    public void setToBeanName(String toBeanName)
    {
        this.toBeanName = toBeanName;
    }
}
