package com.altnsft.algit.processes.thejava.service;

import com.altnsft.algit.processes.thejava.entity.ProcessStatus;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.altnsft.algit.processes.thejava.entity.EventEntity;
import com.altnsft.algit.processes.thejava.entity.PathEntity;
import com.altnsft.algit.processes.thejava.entity.ProcessEntity;
import com.altnsft.algit.processes.thejava.repositories.PathRepository;
import com.altnsft.algit.processes.thejava.repositories.ProcessActionLogRepository;
import com.altnsft.algit.processes.thejava.repositories.ProcessRepository;

/**
 * Created by yusufaltun on 19.07.2018.
 */
@Service
public class EventService
{
    @Autowired
    private PathRepository pathRepository;

    @Autowired
    private ProcessRepository processRepository;

    @Autowired
    private ProcessActionLogRepository processActionLogRepository;

    public void createAndStartProcess(String processName, String pathName)
    {
        PathEntity pathEntity = pathRepository.findByName(pathName);
        ProcessEntity entity = new ProcessEntity();

        entity.setPath(pathEntity);
        entity.setName(processName);
        entity.setProcessStatus(ProcessStatus.CONTINUED);
        processRepository.save(entity);
    }

    public EventEntity findRunnableEvent(ProcessEntity process)
    {
        EventEntity eventEntity = null;
        try
        {
            return processActionLogRepository.findAllByProcessOrderByIdDesc(process).get(0).getEvent();
        }
        catch (Exception e)
        {

        }

        if(eventEntity == null)
        {
            return process.getPath().getStartEvent();
        }

        return null;
    }
}
