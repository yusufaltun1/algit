package com.altnsft.algit.processes.thejava.events;

import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

import static java.lang.String.format;

public class EventPathListenerNode implements EventPathNode
{
    private static final String EXCEPTION_ON_TRIGGERING_METHOD = "Exception on triggering %s  method of %s ";
    private EventListener listener;
    private String beanName;

    private EventPathListenerNode (EventListener listener) {
        this.listener = listener;
        this.beanName = listener.getBeanName();
    }

    public String execute (EventSource source, String methodName) {
        Class<?> clazz = listener.getClass();
        try {
            Method method = clazz.getMethod(methodName, EventSource.class);
            String returnCode = (String) method.invoke(listener, source);

            return returnCode;
        } catch (InvocationTargetException e) {
            if (e.getCause() instanceof ListenerTriggeringBreakerException) {
                throw (ListenerTriggeringBreakerException) e.getCause();
            } else {
                throw new RuntimeException(format(EXCEPTION_ON_TRIGGERING_METHOD, methodName, listener.getClass().getName()), e);
            }
        } catch (Exception e) {
            throw new RuntimeException(format(EXCEPTION_ON_TRIGGERING_METHOD, methodName, listener.getClass().getName()), e);
        }
    }

    @Override
    public String getBeanName() {
        return beanName;
    }

    public static EventPathListenerNode wrap(EventListener listener)
    {
        return new EventPathListenerNode(listener);
    }
}
