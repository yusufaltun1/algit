package com.altnsft.algit.processes.thejava.events;

import java.util.Map;
import java.util.concurrent.locks.ReentrantReadWriteLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.ReadLock;
import java.util.concurrent.locks.ReentrantReadWriteLock.WriteLock;

import static com.google.common.collect.Maps.newHashMap;

public class Events
{
    private static final Map<Long, Event> events = newHashMap();
    private static final ReentrantReadWriteLock lock = new ReentrantReadWriteLock();
    private static final ReadLock readLock = lock.readLock();
    private static final WriteLock writeLock = lock.writeLock();

    public static final Event get (Long id) {
        Event event = null;
        readLock.lock();
        try {
            event = events.get(id);
        } finally {
            readLock.unlock();
        }
        return event;
    }

    private static final void put (Long id, Event event) {
        writeLock.lock();
        try {
        events.put(id, event);
        } finally {
            writeLock.unlock();
        }
    }

    public static final Event event (Long id, EventType type) {
        Event event = get(id);
        if (event == null) {
            event = Event.aNew(type);
            put(id, event);
        }
        return event;
    }

    public static final void reset () {
        events.clear();
    }
}
