package com.altnsft.algit.processes.thejava.entity;

/**
 * Created by yusufaltun on 15.09.2018.
 */
public enum ProcessStatus
{
    CONTINUED, FINISHED;
}
