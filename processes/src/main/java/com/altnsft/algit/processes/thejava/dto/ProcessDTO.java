package com.altnsft.algit.processes.thejava.dto;

import com.altnsft.algit.processes.thejava.entity.ProcessActionLogEntity;

import java.util.List;

/**
 * Created by yusufaltun on 19.07.2018.
 */
public class ProcessDTO
{
    private long id;
    private String name;
    private long pathId;

    private List<ProcessActionLogEntity> logs;

    public List<ProcessActionLogEntity> getLogs() {
        return logs;
    }

    public void setLogs(List<ProcessActionLogEntity> logs) {
        this.logs = logs;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getPathId() {
        return pathId;
    }

    public void setPathId(long pathId) {
        this.pathId = pathId;
    }
}
