package com.altnsft.algit.processes.thejava.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.altnsft.algit.processes.thejava.entity.EventEntity;
import com.altnsft.algit.processes.thejava.entity.ProcessActionLogEntity;
import com.altnsft.algit.processes.thejava.entity.ProcessActionLogStatus;
import com.altnsft.algit.processes.thejava.entity.ProcessEntity;
import com.altnsft.algit.processes.thejava.repositories.ProcessActionLogRepository;

/**
 * Created by yusufaltun on 18.07.2018.
 */
@Service
public class LogService
{
    @Autowired
    private ProcessActionLogRepository processActionLogRepository;

    public void createLog(ProcessEntity processEntity, EventEntity eventEntity, String message, ProcessActionLogStatus status)
    {

        ProcessActionLogEntity processActionLogEntity = new ProcessActionLogEntity();
        processActionLogEntity.setEvent(eventEntity);
        processActionLogEntity.setProcess(processEntity);
        processActionLogEntity.setMessage(message);
        processActionLogEntity.setProcessStatus(status);

        processActionLogRepository.save(processActionLogEntity);
    }

}
