package com.altnsft.algit.processes.thejava.exceptions;

/**
 * Created by yusufaltun on 19.07.2018.
 */
public class ProcessNotFoundException extends RuntimeException
{
    public ProcessNotFoundException(String processCode)
    {
        super("Process not found with code "+ processCode);
    }
}
