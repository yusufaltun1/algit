package com.altnsft.algit.processes.thejava.dto;

import java.util.List;

/**
 * Created by yusufaltun on 19.07.2018.
 */
public class EventDTO
{
    private long id;
    private String eventName;
    private String eventBeanName;
    private String methodName;

    private List<TransitionDTO> transitions;

    public List<TransitionDTO> getTransitions() {
        return transitions;
    }

    public void setTransitions(List<TransitionDTO> transitions) {
        this.transitions = transitions;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    private long pathId;

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventBeanName() {
        return eventBeanName;
    }

    public void setEventBeanName(String eventBeanName) {
        this.eventBeanName = eventBeanName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public long getPathId() {
        return pathId;
    }

    public void setPathId(long pathId) {
        this.pathId = pathId;
    }
}
