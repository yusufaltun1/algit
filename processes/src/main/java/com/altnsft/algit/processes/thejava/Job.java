package com.altnsft.algit.processes.thejava;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import com.altnsft.algit.processes.thejava.context.ApplicationContextProvider;

import com.altnsft.algit.processes.thejava.entity.*;
import com.altnsft.algit.processes.thejava.events.*;
import com.altnsft.algit.processes.thejava.exceptions.RequirementNotProvidedException;
import com.altnsft.algit.processes.thejava.repositories.*;
import com.altnsft.algit.processes.thejava.service.EventService;
import com.altnsft.algit.processes.thejava.service.ParameterService;
import com.altnsft.algit.processes.thejava.service.RequirementService;
import org.springframework.stereotype.Component;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 17.07.2018.
 */
@Component
@Transactional
public class Job
{
    private static final Logger LOGGER = LoggerFactory.getLogger(Job.class);

    @Autowired
    private ProcessActionLogRepository processActionLogRepository;
    @Autowired
    private EventRepository eventRepository;

    @Autowired
    private ProcessRepository processRepository;

    @Autowired
    private TransitionRepository transitionRepository;

    @Autowired
    private ParameterService parameterService;

    @Autowired
    private PreparatoryRepository preparatoryRepository;

    @Autowired
    private RequirementService requirementService;

    @Autowired
    private EventService eventService;

    @Scheduled(fixedDelay = 1000, initialDelay = 1000)
    public void scheduleTaskUsingCronExpression() {

        Iterable<ProcessEntity> entities = processRepository.findAllByProcessStatus(ProcessStatus.CONTINUED);

        Events.reset();

        for (ProcessEntity entity : entities)
        {
            EventType type = new EventType() {
                @Override
                public String name() {
                    return "perform";
                }

                @Override
                public String getMethodName() {
                    return "perform";
                }
            };

            Events.event(entity.getId(), type);

            List<Parameter> parameters = new ArrayList<>();

            EventEntity eventEntity = eventService.findRunnableEvent(entity);
            checkAndCreateEvent(eventEntity, entity, parameters);

            parameters.add(Parameter.by("process", entity));

            Events.get(entity.getId()).fire((Parameter[]) parameters.toArray(new Parameter[parameters.size()]));
        }
    }

    private final void checkAndCreateEvent(EventEntity event, ProcessEntity process, List<Parameter> parameters)
    {
        List<ProcessActionLogEntity> logs = processActionLogRepository.findAllByEventAndProcessOrderByCreatedAtDesc(event, process);
        if (logs.size() == 0 || logs.get(0).getProcessStatus().equals(ProcessActionLogStatus.ERROR))
        {
            try
            {
                parameters.addAll(requirementService.getRequirements(event, process));
                fillParametersIfNeeded(parameters, process);
                createEvent(event, process, parameters);
            }
            catch (RequirementNotProvidedException e)
            {
                LOGGER.warn("Requirement not provided ");
            }
        }
        else if(logs.get(0).getProcessStatus().equals(ProcessActionLogStatus.WAITING_FOR_REQUIREMENT))
        {
            try
            {
                parameters.addAll(requirementService.getRequirements(event, process));
                fillParametersIfNeeded(parameters, process);
                createEvent(event, process, parameters);
            }
            catch (RequirementNotProvidedException e)
            {
                //LOGGER.error("requirement not found", e);
            }
        }
        else
        {
            process.setProcessStatus(ProcessStatus.FINISHED);
            processRepository.save(process);
        }
    }

    private void fillParametersIfNeeded(List<Parameter> parameters, ProcessEntity entity)
    {
        List<ParameterEntity> parameterEntities = parameterService.getAllParameterByProcess(entity);

        PreparatoryEntity preparatoryEntity = preparatoryRepository.findByPath(entity.getPath());
        Preparatory preparatory = null;
        if(preparatoryEntity != null)
        {
            preparatory  = (Preparatory)ApplicationContextProvider.context.getBean(preparatoryEntity.getBeanName());
        }
        if(preparatory != null && parameterEntities.size() > 0)
        {
            parameters.addAll(preparatory.prepare(parameterEntities));

            return;
        }

        for (ParameterEntity parameterEntity : parameterEntities)
        {
            parameters.add(Parameter.by(parameterEntity.getKey(), parameterEntity.getValue()));
        }
    }
    private void createEvent(EventEntity event, ProcessEntity process, List<Parameter> parameters)
    {
        EventListener eventListener = (EventListener) ApplicationContextProvider.context.getBean(event.getEventBeanName());

        List<TransitionEntity> entities = transitionRepository.findAllByEvent(event);

        TransitionMap map = TransitionMap.aNew();

        entities.stream().forEach(entity -> map.add(entity.getTransitionCode(), entity.getToBeanName()));

        Events.get(process.getId()).add(eventListener);

        parameters.add(Parameter.by(event.getEventBeanName(), event));
    }
}
