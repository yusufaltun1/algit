package com.altnsft.algit.processes.thejava.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * Created by yusufaltun on 17.07.2018.
 */
@Table(name = "paths")
@Entity
public class PathEntity extends BaseEntity
{
    @Column(unique = true)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private EventEntity startEvent;

    public EventEntity getStartEvent() {
        return startEvent;
    }

    public void setStartEvent(EventEntity startEvent) {
        this.startEvent = startEvent;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
