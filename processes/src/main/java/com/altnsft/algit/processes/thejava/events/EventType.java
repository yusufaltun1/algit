package com.altnsft.algit.processes.thejava.events;

public interface EventType
{
	public String name();

	public String getMethodName();
}
