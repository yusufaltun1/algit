package com.altnsft.algit.processes.thejava.entity;

/**
 * Created by yusufaltun on 17.07.2018.
 */
public enum ProcessActionLogStatus
{
    RUNNING, WAITING, FINISHED, ERROR, WILL_RETRY, WAITING_FOR_REQUIREMENT;
}
