package com.altnsft.algit.processes.thejava.service;

import com.altnsft.algit.processes.thejava.entity.ParameterEntity;
import com.altnsft.algit.processes.thejava.entity.PathEntity;
import com.altnsft.algit.processes.thejava.entity.ProcessEntity;
import com.altnsft.algit.processes.thejava.entity.ProcessStatus;
import com.altnsft.algit.processes.thejava.events.Parameter;
import com.altnsft.algit.processes.thejava.repositories.PathRepository;
import com.altnsft.algit.processes.thejava.repositories.ProcessRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by yusufaltun on 21.07.2018.
 */
@Service
public class ProcessService
{
    @Autowired
    private ProcessRepository processRepository;

    @Autowired
    private PathRepository pathRepository;

    @Autowired
    private ParameterService parameterService;

    public void createProcess(String processName, long pathId, Parameter... parameters)
    {
        ProcessEntity processEntity = new ProcessEntity();
        PathEntity pathEntity = pathRepository.findById(pathId).get();
        StringBuilder processNameBuilder = new StringBuilder(processName).append("_").append(pathEntity.getName());
        processEntity.setName(processNameBuilder.toString());
        processEntity.setPath(pathEntity);
        processEntity.setProcessStatus(ProcessStatus.CONTINUED);
        processRepository.save(processEntity);

        for(Parameter param : parameters)
        {
            ParameterEntity parameterEntity = new ParameterEntity();
            parameterEntity.setKey(param.name);
            parameterEntity.setProcess(processEntity);
            parameterEntity.setValue((String) param.value);

            parameterService.saveParameter(parameterEntity);
        }
    }
}
