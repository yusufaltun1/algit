package com.altnsft.algit.processes.thejava.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.altnsft.algit.processes.thejava.entity.ParameterEntity;
import com.altnsft.algit.processes.thejava.entity.ProcessEntity;
import com.altnsft.algit.processes.thejava.repositories.ParameterRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 20.07.2018.
 */
@Service
public class ParameterService
{
    @Autowired
    private ParameterRepository parameterRepository;

    public List<ParameterEntity> getAllParameterByProcess(ProcessEntity processEntity)
    {
        try {
           return parameterRepository.findAllByProcess(processEntity);
        }
        catch (Exception e)
        {
            return new ArrayList<>();
        }
    }

    public void saveParameter(ParameterEntity parameterEntity)
    {
        parameterRepository.save(parameterEntity);
    }
}
