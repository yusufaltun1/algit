package com.altnsft.algit.processes.thejava.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * Created by yusufaltun on 17.07.2018.
 */
@Table(name = "process_logs")
@Entity
public class ProcessActionLogEntity extends BaseEntity
{
    private String message;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "event_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private EventEntity event;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "process_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private ProcessEntity process;

    public ProcessEntity getProcess() {
        return process;
    }

    public void setProcess(ProcessEntity process) {
        this.process = process;
    }

    @Enumerated(EnumType.STRING)
    @Column(name = "process_status")
    private ProcessActionLogStatus processStatus;

    public ProcessActionLogStatus getProcessStatus() {
        return processStatus;
    }

    public void setProcessStatus(ProcessActionLogStatus processStatus) {
        this.processStatus = processStatus;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public EventEntity getEvent() {
        return event;
    }

    public void setEvent(EventEntity event) {
        this.event = event;
    }
}
