package com.altnsft.algit.processes.thejava.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.altnsft.algit.processes.thejava.entity.EventEntity;
import com.altnsft.algit.processes.thejava.entity.TransitionEntity;

import java.util.List;

/**
 * Created by yusufaltun on 17.07.2018.
 */
@Repository
public interface TransitionRepository extends CrudRepository<TransitionEntity, Long>
{
    List<TransitionEntity> findAllByEvent(EventEntity eventEntity);
}
