package com.altnsft.algit.processes.thejava.entity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * Created by yusufaltun on 17.07.2018.
 */
@Entity
@Table(name = "evets")
public class EventEntity extends BaseEntity
{
    @Column(unique = true)
    private String eventName;

    @Column(unique = true)
    private String eventBeanName;
    private String methodName;

    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "path_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private PathEntity path;

    public String getEventName() {
        return eventName;
    }

    public void setEventName(String eventName) {
        this.eventName = eventName;
    }

    public String getEventBeanName() {
        return eventBeanName;
    }

    public void setEventBeanName(String eventBeanName) {
        this.eventBeanName = eventBeanName;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }

    public PathEntity getPath() {
        return path;
    }

    public void setPath(PathEntity path) {
        this.path = path;
    }
}
