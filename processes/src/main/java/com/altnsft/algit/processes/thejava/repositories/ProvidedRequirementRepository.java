package com.altnsft.algit.processes.thejava.repositories;

import org.springframework.data.repository.CrudRepository;
import com.altnsft.algit.processes.thejava.entity.ProcessEntity;
import com.altnsft.algit.processes.thejava.entity.ProvidedRequirementEntity;
import com.altnsft.algit.processes.thejava.entity.RequirementEntity;

import java.util.List;

/**
 * Created by yusufaltun on 19.07.2018.
 */
public interface ProvidedRequirementRepository extends CrudRepository<ProvidedRequirementEntity, Long>
{
    ProvidedRequirementEntity findByRequirement(RequirementEntity entity);

    List<ProvidedRequirementEntity> findAllByProcess(ProcessEntity processEntity);
}
