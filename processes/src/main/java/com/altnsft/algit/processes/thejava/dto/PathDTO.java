package com.altnsft.algit.processes.thejava.dto;

import java.util.List;

/**
 * Created by yusufaltun on 19.07.2018.
 */
public class PathDTO
{
    private long id;
    private String name;
    private List<EventDTO> eventDTOS;

    public List<EventDTO> getEventDTOS() {
        return eventDTOS;
    }

    public void setEventDTOS(List<EventDTO> eventDTOS) {
        this.eventDTOS = eventDTOS;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
