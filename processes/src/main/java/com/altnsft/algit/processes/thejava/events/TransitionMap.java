package com.altnsft.algit.processes.thejava.events;

import java.util.HashMap;

/**
 * Created by yusufaltun on 17.07.2018.
 */
public class TransitionMap
{
    private final HashMap<String, String> transitionMap = new HashMap<>();

    public TransitionMap add(String transitionCode, String beanName)
    {
        transitionMap.put(transitionCode, beanName);
        return this;
    }

    public String get(String transitionCode)
    {
        if(transitionMap.containsKey(transitionCode))
        {
            return transitionMap.get(transitionCode);
        }

        return null;
    }

    public static TransitionMap aNew()
    {
        return new TransitionMap();
    }
}
