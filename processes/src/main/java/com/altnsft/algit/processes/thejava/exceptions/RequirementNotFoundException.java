package com.altnsft.algit.processes.thejava.exceptions;

/**
 * Created by yusufaltun on 19.07.2018.
 */
public class RequirementNotFoundException extends RuntimeException
{
    public RequirementNotFoundException(String requirementCode) {
        super("Requirement not found with code "+ requirementCode);
    }
}
