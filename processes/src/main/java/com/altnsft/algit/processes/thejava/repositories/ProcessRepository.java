package com.altnsft.algit.processes.thejava.repositories;

import com.altnsft.algit.processes.thejava.entity.ProcessStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.altnsft.algit.processes.thejava.entity.ProcessEntity;

import java.util.List;

/**
 * Created by yusufaltun on 17.07.2018.
 */
@Repository
public interface ProcessRepository extends CrudRepository<ProcessEntity, Long>
{
    ProcessEntity findByName(String name);
    List<ProcessEntity> findAllByProcessStatus(ProcessStatus processStatus);
}
