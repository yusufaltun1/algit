package com.altnsft.algit.processes.thejava.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.altnsft.algit.processes.thejava.entity.PathEntity;

/**
 * Created by yusufaltun on 17.07.2018.
 */
@Repository
public interface PathRepository extends CrudRepository<PathEntity, Long>
{
    PathEntity findByName(String name);
}
