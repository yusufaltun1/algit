package com.altnsft.algit.processes.thejava.events;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import com.altnsft.algit.processes.thejava.context.ApplicationContextProvider;
import com.altnsft.algit.processes.thejava.exceptions.RequirementNotProvidedException;
import com.altnsft.algit.processes.thejava.service.LogService;
import com.altnsft.algit.processes.thejava.service.RequirementService;
import com.altnsft.algit.processes.thejava.service.TransitionService;
import com.altnsft.algit.processes.thejava.entity.EventEntity;
import com.altnsft.algit.processes.thejava.entity.ProcessActionLogStatus;
import com.altnsft.algit.processes.thejava.entity.ProcessEntity;

import java.util.ArrayList;
import java.util.List;

import static java.lang.String.format;

public class EventPath implements EventPathNode
{
    private static final String STOPPING_ON_TRIGGERING_METHODS = "Stopping triggering rest of listeners for %s. Reason : {%s}";

    private static final Logger logger = LogManager.getLogger(Event.class);

    private List<EventPathNode> listeners = new ArrayList<>();

    public EventPath add (EventListener listener) {
        listeners.add(EventPathListenerNode.wrap(listener));
        return this;
    }

    public EventPath add(EventPath subPath)
    {
        listeners.add(subPath);
        return this;
    }

    public String execute(EventSource source, String methodName)
    {
        if(listeners.size() == 0)
        {
            return "";
        }
        EventPathNode listener = listeners.get(0);
        execute(listener, source, methodName);

        return "OK";
    }

    public void execute(EventPathNode listener, EventSource source, String methodName)
    {
        LogService logService = ApplicationContextProvider.context.getBean("logService", LogService.class);
        TransitionService transitionService = ApplicationContextProvider.context.getBean("transitionService", TransitionService.class);
        RequirementService requirementService = ApplicationContextProvider.context.getBean("requirementService", RequirementService.class);

        EventEntity eventEntity =(EventEntity) source.get(listener.getBeanName());
        ProcessEntity processEntity = (ProcessEntity) source.get("process");

        try
        {
            requirementService.getRequirements(eventEntity, processEntity);

            String returnCode = listener.execute(source, methodName);

            logService.createLog(processEntity, eventEntity, returnCode, ProcessActionLogStatus.FINISHED);

            TransitionMap map = transitionService.fillEventSourceAndGetTransitionMapByEvent(eventEntity, source);

            String nextBeanName = map.get(returnCode);

            if(nextBeanName == null)
            {
                return;
            }

            EventPathListenerNode nextevent = EventPathListenerNode.wrap((EventListener) ApplicationContextProvider.context.getBean(nextBeanName));

            execute(nextevent, source, methodName);
        }
        catch (RequirementNotProvidedException e)
        {
            logService.createLog(processEntity, eventEntity, "Requirements not provided", ProcessActionLogStatus.WAITING_FOR_REQUIREMENT);
        }
        catch (ListenerTriggeringBreakerException e)
        {
            if (Event.LOG_EVENTBRAKER) {
                logger.warn(format(STOPPING_ON_TRIGGERING_METHODS, methodName, e.getMessage()));
            }
            logService.createLog(processEntity, eventEntity, e.getMessage(), ProcessActionLogStatus.ERROR);
            return;
        }
    }

    @Override
    public String getBeanName() {
        return null;
    }

    public static EventPath mainPath () {
        return new EventPath();
    }

    public static EventPath subPath () {
        return new EventPath();
    }
}
