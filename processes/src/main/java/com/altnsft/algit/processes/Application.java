package com.altnsft.algit.processes;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EntityScan(basePackages = {"com.altnsft.algit.processes.thejava.entity"})
@EnableJpaRepositories(basePackages = {"com.altnsft.algit.processes.thejava.repositories"})
@ComponentScan(basePackages = {"com.altnsft.algit.processes.events",
        "com.altnsft.algit.processes.thejava.context",
        "com.altnsft.algit.processes.thejava",
        "com.altnsft.algit.processes.thejava.controller",
        "com.altnsft.algit.processes.thejava.service"
    })
@EnableScheduling
public class Application
{
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
