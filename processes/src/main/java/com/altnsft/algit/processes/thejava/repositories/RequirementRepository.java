package com.altnsft.algit.processes.thejava.repositories;

import org.springframework.data.repository.CrudRepository;
import com.altnsft.algit.processes.thejava.entity.EventEntity;
import com.altnsft.algit.processes.thejava.entity.RequirementEntity;

import java.util.List;

/**
 * Created by yusufaltun on 19.07.2018.
 */
public interface RequirementRepository extends CrudRepository<RequirementEntity, Long>
{
    List<RequirementEntity> findAllByEvent(EventEntity eventEntity);
    RequirementEntity findByKey(String key);
}
