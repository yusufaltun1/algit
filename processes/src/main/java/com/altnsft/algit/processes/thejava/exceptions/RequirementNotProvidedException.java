package com.altnsft.algit.processes.thejava.exceptions;

/**
 * Created by yusufaltun on 19.07.2018.
 */
public class RequirementNotProvidedException extends RuntimeException
{
    private String requirementName;

    public RequirementNotProvidedException(String requirementName) {
        super("Requirement not provided, requirement name "+ requirementName);
        this.requirementName = requirementName;
    }

    public String getRequirementName() {
        return requirementName;
    }
}
