package com.altnsft.algit.processes.thejava.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.altnsft.algit.processes.thejava.entity.EventEntity;
import com.altnsft.algit.processes.thejava.entity.TransitionEntity;
import com.altnsft.algit.processes.thejava.events.EventSource;
import com.altnsft.algit.processes.thejava.events.Parameter;
import com.altnsft.algit.processes.thejava.events.TransitionMap;
import com.altnsft.algit.processes.thejava.repositories.EventRepository;
import com.altnsft.algit.processes.thejava.repositories.TransitionRepository;

import java.util.List;

/**
 * Created by yusufaltun on 18.07.2018.
 */
@Service
public class TransitionService
{
    @Autowired
    private TransitionRepository transitionRepository;

    @Autowired
    private EventRepository eventRepository;

    public TransitionMap fillEventSourceAndGetTransitionMapByEvent(EventEntity eventEntity, EventSource eventSource)
    {
        List<TransitionEntity> transitionEntities = transitionRepository.findAllByEvent(eventEntity);
        TransitionMap map = TransitionMap.aNew();
        transitionEntities.stream().forEach(entity ->
        {
            map.add(entity.getTransitionCode(), entity.getToBeanName());

            EventEntity eventFromTransition = eventRepository.findByEventBeanName(entity.getToBeanName());
            eventSource.add(Parameter.by(entity.getToBeanName(), eventFromTransition));
        });

        return map;
    }
}
