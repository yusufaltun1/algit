package com.altnsft.algit.processes.thejava.events;

import org.springframework.beans.factory.annotation.Required;

import java.io.Serializable;

public interface EventListener extends Serializable
{
    @Required
    String getBeanName();

    String perform(EventSource eventSource);
}
