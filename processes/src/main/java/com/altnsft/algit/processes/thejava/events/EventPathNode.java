package com.altnsft.algit.processes.thejava.events;

public interface EventPathNode
{
    public String execute(EventSource source, String methodName);

    String getBeanName();
}

