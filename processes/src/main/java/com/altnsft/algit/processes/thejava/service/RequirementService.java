package com.altnsft.algit.processes.thejava.service;

import com.altnsft.algit.processes.thejava.events.Parameter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.altnsft.algit.processes.thejava.entity.EventEntity;
import com.altnsft.algit.processes.thejava.entity.ProcessEntity;
import com.altnsft.algit.processes.thejava.entity.ProvidedRequirementEntity;
import com.altnsft.algit.processes.thejava.entity.RequirementEntity;
import com.altnsft.algit.processes.thejava.exceptions.RequirementNotFoundException;
import com.altnsft.algit.processes.thejava.exceptions.RequirementNotProvidedException;
import com.altnsft.algit.processes.thejava.repositories.ProcessRepository;
import com.altnsft.algit.processes.thejava.repositories.ProvidedRequirementRepository;
import com.altnsft.algit.processes.thejava.repositories.RequirementRepository;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 19.07.2018.
 */
@Service
public class RequirementService
{
    @Autowired
    private RequirementRepository requirementRepository;

    @Autowired
    private ProvidedRequirementRepository providedRequirementRepository;

    @Autowired
    private ProcessRepository processRepository;

    public List<Parameter> getRequirements(EventEntity event, ProcessEntity process)
    {
        List<RequirementEntity> requirements = requirementRepository.findAllByEvent(event);

        List<ProvidedRequirementEntity> providedRequirements = providedRequirementRepository.findAllByProcess(process);
        List<Parameter> params = new ArrayList<>();
        requirements.stream().forEach(requirement ->
        {
            Boolean requirementIsProvided = new Boolean(false);
            for (ProvidedRequirementEntity providedRequirement : providedRequirements)
            {
                if(requirement.getId().longValue() == providedRequirement.getRequirement().getId().longValue())
                {
                    requirementIsProvided = true;
                    params.add(Parameter.by(requirement.getKey(), providedRequirement.getValue()));
                }
            }

            if(! requirementIsProvided)
            {
                throw new RequirementNotProvidedException(requirement.getKey());
            }
        });

        return params;
    }

    public void provideRequirement(String key, String requirementValue, String processName)
    {
        RequirementEntity requirementEntity = requirementRepository.findByKey(key);
        if(requirementEntity == null)
        {
            throw new RequirementNotFoundException(key);
        }

        ProcessEntity process = processRepository.findByName(processName);

        ProvidedRequirementEntity providedRequirementEntity = new ProvidedRequirementEntity();
        providedRequirementEntity.setRequirement(requirementEntity);
        providedRequirementEntity.setValue(requirementValue);
        providedRequirementEntity.setProcess(process);


        providedRequirementRepository.save(providedRequirementEntity);
    }
}
