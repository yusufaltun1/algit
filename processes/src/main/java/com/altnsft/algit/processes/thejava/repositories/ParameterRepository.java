package com.altnsft.algit.processes.thejava.repositories;

import org.springframework.data.repository.CrudRepository;
import com.altnsft.algit.processes.thejava.entity.ParameterEntity;
import com.altnsft.algit.processes.thejava.entity.ProcessEntity;

import java.util.List;


/**
 * Created by yusufaltun on 19.07.2018.
 */
public interface ParameterRepository extends CrudRepository<ParameterEntity, Long>
{
    List<ParameterEntity> findAllByProcess(ProcessEntity processEntity) ;
}
