package com.altnsft.algit.processes.thejava.repositories;

import org.springframework.data.repository.CrudRepository;
import com.altnsft.algit.processes.thejava.entity.PathEntity;
import com.altnsft.algit.processes.thejava.entity.PreparatoryEntity;

/**
 * Created by yusufaltun on 19.07.2018.
 */
public interface PreparatoryRepository extends CrudRepository<PreparatoryEntity, Long>
{
    PreparatoryEntity findByPath(PathEntity pathEntity);
}
