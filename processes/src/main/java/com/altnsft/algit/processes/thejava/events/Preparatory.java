package com.altnsft.algit.processes.thejava.events;

import com.altnsft.algit.processes.thejava.entity.ParameterEntity;

import java.util.List;

/**
 * Created by yusufaltun on 19.07.2018.
 */
public interface Preparatory
{
    List<Parameter> prepare(List<ParameterEntity> parameters);
}
