package com.altnsft.algit.processes.thejava.repositories;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import com.altnsft.algit.processes.thejava.entity.EventEntity;
import com.altnsft.algit.processes.thejava.entity.ProcessActionLogEntity;
import com.altnsft.algit.processes.thejava.entity.ProcessEntity;

import java.util.List;

/**
 * Created by yusufaltun on 17.07.2018.
 */
@Repository
public interface ProcessActionLogRepository extends CrudRepository<ProcessActionLogEntity, Long>
{
    List<ProcessActionLogEntity> findAllByEventAndProcessOrderByCreatedAtDesc(EventEntity eventEntity, ProcessEntity processEntity);
    List<ProcessActionLogEntity> findAllByProcessOrderByIdDesc(ProcessEntity processEntity);
}
