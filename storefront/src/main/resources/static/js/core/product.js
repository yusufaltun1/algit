/**
 * Created by yusufaltun on 23.06.2018.
 */


$(document).ready(function()
{
    var addedOptions = [];

    var addOptionBtn = $('#add-option-value');
    var saveOptionBtn = $('#save-option');
    var currentValues = [];
    var index = 0;
    var valueIndex = 0;

    function reloadTable()
    {
        $("#options tr").remove();
        var table = $("#options");
        var optionIndex = 0;
        addedOptions.forEach(function(item)
        {
            var tr = '<tr>'+
                '<td>'+ item.optionName +'</td>'+
                '<td>'+ item.optionType +'</td>'+
                '<td class="td-actions text-right">'+
                '<button type="button" rel="tooltip" title="Remove" id="'+ optionIndex +'" class="remove-product btn btn-danger btn-link btn-sm">'+
                '<i class="material-icons">close</i>'+
                '</button>'+
                '<input type="hidden" name="options['+ optionIndex +'].optionName" value="'+ item.optionName+'" />'+
                '<input type="hidden" name="options['+ optionIndex +'].optionType" value="'+ item.optionType+'" />';

            if(item.optionId != null)
            {
                tr += '<input type="hidden" name="options['+ optionIndex +'].optionId" value="'+ item.optionId+'" />';
            }

            var valueIndex = 0;
            item.optionValues.forEach(function (obj)
            {
                if(obj.valueId != null)
                {
                    tr += '<input type="hidden" name="options['+ optionIndex +'].optionValues['+ valueIndex +'].valueId" value="'+ obj.valueId+'" />';
                }
                tr += '<input type="hidden" name="options['+ optionIndex +'].optionValues['+ valueIndex +'].code" value="'+ obj.code+'" />'+
                      '<input type="hidden" name="options['+ optionIndex +'].optionValues['+ valueIndex +'].name" value="'+ obj.name+'" />'+
                      '<input type="hidden" name="options['+ optionIndex +'].optionValues['+ valueIndex +'].price" value="'+ obj.price+'" />';

                valueIndex ++;
            });

            tr += '</td>'+
                '</tr>';
            table.append(tr);

            optionIndex ++;
        });

        $(".remove-product").on('click', function ()
        {
            var val = $(this).attr('id');
            addedOptions.splice(val, 1);
            reloadTable();
        });
    }

    addOptionBtn.on('click', function()
    {
        var code = $('#option-value-code').val();
        var name = $('#option-value-name').val();
        var price = $('#option-value-price').val();

        if(code == '' || name == '' || price == '')
        {
            alert("Secenek ismi girilmeli");
            return;
        }

        var table = $('#added-options');

        var row = '<tr>'+
                        '<td>'+ code +'</td>'+
                        '<td>'+ name +'</td>'+
                        '<td>'+price +'</td>'+
                    '</tr>';

        table.append(row);

        $('#option-value-code').val('');
        $('#option-value-name').val('');
        $('#option-value-price').val('');

        var obj = {
            code : code,
            name : name,
            price : price
        };

        valueIndex ++;
        currentValues.push(obj);
    });

    saveOptionBtn.on('click', function ()
    {
        var optionName = $('#option-name').val();
        var optionType = $("input[name='option-type']:checked").val()

        if(optionName == "")
        {
            alert("Secenek ismi girilmeli");
            return;
        }

        var obj = {
            optionName : optionName,
            optionType : optionType,
            optionValues : currentValues
        };

        addedOptions.push(obj);
        currentValues = [];
        index ++;
        valueIndex = 0;

        $('#myModal').modal('hide');
        $('#option-name').val('');
        $('#added-options tr').remove();
        reloadTable();
    });

    var productId = $('#product-id').val();
    $.get("/option/get/"+productId, function(dataFromService)
    {
        dataFromService.forEach(function(item) {
            var values = [];
            item.optionValues.forEach(function(value){
               values.push({
                   valueId : value.valueId,
                   code : value.code,
                   name : value.name,
                   price : value.price
               });
            });

            var obj = {
                optionId : item.optionId,
                optionName : item.optionName,
                optionType : item.optionType,
                optionValues : values
            };

            addedOptions.push(obj);

            reloadTable();
        });
    });
});
