var map, infoWindow;
var pos;


/**
 * Created by yusufaltun on 23.06.2018.
 */
$(document).ready(function(){
    $('#btn-upload').click(function(e){
        e.preventDefault();
        $('#image').click();}
    );

     var lat = $('#mapLat').val();
     var lng = $('#mapLng').val();

    var s = document.createElement("script");
    s.type = "text/javascript";
    s.src  = "https://maps.googleapis.com/maps/api/js?key=AIzaSyBCDcfCaDpddoja0HvjWY0m7qhBQB2L5yg&callback=gmap_draw";
    window.gmap_draw = function(){
        if(lat == null || lat == '' || lng == null || lng == '')
        {
            if (navigator.geolocation)
            {
                navigator.geolocation.getCurrentPosition(function(position)
                {
                    pos = {
                        lat: position.coords.latitude,
                        lng: position.coords.longitude
                    };

                    focusMap();
                    fillInfo();
                });
            }
        }
        else
        {
            pos = {
                lat: parseFloat(lat),
                lng: parseFloat(lng)
            };
            focusMap();
            fillInfo();
        }

    };
    $("#mapDiv").append(s);

    function focusMap()
    {
        map = new google.maps.Map(document.getElementById('map'), {
            center: {lat: pos.lat, lng: pos.lng},
            zoom: 12
        });
    }

    function fillInfo()
    {
        infoWindow = new google.maps.InfoWindow;
        $.get("https://maps.googleapis.com/maps/api/geocode/json?latlng="+ pos.lat +","+ pos.lng +"&key=AIzaSyBCDcfCaDpddoja0HvjWY0m7qhBQB2L5yg", function(data, status)
        {
            console.log("Data: " + data.results[0].formatted_address + "\nStatus: " + status);

            $('#address').val(data.results[0].formatted_address);
            $('#mapLat').val(pos.lat);
            $('#mapLng').val(pos.lng);

            $( "#address" ).trigger( "focus" );
            $( "#mapLat" ).trigger( "focus" );
            $( "#mapLng" ).trigger( "focus" );
            infoWindow.setPosition(pos);
            infoWindow.setContent(data.results[0].formatted_address);
            infoWindow.open(map);
        });

        map.setCenter(pos);
    }


    function readURL(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();

            reader.onload = function (e) {
                $('#selectedImage').attr('src', e.target.result);
            }

            reader.readAsDataURL(input.files[0]);
        }
    }

    $("#image").change(function(){
        readURL(this);
    });
});

