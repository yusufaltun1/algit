/**
 * Created by yusufaltun on 23.06.2018.
 */


$(document).ready(function(){
    var data;
    var addedProductsList = [];
    function updateTable()
    {
        $("#added-products tr").remove();
        var index = 0;
        addedProductsList.forEach(function(item)
        {
            var tr = '<tr>'+
                '<td>'+ item.id +'</td>'+
                '<td>'+ item.name +'</td>'+
                '<td class="td-actions text-right">'+
                '<button type="button" rel="tooltip" title="Remove" id="'+ item.id +'" class="remove-product btn btn-danger btn-link btn-sm">'+
                '<i class="material-icons">close</i>'+
                '</button>'+
                '<input type="hidden" name="menuItems['+ index +'].productId" value="'+ item.id+'" />'+
                '<input type="hidden" name="menuItems['+ index +'].productTitle" value="'+ item.name+'" />'+
                '<input type="hidden" name="menuItems['+ index +'].menuItemId" value="'+ item.menuItemId+'" />'+
                '</td>'+
                '</tr>';
            addedProducts.append(tr);

            index ++;
        });

        $(".remove-product").on('click', function ()
        {
            var val = $(this).attr('id');
            var removeIndex = addedProductsList.map(function(item) { return item.id; }).indexOf(parseInt(val));
            addedProductsList.splice(removeIndex, 1);
            updateTable();
        });
    }

    $.get("/product/all", function(dataFromService)
    {
        data = dataFromService;

        var dataList = $('#json-datalist');
        data.forEach(function(item) {
            var option = document.createElement('option');
            option.value = item.id;
            option.text = item.name;
            dataList.append(option);
        });

        dataList.on('change', function ()
        {
            alert($(this).text())
        });
    });

    var addedProducts = $('#added-products');
    $("#add-product-btn").on('click', function ()
    {
        var val = $('#add-product').val();

        data.forEach(function(item)
        {
             if(item.id == val)
             {
                 addedProductsList.push(item);
                 updateTable();
             }
        });
    });

    var menuId = $('#menu-id').val();
    $.get("/menu/menu-items/"+menuId, function(dataFromService)
    {
        dataFromService.forEach(function(item) {
            addedProductsList.push(
                {
                    id  :item.productDTO.id,
                    name:item.productDTO.name,
                    menuItemId: item.menuItemId
                }
            );
        });

        updateTable();
    });
});

