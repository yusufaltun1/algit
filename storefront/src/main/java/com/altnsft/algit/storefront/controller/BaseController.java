package com.altnsft.algit.storefront.controller;

import com.altnsft.algit.models.dtos.StoreDTO;
import com.altnsft.algit.repositories.entities.StoreEntity;
import com.altnsft.algit.repositories.entities.UserEntity;
import com.altnsft.algit.storefront.services.UserService;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.web.bind.annotation.ModelAttribute;

/**
 * Created by yusufaltun on 18.06.2018.
 */
public abstract class BaseController
{
    @ModelAttribute("storeId")
    public Long getStoreId () {
        return getCurrentUser().getStoreEntity().getId();
    }

    protected UserEntity getCurrentUser()
    {
        String email = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        UserEntity userEntity = getUserService().findUserByEmail(email);

        return userEntity;
    }

    protected abstract UserService getUserService();
}
