package com.altnsft.algit.storefront.dto;

import java.util.List;

/**
 * Created by yusufaltun on 30.08.2018.
 */
public class GalleryImagesForm
{
    private List<String> images;

    public List<String> getImages() {
        return images;
    }

    public void setImages(List<String> images) {
        this.images = images;
    }
}
