package com.altnsft.algit.storefront.forms;

import com.altnsft.algit.storefront.annotations.ValidLongValue;

import javax.validation.constraints.Null;

/**
 * Created by yusufaltun on 23.06.2018.
 */
public class MenuItemForm
{
    @Null
    private int menuItemId;
    @ValidLongValue
    private int productId;
    private String productTitle;

    public int getMenuItemId() {
        return menuItemId;
    }


    public int getProductId() {
        return productId;
    }

    public void setMenuItemId(int menuItemId) {
        this.menuItemId = menuItemId;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public String getProductTitle() {
        return productTitle;
    }

    public void setProductTitle(String productTitle) {
        this.productTitle = productTitle;
    }
}
