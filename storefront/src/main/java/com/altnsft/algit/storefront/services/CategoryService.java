package com.altnsft.algit.storefront.services;

import com.altnsft.algit.repositories.entities.CategoryEntity;
import com.altnsft.algit.repositories.entities.StoreEntity;
import com.altnsft.algit.repositories.repository.CategoryRepository;
import com.altnsft.algit.storefront.forms.CategoryForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by yusufaltun on 16.06.2018.
 */
@Service
public class CategoryService extends BaseService
{
    @Autowired
    private CategoryRepository categoryRepository;

    public void save(CategoryForm form, StoreEntity storeEntity)
    {
        CategoryEntity entity = categoryRepository.findById(form.getId()).orElse(new CategoryEntity());

        entity.setId(form.getId());
        entity.setName(form.getName());
        entity.setText(form.getTitle());
        entity.setStoreEntity(storeEntity);

        categoryRepository.save(entity);

        getRestTemplate().getForEntity(getBaseURL() +"/store/update/"+ storeEntity.getId(), Void.class);
    }

    public List<CategoryEntity> getAllCategoriesByStore(StoreEntity storeEntity)
    {
        return categoryRepository.findAllByStoreEntity(storeEntity);
    }

    public void remove(int id)
    {
        categoryRepository.deleteById(Long.valueOf(id));
        getRestTemplate().getForEntity(getBaseURL() +"/store/update/"+ id, Void.class);
    }

    public CategoryEntity getCategoryById(long categoryId) {

        return categoryRepository.findById(categoryId).get();
    }

    public List<CategoryEntity> getAllCategoryForName(String name)
    {
        return categoryRepository.findAllByName(name);
    }
}
