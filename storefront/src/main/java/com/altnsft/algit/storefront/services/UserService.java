package com.altnsft.algit.storefront.services;

import com.altnsft.algit.repositories.entities.UserEntity;
import com.altnsft.algit.repositories.entities.UserRoleEntity;
import com.altnsft.algit.repositories.repository.RoleRepository;
import com.altnsft.algit.repositories.repository.UserRepository;
import com.altnsft.algit.repositories.repository.UserRoleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by yusufaltun on 16.06.2018.
 */
@Service
public class UserService
{

    @Autowired
    private UserRepository userRepository;
    @Autowired
    private RoleRepository roleRepository;

    @Autowired
    private UserRoleRepository userRoleRepository;

    public UserEntity findUserByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void saveUser(UserEntity user)
    {
        //Role userRole = roleRepository.findByRole("STORE");
        //user.setRoles(new HashSet<Role>(Arrays.asList(userRole)));
        userRepository.save(user);
    }

    public List<UserRoleEntity> getRoles(UserEntity userEntity)
    {
        return userRoleRepository.findByUserEntity(userEntity);
    }
}
