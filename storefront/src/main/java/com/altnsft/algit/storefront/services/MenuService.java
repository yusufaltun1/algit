package com.altnsft.algit.storefront.services;

import com.altnsft.algit.repositories.entities.*;
import com.altnsft.algit.repositories.repository.FoodCategoryRepository;
import com.altnsft.algit.repositories.repository.MenuItemRepository;
import com.altnsft.algit.repositories.repository.MenuRepository;
import com.altnsft.algit.repositories.repository.ProductRepository;
import com.altnsft.algit.storefront.dto.MenuItemDTO;
import com.altnsft.algit.storefront.forms.MenuForm;
import com.altnsft.algit.storefront.forms.MenuItemForm;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.validation.Valid;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 23.06.2018.
 */
@Service
public class MenuService extends BaseService
{
    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private MenuItemRepository menuItemRepository;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ProductService productService;

    @Autowired
    private FoodCategoryRepository foodCategoryRepository;

    public MenuForm getMenu(long id, StoreEntity storeEntity)
    {
        MenuEntity entity = menuRepository.findById(id).get();

        MenuForm form = new MenuForm();
        form.setDescription(entity.getDescription());
        form.setId(entity.getId());
        form.setName(entity.getName());
        form.setPrice(entity.getPrice().doubleValue());
        form.setSellable(entity.isSellable());
        form.setTitle(entity.getTitle());
        form.setImageStr(entity.getImage());
        if(entity.getFoodCategory() != null)
        {
            form.setFoodCategoryId(entity.getFoodCategory().getId());
        }
        CategoryEntity categoryEntity = categoryService.getCategoryById(entity.getCategoryEntity().getId());
        form.setCategoryId(categoryEntity.getId());
        List<MenuItemEntity> items = menuItemRepository.findAllByMenuEntity(entity);
        List<MenuItemForm> itemsForm = new ArrayList<>();

        for (MenuItemEntity item : items) {
            MenuItemForm menuItemForm = new MenuItemForm();
            menuItemForm.setProductId(item.getProductEntity().getId().intValue());
            menuItemForm.setProductTitle(item.getProductEntity().getTitle());

            itemsForm.add(menuItemForm);
        }

        form.setMenuItems(itemsForm);
        return form;
    }

    public void save( MenuForm form, StoreEntity storeEntity) throws IOException {
        MenuEntity entity = menuRepository.findById(form.getId()).orElse(new MenuEntity());

        entity.setDescription(form.getDescription());
        entity.setId(form.getId());
        entity.setName(form.getName());
        entity.setPrice(BigDecimal.valueOf(form.getPrice()));
        entity.setSellable(form.isSellable());
        entity.setTitle(form.getTitle());
        entity.setStoreEntity(storeEntity);

        FoodCategoryEntity foodCategoryEntity = foodCategoryRepository.findById(form.getFoodCategoryId()).get();
        entity.setFoodCategory(foodCategoryEntity);

        CategoryEntity categoryEntity = categoryService.getCategoryById(form.getCategoryId());
        entity.setCategoryEntity(categoryEntity);

        entity = menuRepository.save(entity);
//        String file = "/tmpimg/menu/menu-img-"+entity.getId() +"."+ FilenameUtils.getExtension(form.getImage().getOriginalFilename());
//        try {
//            form.getImage().transferTo(new File(file));
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
        //entity.setImage(encodeFileToBase64Binary(file));
        menuRepository.save(entity);
        List<MenuItemEntity> menuItemsFromDb = menuItemRepository.findAllByMenuEntity(entity);

        saveUnsavedItems(form, menuItemsFromDb, entity);

        deleteRemovedItems(form, menuItemsFromDb);

        menuItemsFromDb = menuItemRepository.findAllByMenuEntity(entity);

        entity.setMaxMakeTime(getMaxMakeTime(menuItemsFromDb));

        getRestTemplate().getForEntity(getBaseURL() +"/store/update/"+ storeEntity.getId(), Void.class);
    }

    private long getMaxMakeTime(List<MenuItemEntity> menuItems)
    {
        long max = 0;
        for (MenuItemEntity menuItem : menuItems) {
            if(menuItem.getProductEntity().getMakeTime() > max)
            {
                max = menuItem.getProductEntity().getMakeTime();
            }
        }

        return max;
    }


    private void saveUnsavedItems(MenuForm form, List<MenuItemEntity> menuItemsFromDb, MenuEntity entity)
    {
        if(form.getMenuItems() == null)
        {
            return;
        }

        for (MenuItemForm menuItemForm : form.getMenuItems())
        {
            boolean founded = false;
            for (MenuItemEntity menuItemEntity : menuItemsFromDb)
            {
                if(Long.valueOf(menuItemForm.getMenuItemId()) == menuItemEntity.getId())
                {
                    founded = true;
                    break;
                }
            }

            if(!founded)
            {
                ProductEntity productEntity = productRepository.findById(Long.valueOf(menuItemForm.getProductId())).get();
                MenuItemEntity menuItemEntity = new MenuItemEntity();
                menuItemEntity.setMenuEntity(entity);
                menuItemEntity.setProductEntity(productEntity);

                menuItemRepository.save(menuItemEntity);
            }
        }
    }

    private void deleteRemovedItems(MenuForm form, List<MenuItemEntity> menuItemsFromDb)
    {
        if(form.getMenuItems() == null)
        {
            form.setMenuItems(new ArrayList<>());
        }

        for (MenuItemEntity menuItemEntity : menuItemsFromDb)
        {
            boolean founded = false;
            for (MenuItemForm menuItemForm : form.getMenuItems())
            {
                if(menuItemEntity.getId() == Long.valueOf(menuItemForm.getMenuItemId()))
                {
                    founded = true;
                    break;
                }
            }

            if(!founded)
            {
                menuItemRepository.delete(menuItemEntity);
            }
        }
    }

    public List<MenuItemDTO> getMenuItemsDTO(long menuId)
    {
        List<MenuItemDTO> dtos = new ArrayList<>();
        MenuEntity menuEntity = menuRepository.findById(menuId).get();

        List<MenuItemEntity> menus = menuItemRepository.findAllByMenuEntity(menuEntity);
        for (MenuItemEntity menuItem : menus)
        {
            MenuItemDTO dto = new MenuItemDTO();
            dto.setMenuItemId(menuItem.getId());
            dto.setProductDTO(productService.convertProduct(menuItem.getProductEntity()));

            dtos.add(dto);
        }

        return dtos;
    }
    public List<MenuEntity> getAllMenuForName(String name)
    {
        List<MenuEntity> menuEntities=menuRepository.findAllByName(name);
        return menuEntities;
    }

    public List<MenuEntity> getMenus(StoreEntity storeEntity) {
        return menuRepository.findAllByStoreEntity(storeEntity);
    }
    private String encodeFileToBase64Binary(String fileName) throws IOException {
        File file = new File(fileName);
        byte[] encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(file));
        return new String(encoded, StandardCharsets.US_ASCII);
    }
}
