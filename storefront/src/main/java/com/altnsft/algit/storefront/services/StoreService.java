package com.altnsft.algit.storefront.services;

import org.springframework.stereotype.Service;

/**
 * Created by yusufaltun on 24.07.2018.
 */
@Service
public class StoreService extends BaseService
{
    public void updateStore(long storeId)
    {
        getRestTemplate().getForEntity(getBaseURL() +"/store/update/"+ storeId, Void.class);
    }
}
