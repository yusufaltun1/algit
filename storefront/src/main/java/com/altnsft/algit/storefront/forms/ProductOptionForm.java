package com.altnsft.algit.storefront.forms;

import java.util.List;

/**
 * Created by yusufaltun on 26.06.2018.
 */
public class ProductOptionForm
{
    private int optionId;
    private String optionName;
    private String optionType;

    private List<ProductOptionValueForm> optionValues;

    public List<ProductOptionValueForm> getOptionValues() {
        return optionValues;
    }

    public void setOptionValues(List<ProductOptionValueForm> optionValues) {
        this.optionValues = optionValues;
    }

    public String getOptionName() {
        return optionName;
    }

    public void setOptionName(String optionName) {
        this.optionName = optionName;
    }

    public String getOptionType() {
        return optionType;
    }

    public void setOptionType(String optionType) {
        this.optionType = optionType;
    }

    public int getOptionId() {
        return optionId;
    }

    public void setOptionId(int optionId) {
        this.optionId = optionId;
    }

}
