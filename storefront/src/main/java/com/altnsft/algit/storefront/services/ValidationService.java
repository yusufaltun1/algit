package com.altnsft.algit.storefront.services;

import com.altnsft.algit.storefront.forms.CategoryForm;
import com.altnsft.algit.storefront.validators.CategoryFormValidator;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.validation.Errors;

/**
 * Created by yusufaltun on 16.06.2018.
 */
@Service
public class ValidationService
{
    @Autowired
    private CategoryFormValidator categoryFormValidator;

    public void validate(Errors errors, CategoryForm categoryForm)
    {
        categoryFormValidator.validate(categoryForm, errors);
    }

}
