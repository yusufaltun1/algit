package com.altnsft.algit.storefront.services;

import com.altnsft.algit.repositories.entities.ProductOptionEntity;
import com.altnsft.algit.repositories.entities.ProductOptionValueEntity;
import com.altnsft.algit.repositories.repository.ProductOptionValueRepository;
import com.altnsft.algit.storefront.forms.ProductOptionValueForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;

/**
 * Created by yusufaltun on 26.06.2018.
 */
@Service
public class ProductOptionValueService extends BaseService
{
    @Autowired
    private ProductOptionValueRepository productOptionValueRepository;

    public void saveProductOptionValue(ProductOptionValueForm form, ProductOptionEntity productOption)
    {
        ProductOptionValueEntity entity = new ProductOptionValueEntity();
        entity.setCode(form.getCode());
        entity.setPrice(BigDecimal.valueOf(form.getPrice()));
        entity.setProductOptionEntity(productOption);
        entity.setText(form.getName());

        productOptionValueRepository.save(entity);

        getRestTemplate().getForEntity(getBaseURL() +"/store/update/"+ productOption.getProductEntity().getStoreEntity().getId(), Void.class);
    }
}
