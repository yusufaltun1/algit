package com.altnsft.algit.storefront.dto;

/**
 * Created by yusufaltun on 26.06.2018.
 */
public class ProductOptionValueDTO
{
    private int valueId;
    private String code;
    private String name;
    private Double price;

    public int getValueId() {
        return valueId;
    }

    public void setValueId(int valueId) {
        this.valueId = valueId;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }
}
