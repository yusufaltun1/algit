package com.altnsft.algit.storefront.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * Created by yusufaltun on 16.06.2018.
 */
@Controller
@RequestMapping("/main")
public class MainController
{
    @GetMapping("/all")
    public String getStores()
    {
        return "tables";
    }
}
