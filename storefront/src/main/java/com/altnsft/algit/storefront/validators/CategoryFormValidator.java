package com.altnsft.algit.storefront.validators;

import com.altnsft.algit.storefront.forms.CategoryForm;
import org.springframework.stereotype.Component;
import org.springframework.validation.Errors;

/**
 * Created by yusufaltun on 16.06.2018.
 */
@Component
public class CategoryFormValidator implements org.springframework.validation.Validator{
    @Override
    public boolean supports(Class<?> aClass) {
        return CategoryForm.class.equals(aClass);
    }

    @Override
    public void validate(Object o, Errors errors) {
        CategoryForm form = (CategoryForm) o;
        if (form.getName() == null || form.getName().isEmpty())
        {
            errors.rejectValue("name", "empty");
        }

        if (form.getTitle() == null || form.getTitle().isEmpty())
        {
            errors.rejectValue("title", "empty");
        }
    }
}
