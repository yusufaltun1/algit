package com.altnsft.algit.storefront.controller;

import com.altnsft.algit.storefront.dto.GalleryImagesForm;
import com.altnsft.algit.storefront.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * Created by yusufaltun on 30.08.2018.
 */
@Controller
@RequestMapping("/gallery")
public class UpdateGalleryImagesController extends BaseController
{
    @Autowired
    private UserService userService;

    @GetMapping("/edit")
    public String openEditPage(Model model)
    {
        GalleryImagesForm galleryImagesForm = new GalleryImagesForm();

        model.addAttribute("galleryImagesForm", galleryImagesForm);
        return "edit-gallery-images";
    }

    @PostMapping("/save")
    public String save(@ModelAttribute GalleryImagesForm galleryImagesForm)
    {
        for (String image : galleryImagesForm.getImages())
        {
            System.out.println(image);
        }
        return "redirect:/gallery/edit";
    }

    @Override
    protected UserService getUserService() {
        return null;
    }
}
