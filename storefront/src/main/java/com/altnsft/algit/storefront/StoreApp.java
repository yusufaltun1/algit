package com.altnsft.algit.storefront;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

@SpringBootApplication
@EntityScan( basePackages = {"com.altnsft.algit.repositories.entities"} )
@EnableJpaRepositories(basePackages = {"com.altnsft.algit.repositories.repository"})
@ComponentScan({"com.altnsft.algit.storefront.controller", "com.altnsft.algit.storefront.services", "com.altnsft.algit.storefront.validators",  "com.altnsft.algit.storefront.security"})
public class StoreApp extends SpringBootServletInitializer
{

	public static void main(String[] args)
	{
		SpringApplication.run(StoreApp.class, args);
	}

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder builder) {
		System.out.println("initializing");
		return builder.sources(StoreApp.class);
	}
}
