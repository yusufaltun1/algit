package com.altnsft.algit.storefront.forms;

public class ProductSearchForm {

    String productName;


    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }
}
