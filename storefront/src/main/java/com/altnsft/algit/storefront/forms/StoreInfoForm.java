package com.altnsft.algit.storefront.forms;

import org.springframework.web.multipart.MultipartFile;

/**
 * Created by yusufaltun on 2.07.2018.
 */
public class StoreInfoForm
{
    private int id;

    private String address;

    private String mapLat;
    private String mapLng;
    private String description;

    private MultipartFile image;

    private String mondayOpentime;

    private String mondayCloseTime;

    private String tuesdayOpenTime;

    private String tuesdayCloseTime;

    private String wednesdayOpenTime;

    private String wednesdayCloseTime;

    private String thursdayOpenTime;

    private String thursdayCloseTime;

    private String fridayOpenTime;

    private String fridayCloseTime;

    private String saturdayOpenTime;

    private String saturdayCloseTime;

    private String sundayOpenTime;

    private String sundayCloseTime;

    public String getMondayOpentime() {
        return mondayOpentime;
    }

    public void setMondayOpentime(String mondayOpentime) {
        this.mondayOpentime = mondayOpentime;
    }

    public String getMondayCloseTime() {
        return mondayCloseTime;
    }

    public void setMondayCloseTime(String mondayCloseTime) {
        this.mondayCloseTime = mondayCloseTime;
    }

    public String getTuesdayOpenTime() {
        return tuesdayOpenTime;
    }

    public void setTuesdayOpenTime(String tuesdayOpenTime) {
        this.tuesdayOpenTime = tuesdayOpenTime;
    }

    public String getTuesdayCloseTime() {
        return tuesdayCloseTime;
    }

    public void setTuesdayCloseTime(String tuesdayCloseTime) {
        this.tuesdayCloseTime = tuesdayCloseTime;
    }

    public String getWednesdayOpenTime() {
        return wednesdayOpenTime;
    }

    public void setWednesdayOpenTime(String wednesdayOpenTime) {
        this.wednesdayOpenTime = wednesdayOpenTime;
    }

    public String getWednesdayCloseTime() {
        return wednesdayCloseTime;
    }

    public void setWednesdayCloseTime(String wednesdayCloseTime) {
        this.wednesdayCloseTime = wednesdayCloseTime;
    }

    public String getThursdayOpenTime() {
        return thursdayOpenTime;
    }

    public void setThursdayOpenTime(String thusdayOpenTime) {
        this.thursdayOpenTime = thusdayOpenTime;
    }

    public String getThursdayCloseTime() {
        return thursdayCloseTime;
    }

    public void setThusdayCloseTime(String thusdayCloseTime) {
        this.thursdayCloseTime = thusdayCloseTime;
    }

    public String getFridayOpenTime() {
        return fridayOpenTime;
    }

    public void setFridayOpenTime(String fridayOpenTime) {
        this.fridayOpenTime = fridayOpenTime;
    }

    public String getFridayCloseTime() {
        return fridayCloseTime;
    }

    public void setFridayCloseTime(String fridayCloseTime) {
        this.fridayCloseTime = fridayCloseTime;
    }

    public String getSaturdayOpenTime() {
        return saturdayOpenTime;
    }

    public void setSaturdayOpenTime(String saturdayOpenTime) {
        this.saturdayOpenTime = saturdayOpenTime;
    }

    public String getSaturdayCloseTime() {
        return saturdayCloseTime;
    }

    public void setSaturdayCloseTime(String saturdayCloseTime) {
        this.saturdayCloseTime = saturdayCloseTime;
    }

    public String getSundayOpenTime() {
        return sundayOpenTime;
    }

    public void setSundayOpenTime(String sundayOpenTime) {
        this.sundayOpenTime = sundayOpenTime;
    }

    public String getSundayCloseTime() {
        return sundayCloseTime;
    }

    public void setSundayCloseTime(String sundayCloseTime) {
        this.sundayCloseTime = sundayCloseTime;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    private Long maxCancelTime;

    public Long getMaxCancelTime() {
        return maxCancelTime;
    }

    public void setMaxCancelTime(Long maxCancelTime) {
        this.maxCancelTime = maxCancelTime;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMapLat() {
        return mapLat;
    }

    public void setMapLat(String mapLat) {
        this.mapLat = mapLat;
    }

    public String getMapLng() {
        return mapLng;
    }

    public void setMapLng(String mapLng) {
        this.mapLng = mapLng;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setThursdayCloseTime(String thursdayCloseTime) {
        this.thursdayCloseTime = thursdayCloseTime;
    }
}
