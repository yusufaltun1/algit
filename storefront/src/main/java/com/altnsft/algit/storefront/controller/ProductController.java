package com.altnsft.algit.storefront.controller;

import com.altnsft.algit.repositories.entities.CategoryEntity;
import com.altnsft.algit.repositories.entities.ProductEntity;
import com.altnsft.algit.repositories.entities.UserEntity;
import com.altnsft.algit.storefront.dto.ProductDTO;
import com.altnsft.algit.storefront.forms.ProductForm;
import com.altnsft.algit.storefront.forms.ProductSearchForm;
import com.altnsft.algit.storefront.services.CategoryService;
import com.altnsft.algit.storefront.services.ProductService;
import com.altnsft.algit.storefront.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.util.List;

/**
 * Created by yusufaltun on 20.06.2018.
 */
@Controller
@RequestMapping("/product")
public class ProductController extends BaseController
{
    @Autowired
    private UserService userService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private ProductService productService;

    @RequestMapping(value = "/save", method = RequestMethod.GET)
    public String gotoSaveProductPage(Model model)
    {
        UserEntity currentUser = getCurrentUser();

        List<CategoryEntity> categories = categoryService.getAllCategoriesByStore(currentUser.getStoreEntity());

        model.addAttribute("categories", categories);
        model.addAttribute("product", new ProductForm());

        return "create-product";
    }

    @RequestMapping(method = RequestMethod.GET)
    public String listProduct(Model model)
    {
        UserEntity currentUser = getCurrentUser();
        List<ProductEntity> products = productService.getAllProducts(currentUser.getStoreEntity());
        model.addAttribute("products", products);
        model.addAttribute("productSearchForm",new ProductSearchForm());

        return "list-product";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveProduct(@ModelAttribute("category") @Valid ProductForm form, BindingResult bindingResult, Model model, RedirectAttributes redirectAttributes)
    {
        UserEntity currentUser = getCurrentUser();
        if (bindingResult.hasErrors())
        {
            return "create-product";
        }

        productService.save(form, currentUser.getStoreEntity());

        redirectAttributes.addFlashAttribute("globalMessage", new GlobalMessage("Urun kayit edildi"));
        return "redirect:/product";
    }

    @RequestMapping(value = "/edit/{productId}", method = RequestMethod.GET)
    public String editProduct(@PathVariable long productId, Model model, RedirectAttributes redirectAttributes)
    {
        UserEntity currentUser = getCurrentUser();
        ProductForm productForm = productService.getProduct(productId);
        List<CategoryEntity> categories = categoryService.getAllCategoriesByStore(currentUser.getStoreEntity());

        model.addAttribute("categories", categories);
        model.addAttribute("product", productForm);

        return "create-product";
    }

    @RequestMapping(value = "/all", method = RequestMethod.GET)
    @ResponseBody
    public List<ProductDTO> getAllProducts()
    {
        UserEntity currentUser = getCurrentUser();
        return productService.getAllProductsForDTO(currentUser.getStoreEntity());
    }
    @RequestMapping(value = "/search",method=RequestMethod.POST)
    public String getSearchProduct(@ModelAttribute("category") @Valid ProductSearchForm ProductSearchForm, Model model)
    {
        List<ProductEntity> productEntities=productService.getProductForName(ProductSearchForm.getProductName());
        model.addAttribute("products",productEntities);
        model.addAttribute("productSearchForm",new ProductSearchForm());
        return "list-product";
    }

    @Override
    protected UserService getUserService() {
        return userService;
    }
}
