package com.altnsft.algit.storefront.forms;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;

/**
 * Created by yusufaltun on 16.06.2018.
 */
public class CategoryForm implements Form
{
    private long id;

    @NotNull
    @Size(min=3, max=30)
    private String name;
    @NotNull
    @Size(min=3, max=30)
    private String title;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
