package com.altnsft.algit.storefront.services;

import com.altnsft.algit.repositories.entities.ProductEntity;
import com.altnsft.algit.repositories.entities.ProductOptionEntity;
import com.altnsft.algit.repositories.entities.ProductOptionType;
import com.altnsft.algit.repositories.repository.ProductOptionRepository;
import com.altnsft.algit.storefront.forms.ProductOptionForm;
import com.altnsft.algit.storefront.forms.ProductOptionValueForm;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by yusufaltun on 26.06.2018.
 */
@Service
public class ProductOptionService extends BaseService
{
    @Autowired
    private ProductOptionRepository productOptionRepository;

    @Autowired
    private ProductOptionValueService productOptionValueService;

    public void saveProductOptions(ProductOptionForm productOptionForm, ProductEntity productEntity)
    {
        ProductOptionEntity entity = new ProductOptionEntity();
        entity.setName(productOptionForm.getOptionName());

        ProductOptionType productOptionType = ProductOptionType.valueOf(productOptionForm.getOptionType());
        entity.setOptionType(productOptionType);
        entity.setProductEntity(productEntity);
        entity = productOptionRepository.save(entity);

        for (ProductOptionValueForm productOptionValueForm : productOptionForm.getOptionValues()) {
            productOptionValueService.saveProductOptionValue(productOptionValueForm, entity);
        }

        getRestTemplate().getForEntity(getBaseURL() +"/store/update/"+ productEntity.getStoreEntity().getId(), Void.class);
    }
}
