package com.altnsft.algit.storefront.forms;

import com.altnsft.algit.storefront.annotations.ValidLongValue;
import org.springframework.web.multipart.MultipartFile;

import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * Created by yusufaltun on 23.06.2018.
 */

public class MenuForm
{
    private Long id;
    @NotNull
    @Size(min = 2)
    private String name;
    @NotNull
    private Double price;
    @NotNull
    @Size(min = 2)
    private String title;
    @Size(min = 2)
    private String description;

    private List<MenuItemForm> menuItems;
    @NotNull
    private boolean sellable;
    @NotNull
    private long categoryId;

    private long foodCategoryId;

    private MultipartFile image;
    private String imageStr;

    public long getFoodCategoryId() {
        return foodCategoryId;
    }

    public void setFoodCategoryId(long foodCategoryId) {
        this.foodCategoryId = foodCategoryId;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageStr() {
        return imageStr;
    }

    public void setImageStr(String imageStr) {
        this.imageStr = imageStr;
    }

    public MultipartFile getImage() {
        return image;
    }

    public void setImage(MultipartFile image) {
        this.image = image;
    }

    public long getCategoryId() {
        return categoryId;
    }

    public void setCategoryId(long categoryId) {
        this.categoryId = categoryId;
    }

    public boolean isSellable() {
        return sellable;
    }

    public void setSellable(boolean sellable) {
        this.sellable = sellable;
    }

    public List<MenuItemForm> getMenuItems() {
        return menuItems;
    }

    public void setMenuItems(List<MenuItemForm> menuItems) {
        this.menuItems = menuItems;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
