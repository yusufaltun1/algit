package com.altnsft.algit.storefront.controller;

import com.altnsft.algit.storefront.dto.ProductOptionDTO;
import com.altnsft.algit.storefront.services.ProductOptionService;
import com.altnsft.algit.storefront.services.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

/**
 * Created by yusufaltun on 26.06.2018.
 */
@Controller
@RequestMapping("/option")
public class ProductOptionController
{
    @Autowired
    private ProductOptionService productOptionService;
    @Autowired
    private ProductService productService;

    @GetMapping("/get/{productId}")
    @ResponseBody
    private List<ProductOptionDTO> getOptions(@PathVariable int productId)
    {
        return productService.getAllOptions(productId);
    }
}
