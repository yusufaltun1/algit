package com.altnsft.algit.storefront.controller;

import com.altnsft.algit.repositories.entities.CategoryEntity;
import com.altnsft.algit.repositories.entities.UserEntity;
import com.altnsft.algit.storefront.forms.CategoryForm;
import com.altnsft.algit.storefront.forms.CategorySearhForm;
import com.altnsft.algit.storefront.services.CategoryService;
import com.altnsft.algit.storefront.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;
import java.util.List;

/**
 * Created by yusufaltun on 16.06.2018.
 */
@Controller
@RequestMapping("/category")
public class CategoryController extends BaseController
{
    @Autowired
    private CategoryService categoryService;

    @Autowired
    private UserService userService;

    @GetMapping
    public String listCategory(Model model, HttpServletRequest request)
    {
        UserEntity currentUser = getCurrentUser();

        List<CategoryEntity> categories = categoryService.getAllCategoriesByStore(currentUser.getStoreEntity());
        model.addAttribute("categories", categories);
        model.addAttribute("categorySeachForm",new CategorySearhForm());
        return "list-category";
    }

    @RequestMapping(value = "/save", method = RequestMethod.GET)
    public String createCategoryPage(Model model)
    {
        model.addAttribute("category", new CategoryForm());

        return "create-category";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String createCategory(@ModelAttribute("category") @Valid CategoryForm form, BindingResult bindingResult, RedirectAttributes redirectAttributes)
    {
        if(bindingResult.hasErrors())
        {
            return "create-category";
        }
        UserEntity currentUser = getCurrentUser();

        categoryService.save(form, currentUser.getStoreEntity());
        redirectAttributes.addFlashAttribute("globalMessage", new GlobalMessage("Kategori kayit edildi"));
        return "redirect:/category";
    }

    @RequestMapping("/remove/{categoryId}")
    public String deleteCategory(@PathVariable("categoryId") int categoryId, RedirectAttributes redirectAttributes)
    {
        categoryService.remove(categoryId);
        redirectAttributes.addFlashAttribute("globalMessage", new GlobalMessage("Kategori silindi"));
        return "redirect:/category";
    }

    @RequestMapping("/edit/{categoryId}")
    public String edit(@PathVariable("categoryId") int categoryId, Model model, RedirectAttributes redirectAttributes)
    {
        CategoryEntity entity = categoryService.getCategoryById(categoryId);
        model.addAttribute("category", getCategoryForm(entity));
        //categoryService.remove(categoryId);
        return "create-category";
    }
    @RequestMapping(value = "/search",method=RequestMethod.POST)
    public String getSearchCategory(@ModelAttribute("categorySeachForm") @Valid CategorySearhForm CategorySearhForm, Model model)
    {
            List<CategoryEntity> categoryEntities=categoryService.getAllCategoryForName(CategorySearhForm.getCategoryName());
            model.addAttribute("categories",categoryEntities);
            model.addAttribute("categorySeachForm",new CategorySearhForm());
            return "list-category";
    }

    @Override
    protected UserService getUserService() {
        return userService;
    }

    private CategoryForm getCategoryForm(CategoryEntity categoryEntity)
    {
        CategoryForm categoryForm = new CategoryForm();
        categoryForm.setId(categoryEntity.getId());
        categoryForm.setName(categoryEntity.getName());
        categoryForm.setTitle(categoryEntity.getText());
        return categoryForm;
    }
}
