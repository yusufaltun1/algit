package com.altnsft.algit.storefront.services;

import com.altnsft.algit.repositories.entities.UserEntity;
import com.altnsft.algit.repositories.entities.UserRoleEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 17.06.2018.
 */

@Service
public class LoginService implements UserDetailsService{

    @Autowired
    private UserService userService;

    @Override
    public UserDetails loadUserByUsername(String s) throws UsernameNotFoundException {
        UserEntity userEntity = userService.findUserByEmail(s);

        if(userEntity == null)
        {
            throw new UsernameNotFoundException("username was not found in the database");
        }

        List<UserRoleEntity> roles = userService.getRoles(userEntity);
        List<GrantedAuthority> grantList = new ArrayList<>();

        if(roles != null)
        {
            for (UserRoleEntity role : roles)
            {
                GrantedAuthority authority = new SimpleGrantedAuthority(role.getRole().getRole());
                grantList.add(authority);
            }
        }

        UserDetails userDetails = new User(userEntity.getEmail(), userEntity.getPassword(), grantList);

        return userDetails;
    }
}
