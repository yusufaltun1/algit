package com.altnsft.algit.storefront.services;

import com.altnsft.algit.repositories.entities.*;
import com.altnsft.algit.repositories.repository.CategoryRepository;
import com.altnsft.algit.repositories.repository.ProductOptionRepository;
import com.altnsft.algit.repositories.repository.ProductOptionValueRepository;
import com.altnsft.algit.repositories.repository.ProductRepository;
import com.altnsft.algit.storefront.dto.ProductDTO;
import com.altnsft.algit.storefront.dto.ProductOptionDTO;
import com.altnsft.algit.storefront.dto.ProductOptionValueDTO;
import com.altnsft.algit.storefront.forms.ProductForm;
import com.altnsft.algit.storefront.forms.ProductOptionForm;
import org.apache.catalina.Store;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 21.06.2018.
 */
@Service
public class ProductService extends BaseService
{
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private ProductOptionRepository productOptionRepository;

    @Autowired
    private ProductOptionService productOptionService;

    @Autowired
    private ProductOptionValueRepository productOptionValueRepository;

    public void save(ProductForm form, StoreEntity storeEntity)
    {
        boolean isNew = false;
        ProductEntity productEntity  = productRepository.findById(form.getId()).orElse(new ProductEntity());

        if(productEntity.getId() == null)
        {
            isNew = true;
        }

        productEntity.setCode(form.getCode());
        productEntity.setDescription(form.getDescription());
        productEntity.setMakeTime(form.getMakeTime());
        productEntity.setName(form.getName());
        productEntity.setPrice(BigDecimal.valueOf(form.getPrice()));
        productEntity.setSellable(form.isSellable());
        productEntity.setStoreEntity(storeEntity);
        productEntity.setTitle(form.getTitle());

        CategoryEntity category = categoryRepository.findById(form.getCategoryId()).get();
        productEntity.setCategoryEntity(category);

        productEntity = productRepository.save(productEntity);

        if(isNew)
        {
            if(form.getOptions() != null)
            {
                for (ProductOptionForm productOptionForm : form.getOptions())
                {
                    productOptionService.saveProductOptions(productOptionForm, productEntity);
                }
            }
        }
        else
        {
            List<ProductOptionEntity> optionsFromDB = productOptionRepository.findAllByProductEntity(productEntity);

            addOptionIfNeeded(form, optionsFromDB, productEntity);

            removeOptionIfNeeded(form, optionsFromDB);
        }

        getRestTemplate().getForEntity(getBaseURL() +"/store/update/"+ storeEntity.getId(), Void.class);
    }

    private void removeOptionIfNeeded(ProductForm form, List<ProductOptionEntity> optionsFromDB)
    {
        for (ProductOptionEntity entity : optionsFromDB)
        {
            boolean founded = false;
            for (ProductOptionForm productOptionForm : form.getOptions())
            {
                if (entity.getId().intValue() == productOptionForm.getOptionId())
                {
                    founded = true;
                    break;
                }
            }

            if(!founded)
            {
                productOptionRepository.delete(entity);
            }
        }
    }
    private void addOptionIfNeeded(ProductForm form, List<ProductOptionEntity> optionsFromDB, ProductEntity productEntity)
    {
        if(CollectionUtils.isEmpty(form.getOptions()))
        {
            return;
        }
        for (ProductOptionForm productOptionForm : form.getOptions())
        {
            boolean founded = false;
            for (ProductOptionEntity entity : optionsFromDB)
            {
                if (productOptionForm.getOptionId() == entity.getId().intValue())
                {
                    founded = true;
                    break;
                }
            }

            if(!founded)
            {
                productOptionService.saveProductOptions(productOptionForm, productEntity);
            }
        }
    }

    public ProductForm getProduct(long id)
    {
        ProductEntity entity = productRepository.findById(id).get();
        ProductForm productForm = new ProductForm();

        productForm.setId(entity.getId());
        productForm.setCategoryId(entity.getCategoryEntity().getId());
        productForm.setCode(entity.getCode());
        productForm.setDescription(entity.getDescription());
        productForm.setMakeTime(entity.getMakeTime());
        productForm.setName(entity.getName());
        productForm.setPrice(entity.getPrice().doubleValue());
        productForm.setSellable(entity.isSellable());
        productForm.setTitle(entity.getTitle());
        return productForm;
    }

    public List<ProductEntity> getAllProducts(StoreEntity storeEntity) {
        return productRepository.findAllByStoreEntity(storeEntity);
    }

    public List<ProductDTO> getAllProductsForDTO(StoreEntity storeEntity)
    {
        List<ProductEntity> allProducts = getAllProducts(storeEntity);
        List<ProductDTO> dtos = new ArrayList<>();

        for (ProductEntity entity : allProducts)
        {
            ProductDTO productDTO = convertProduct(entity);

            dtos.add(productDTO);
        }

        return dtos;
    }

    public ProductDTO convertProduct(ProductEntity entity)
    {
        ProductDTO dto = new ProductDTO();
        dto.setId(entity.getId());
        dto.setName(entity.getName());

        return dto;
    }

    public List<ProductOptionDTO> getAllOptions(int productId)
    {
        ProductEntity entity = productRepository.findById(Long.valueOf(productId)).get();

        List<ProductOptionEntity> optionEntities = productOptionRepository.findAllByProductEntity(entity);
        List<ProductOptionDTO> optionDTOS = new ArrayList<>();
        for (ProductOptionEntity optionEntity : optionEntities)
        {
            List<ProductOptionValueEntity> valueEntities = productOptionValueRepository.findAllByProductOptionEntity(optionEntity);
            ProductOptionDTO productOptionDTO = new ProductOptionDTO();
            productOptionDTO.setOptionId(optionEntity.getId().intValue());
            productOptionDTO.setOptionName(optionEntity.getName());
            productOptionDTO.setOptionType(optionEntity.getOptionType().name());
            productOptionDTO.setOptionValues(new ArrayList<>());

            for (ProductOptionValueEntity valueEntity : valueEntities)
            {
                ProductOptionValueDTO valueDTO = new ProductOptionValueDTO();
                valueDTO.setCode(valueDTO.getCode());
                valueDTO.setName(valueEntity.getText());
                valueDTO.setPrice(valueEntity.getPrice().doubleValue());
                valueDTO.setValueId(valueEntity.getId().intValue());

                productOptionDTO.getOptionValues().add(valueDTO);
            }
            optionDTOS.add(productOptionDTO);
        }

        return optionDTOS;
    }
    public List<ProductEntity> getProductForName(String productName)
    {
        List<ProductEntity> productEntityList=productRepository.findAllByName(productName);
        return productEntityList;
    }
}
