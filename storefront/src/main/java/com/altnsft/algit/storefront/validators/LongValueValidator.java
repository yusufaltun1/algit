package com.altnsft.algit.storefront.validators;

import com.altnsft.algit.storefront.annotations.ValidLongValue;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LongValueValidator implements
        ConstraintValidator<ValidLongValue, String> {
 
    @Override
    public void initialize(ValidLongValue contactNumber) {
    }

    @Override
    public boolean isValid(String s, ConstraintValidatorContext constraintValidatorContext) {
        return true;
    }
}