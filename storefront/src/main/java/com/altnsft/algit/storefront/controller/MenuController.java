package com.altnsft.algit.storefront.controller;

import com.altnsft.algit.repositories.entities.CategoryEntity;
import com.altnsft.algit.repositories.entities.FoodCategoryEntity;
import com.altnsft.algit.repositories.entities.MenuEntity;
import com.altnsft.algit.repositories.entities.UserEntity;
import com.altnsft.algit.repositories.repository.FoodCategoryRepository;
import com.altnsft.algit.storefront.dto.MenuItemDTO;
import com.altnsft.algit.storefront.forms.MenuForm;
import com.altnsft.algit.storefront.forms.MenuSearhForm;
import com.altnsft.algit.storefront.services.CategoryService;
import com.altnsft.algit.storefront.services.MenuService;
import com.altnsft.algit.storefront.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import javax.validation.Valid;
import java.io.IOException;
import java.util.List;

/**
 * Created by yusufaltun on 23.06.2018.
 */
@Controller
@RequestMapping("/menu")
public class MenuController extends BaseController {

    @Autowired
    private UserService userService;

    @Autowired
    private MenuService menuService;

    @Autowired
    private CategoryService categoryService;

    @Autowired
    private FoodCategoryRepository foodCategoryRepository;

    @RequestMapping(value = "/save", method = RequestMethod.GET)
    public String gotoSaveProductPage(Model model)
    {
        UserEntity currentUser = getCurrentUser();

        List<CategoryEntity> categories = categoryService.getAllCategoriesByStore(currentUser.getStoreEntity());
        Iterable<FoodCategoryEntity> foodCategoryEntities = foodCategoryRepository.findAll();
        MenuForm form = new MenuForm();
        form.setId(0);
        model.addAttribute("categories", categories);
        model.addAttribute("menu", form);
        model.addAttribute("foodCategories", foodCategoryEntities);

        return "create-menu";
    }

    @RequestMapping(value = "/save", method = RequestMethod.POST)
    public String saveMenu(@ModelAttribute("menu") @Valid MenuForm form, BindingResult bindingResult, RedirectAttributes redirectAttributes) throws IOException {
        UserEntity currentUser = getCurrentUser();

        menuService.save(form, currentUser.getStoreEntity());

        redirectAttributes.addFlashAttribute("globalMessage", new GlobalMessage("Menu kayit edildi"));

        return "redirect:/menu";
    }

    @RequestMapping(value = "/edit/{menuId}", method = RequestMethod.GET)
    public String editMenuPage(@PathVariable long menuId,   Model model)
    {
        UserEntity currentUser = getCurrentUser();
        MenuForm form = menuService.getMenu(menuId, currentUser.getStoreEntity());
        List<CategoryEntity> categories = categoryService.getAllCategoriesByStore(currentUser.getStoreEntity());
        Iterable<FoodCategoryEntity> foodCategoryEntities = foodCategoryRepository.findAll();

        model.addAttribute("categories", categories);
        model.addAttribute("menu", form);
        model.addAttribute("foodCategories", foodCategoryEntities);
        return "create-menu";
    }

    @GetMapping
    public String list(Model model)
    {
        UserEntity currentUser = getCurrentUser();
        List<MenuEntity> menus = menuService.getMenus(currentUser.getStoreEntity());
        model.addAttribute("menus", menus);
        model.addAttribute("menuSearchForm",new MenuSearhForm());

        return "list-menu";
    }

    @RequestMapping(value = "/menu-items/{menuId}", method = RequestMethod.GET)
    @ResponseBody
    public List<MenuItemDTO> getMenuItems(@PathVariable long menuId,   Model model)
    {
        return menuService.getMenuItemsDTO(menuId);
    }
    @RequestMapping(value = "/search",method=RequestMethod.POST)
    public String getSearchMenu(@ModelAttribute("menuSeachForm") @Valid MenuSearhForm MenuSearhForm, Model model)
    {
        List<MenuEntity> menuEntities=menuService.getAllMenuForName(MenuSearhForm.getMenuName());
        model.addAttribute("menus",menuEntities);
        model.addAttribute("menuSearchForm",new MenuSearhForm());
        return "list-menu";
    }

    @Override
    protected UserService getUserService() {
        return userService;
    }
}
