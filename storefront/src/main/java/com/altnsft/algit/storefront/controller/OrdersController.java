package com.altnsft.algit.storefront.controller;

import com.altnsft.algit.models.dtos.OrderDTO;
import com.altnsft.algit.repositories.entities.OrderEntity;
import com.altnsft.algit.repositories.entities.StoreEntity;
import com.altnsft.algit.storefront.services.OrderService;
import com.altnsft.algit.storefront.services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.util.List;

/**
 * Created by yusufaltun on 22.07.2018.
 */
@Controller
@RequestMapping("/orders")
public class OrdersController extends BaseController
{
    @Autowired
    private UserService userService;

    @Autowired
    private OrderService orderService;

    @GetMapping
    public String gotoOrdersPage(Model model)
    {
        StoreEntity storeEntity = getCurrentUser().getStoreEntity();
        List<OrderDTO> orders = orderService.getAllOrders(storeEntity.getId());
        model.addAttribute("orders", orders);

        return "orders";
    }

    @GetMapping("/{order-id}")
    public String gotOrderDetailPage(@PathVariable(name = "order-id") long orderId,  Model model)
    {
        OrderDTO order = orderService.getOrder(orderId);
        model.addAttribute("order", order);

        return "order-detail";
    }

    @GetMapping("/to-prepairing/{order-id}")
    public String toPrepairing(Model model, @PathVariable(name = "order-id") long orderId, RedirectAttributes redirectAttributes)
    {
        orderService.toPrepairing(orderId);
        redirectAttributes.addFlashAttribute("globalMessage", new GlobalMessage("Siparis guncellendi"));
        return "redirect:/orders";
    }

    @GetMapping("/to-ready/{order-id}")
    public String toReady(Model model, @PathVariable(name = "order-id") long orderId, RedirectAttributes redirectAttributes)
    {
        orderService.toReady(orderId);
        redirectAttributes.addFlashAttribute("globalMessage", new GlobalMessage("Siparis guncellendi"));
        return "redirect:/orders";
    }

    @GetMapping("/to-delivered/{order-id}")
    public String toDelivered(Model model, @PathVariable(name = "order-id") long orderId, RedirectAttributes redirectAttributes)
    {
        orderService.toDelivered(orderId);
        redirectAttributes.addFlashAttribute("globalMessage", new GlobalMessage("Siparis guncellendi"));
        return "redirect:/orders";
    }


    @Override
    protected UserService getUserService() {
        return userService;
    }
}
