package com.altnsft.algit.storefront.validators;

import com.altnsft.algit.storefront.forms.Form;
import org.springframework.validation.Errors;


/**
 * Created by yusufaltun on 16.06.2018.
 */
public interface Validator<T extends Form>
{
    Errors validate(Errors bindingResult, T form);
}
