package com.altnsft.algit.storefront.controller;

import com.altnsft.algit.repositories.entities.CategoryEntity;
import com.altnsft.algit.repositories.entities.StoreAvailableTimesEntity;
import com.altnsft.algit.repositories.entities.StoreEntity;
import com.altnsft.algit.repositories.entities.StoreInfoEntity;
import com.altnsft.algit.repositories.repository.StoreAvailableTimeRepository;
import com.altnsft.algit.repositories.repository.StoreInfoRepository;
import com.altnsft.algit.storefront.forms.StoreInfoForm;
import com.altnsft.algit.storefront.services.StoreService;
import com.altnsft.algit.storefront.services.UserService;
import org.apache.commons.fileupload.disk.DiskFileItem;
import org.apache.commons.io.FileUtils;
import org.apache.commons.io.FilenameUtils;
import org.apache.tomcat.util.codec.binary.Base64;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.commons.CommonsMultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.charset.StandardCharsets;

/**
 * Created by yusufaltun on 2.07.2018.
 */
@Controller
@RequestMapping("/store-info")
public class StoreInfoController extends BaseController
{
    @Autowired
    private UserService userService;

    @Autowired
    private StoreInfoRepository storeInfoRepository;

    @Autowired
    private StoreService storeService;

    @Autowired
    private StoreAvailableTimeRepository storeAvailableTimeRepository;

    @RequestMapping("/edit")
    public String edit(Model model)
    {
        StoreEntity storeEntity = getCurrentUser().getStoreEntity();
        StoreAvailableTimesEntity storeAvailableTimesEntity = storeAvailableTimeRepository.findByStoreEntity(storeEntity);
        StoreInfoEntity storeInfoEntity = storeInfoRepository.findByStore(storeEntity);
        if(storeInfoEntity == null)
        {
            model.addAttribute("storeInfo", new StoreInfoForm());
        }
        else
        {
            StoreInfoForm storeInfoForm = new StoreInfoForm();
            storeInfoForm.setId(storeInfoEntity.getId().intValue());
            storeInfoForm.setAddress(storeInfoEntity.getAddress());
            storeInfoForm.setMapLat(String.valueOf(storeInfoEntity.getMapLat()));
            storeInfoForm.setMapLng(String.valueOf(storeInfoEntity.getMapLng()));
            storeInfoForm.setMaxCancelTime(storeInfoEntity.getMaxCancelTime());

            if(storeAvailableTimesEntity != null)
            {
                storeInfoForm.setMondayOpentime(storeAvailableTimesEntity.getMonday().split("-")[0]);
                storeInfoForm.setMondayCloseTime(storeAvailableTimesEntity.getMonday().split("-")[1]);
                storeInfoForm.setTuesdayOpenTime(storeAvailableTimesEntity.getTuesday().split("-")[0]);
                storeInfoForm.setTuesdayCloseTime(storeAvailableTimesEntity.getTuesday().split("-")[1]);
                storeInfoForm.setThursdayOpenTime(storeAvailableTimesEntity.getThursday().split("-")[0]);
                storeInfoForm.setThusdayCloseTime(storeAvailableTimesEntity.getThursday().split("-")[1]);
                storeInfoForm.setWednesdayOpenTime(storeAvailableTimesEntity.getWednesday().split("-")[0]);
                storeInfoForm.setWednesdayCloseTime(storeAvailableTimesEntity.getWednesday().split("-")[1]);
                storeInfoForm.setFridayOpenTime(storeAvailableTimesEntity.getFriday().split("-")[0]);
                storeInfoForm.setFridayCloseTime(storeAvailableTimesEntity.getFriday().split("-")[1]);
                storeInfoForm.setSaturdayOpenTime(storeAvailableTimesEntity.getSaturday().split("-")[0]);
                storeInfoForm.setSaturdayCloseTime(storeAvailableTimesEntity.getSaturday().split("-")[1]);
                storeInfoForm.setSundayOpenTime(storeAvailableTimesEntity.getSunday().split("-")[0]);
                storeInfoForm.setSundayCloseTime(storeAvailableTimesEntity.getSunday().split("-")[1]);
            }

            model.addAttribute("storeInfo", storeInfoForm);
        }

        //categoryService.remove(categoryId);
        return "update_store_info";
    }

    @PostMapping("/update")
    public String edit(@ModelAttribute StoreInfoForm storeInfoForm) throws IOException {
        StoreEntity storeEntity = getCurrentUser().getStoreEntity();
        String file = "/tmpimg/store/store-img-"+storeEntity.getId() +"."+ FilenameUtils.getExtension(storeInfoForm.getImage().getOriginalFilename());

        StoreInfoEntity storeInfoEntity = storeInfoRepository.findByStore(storeEntity);
        StoreAvailableTimesEntity storeAvailableTimesEntity = storeAvailableTimeRepository.findByStoreEntity(storeEntity);
        if(storeAvailableTimesEntity == null)
        {
            storeAvailableTimesEntity = new StoreAvailableTimesEntity();
        }
        if(storeInfoEntity == null)
        {
            storeInfoEntity = new StoreInfoEntity();
        }
        storeInfoEntity.setAddress(storeInfoForm.getAddress());
        storeInfoEntity.setMapLat(Float.valueOf(storeInfoForm.getMapLat()));
        storeInfoEntity.setMapLng(Float.valueOf(storeInfoForm.getMapLng()));
        //storeInfoEntity.setImage(encodeFileToBase64Binary(file));
        storeInfoEntity.setStore(storeEntity);
        storeInfoEntity.setMaxCancelTime(storeInfoForm.getMaxCancelTime());
        storeInfoEntity.setDescription(storeInfoForm.getDescription());

        storeAvailableTimesEntity.setMonday(storeInfoForm.getMondayOpentime() + "-" + storeInfoForm.getMondayCloseTime());
        storeAvailableTimesEntity.setTuesday(storeInfoForm.getTuesdayOpenTime() + "-" + storeInfoForm.getTuesdayCloseTime());
        storeAvailableTimesEntity.setWednesday(storeInfoForm.getWednesdayOpenTime() + "-" + storeInfoForm.getWednesdayCloseTime());
        storeAvailableTimesEntity.setThursday(storeInfoForm.getThursdayOpenTime() + "-" + storeInfoForm.getThursdayCloseTime());
        storeAvailableTimesEntity.setFriday(storeInfoForm.getFridayOpenTime() + "-" + storeInfoForm.getFridayCloseTime());
        storeAvailableTimesEntity.setSaturday(storeInfoForm.getSaturdayOpenTime() + "-" + storeInfoForm.getSaturdayCloseTime());
        storeAvailableTimesEntity.setSunday(storeInfoForm.getSundayOpenTime() + "-" + storeInfoForm.getSundayCloseTime());
        storeAvailableTimesEntity.setStoreEntity(storeEntity);

        storeAvailableTimeRepository.save(storeAvailableTimesEntity);

        storeInfoEntity = storeInfoRepository.save(storeInfoEntity);

        try {
            storeInfoForm.getImage().transferTo(new File(file));
        } catch (IOException e) {
            e.printStackTrace();
        }

        storeService.updateStore(storeEntity.getId());

        return "redirect:/store-info/edit";
    }

    private String encodeFileToBase64Binary(String fileName) throws IOException {
        File file = new File(fileName);
        byte[] encoded = Base64.encodeBase64(FileUtils.readFileToByteArray(file));
        return new String(encoded, StandardCharsets.US_ASCII);
    }

    @Override
    protected UserService getUserService() {
        return userService;
    }
}
