package com.altnsft.algit.storefront.dto;

/**
 * Created by yusufaltun on 23.06.2018.
 */
public class ProductDTO
{
    private long id;
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
