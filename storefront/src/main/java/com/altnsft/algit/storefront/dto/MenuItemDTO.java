package com.altnsft.algit.storefront.dto;

/**
 * Created by yusufaltun on 23.06.2018.
 */
public class MenuItemDTO
{
    private long menuItemId;
    private ProductDTO productDTO;

    public long getMenuItemId()
    {
        return menuItemId;
    }

    public void setMenuItemId(long menuItemId)
    {
        this.menuItemId = menuItemId;
    }

    public ProductDTO getProductDTO()
    {
        return productDTO;
    }

    public void setProductDTO(ProductDTO productDTO)
    {
        this.productDTO = productDTO;
    }
}
