package com.altnsft.algit.storefront;

import com.altnsft.algit.storefront.security.CustomAuthenticationProvider;
import com.altnsft.algit.storefront.services.LoginService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

	@Autowired
	private LoginService loginService;

	@Autowired
	public void configureGlobal(AuthenticationManagerBuilder auth) throws Exception
	{
		auth.userDetailsService(loginService);
		auth.authenticationProvider(authenticationProvider());
	}

	@Bean
	public CustomAuthenticationProvider authenticationProvider()
	{
		CustomAuthenticationProvider daoAuthenticationProvider = new CustomAuthenticationProvider();
		daoAuthenticationProvider.setLoginService(loginService);
		return daoAuthenticationProvider;
	}

	@Override
	protected void configure(HttpSecurity http) throws Exception
	{
		http.authorizeRequests().antMatchers("/login").permitAll();

		http.authorizeRequests().and().formLogin().loginProcessingUrl("/j_spring_security_check")
				.loginPage("/login")
				.defaultSuccessUrl("/category")
				.failureUrl("/login?error=true")
				.usernameParameter("email")
				.passwordParameter("password")
				.and().logout().logoutUrl("/j_spring_security_logout").logoutSuccessUrl("/login").and().csrf();
	}
}
