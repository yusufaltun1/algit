package com.altnsft.algit.storefront.annotations;

import com.altnsft.algit.storefront.validators.LongValueValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

/**
 * Created by yusufaltun on 24.06.2018.
 */
@Documented
@Constraint(validatedBy = LongValueValidator.class)
@Target( { ElementType.METHOD, ElementType.FIELD })
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidLongValue {
    String message() default "Invalid long valie";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}