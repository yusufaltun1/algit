package com.altnsft.algit.storefront.services;

import com.altnsft.algit.models.dtos.OrderDTO;
import com.altnsft.algit.models.dtos.OrderWrapper;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * Created by yusufaltun on 22.07.2018.
 */
@Service
public class OrderService extends BaseService
{
    public List<OrderDTO> getAllOrders(long storeId)
    {
        ResponseEntity<OrderWrapper> response = getRestTemplate().getForEntity(getBaseURL() +"/order/all/order/"+ storeId, OrderWrapper.class);
        return response.getBody().getOrders();
    }

    public OrderDTO getOrder(long orderId)
    {
        ResponseEntity<OrderDTO> response = getRestTemplate().getForEntity(getBaseURL() +"/order/order-detail/"+ orderId, OrderDTO.class);
        return response.getBody();
    }

    public void toPrepairing(long orderId)
    {
        ResponseEntity<Void> response = getRestTemplate().getForEntity(getBaseURL() +"/order/to-prepairing/"+ orderId, Void.class);
    }

    public void toReady(long orderId)
    {
        ResponseEntity<Void> response = getRestTemplate().getForEntity(getBaseURL() +"/order/to-ready/"+ orderId, Void.class);
    }

    public void toDelivered(long orderId)
    {
        ResponseEntity<Void> response = getRestTemplate().getForEntity(getBaseURL() +"/order/to-delivered/"+ orderId, Void.class);
    }
}
