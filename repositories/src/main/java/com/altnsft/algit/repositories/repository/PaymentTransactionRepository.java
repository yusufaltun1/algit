package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.PaymentTransactionEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yusufaltun on 15.09.2018.
 */
@Repository
public interface PaymentTransactionRepository extends CrudRepository<PaymentTransactionEntity, Long>
{
    List<PaymentTransactionEntity> findAllByConversationId(String conversationId);

    PaymentTransactionEntity findByConversationIdAndStatus(String conversationId, String status);
}
