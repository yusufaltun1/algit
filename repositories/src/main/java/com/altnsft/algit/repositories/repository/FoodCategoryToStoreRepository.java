package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.FoodCategoryEntity;
import com.altnsft.algit.repositories.entities.FoodCategoryToStoreEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yusufaltun on 20.08.2018.
 */
@Repository
public interface FoodCategoryToStoreRepository extends CrudRepository<FoodCategoryToStoreEntity, Long>
{
    List<FoodCategoryToStoreEntity> findAllByFoodCategoryIn(List<FoodCategoryEntity> foodCategories);
}
