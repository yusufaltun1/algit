package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.CategoryEntity;
import com.altnsft.algit.repositories.entities.MenuEntity;
import com.altnsft.algit.repositories.entities.StoreEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by yusufaltun on 14.06.2018.
 */
public interface MenuRepository extends CrudRepository<MenuEntity, Long>
{
    public List<MenuEntity> findAllByStoreEntity(StoreEntity storeEntity);
    List<MenuEntity> findAllByName(String name);

    List<MenuEntity> findAllByCategoryEntity(CategoryEntity entity);
}
