package com.altnsft.algit.repositories.entities;

/**
 * Created by yusufaltun on 21.07.2018.
 */
public enum CancelRequestStatus
{
    WAITING, CANCELLED, CANCEL_REJECTED;
}
