package com.altnsft.algit.repositories.entities;

import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by yusufaltun on 20.08.2018.
 */
@Entity
@Table(name = "food_categories")
public class FoodCategoryEntity extends BaseEntity
{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Class getClz() {
        return FoodCategoryEntity.class;
    }
}
