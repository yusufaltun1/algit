package com.altnsft.algit.repositories.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * Created by yusufaltun on 2.07.2018.
 */
@Entity
@Table(name = "store_info")
public class StoreInfoEntity extends BaseEntity
{
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "store_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private StoreEntity store;

    private String address;

    @Column(name = "map_lat")
    private Float mapLat;
    @Column(name = "map_lng")
    private Float mapLng;

    private String image;

    @Column(name = "max_cancel_time")
    private Long maxCancelTime;

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getMaxCancelTime() {
        return maxCancelTime;
    }

    public void setMaxCancelTime(Long maxCancelTime) {
        this.maxCancelTime = maxCancelTime;
    }

    public StoreEntity getStore() {
        return store;
    }

    public void setStore(StoreEntity store) {
        this.store = store;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public Float getMapLat() {
        return mapLat;
    }

    public void setMapLat(Float mapLat) {
        this.mapLat = mapLat;
    }

    public Float getMapLng() {
        return mapLng;
    }

    public void setMapLng(Float mapLng) {
        this.mapLng = mapLng;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public StoreInfoEntity(StoreEntity store, String address, Float mapLat, Float mapLng, String image) {
        this.store = store;
        this.address = address;
        this.mapLat = mapLat;
        this.mapLng = mapLng;
        this.image = image;
    }

    public StoreInfoEntity() {
    }

    @Override

    public Class getClz() {
        return StoreInfoEntity.class;
    }
}
