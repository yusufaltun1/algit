package com.altnsft.algit.repositories.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * Created by yusufaltun on 13.06.2018.
 */
@Entity
@Table(name = "selected_options")
public class SelectedOptionEntity extends BaseEntity
{
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "option_value_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private ProductOptionValueEntity productOptionValueEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cart_entry_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private CartEntryEntity cartEntryEntity;

    public ProductOptionValueEntity getProductOptionValueEntity() {
        return productOptionValueEntity;
    }

    public void setProductOptionValueEntity(ProductOptionValueEntity productOptionValueEntity) {
        this.productOptionValueEntity = productOptionValueEntity;
    }

    public CartEntryEntity getCartEntryEntity() {
        return cartEntryEntity;
    }

    public void setCartEntryEntity(CartEntryEntity cartEntryEntity) {
        this.cartEntryEntity = cartEntryEntity;
    }

    public SelectedOptionEntity(ProductOptionValueEntity productOptionValueEntity, CartEntryEntity cartEntryEntity) {
        this.productOptionValueEntity = productOptionValueEntity;
        this.cartEntryEntity = cartEntryEntity;
    }

    public SelectedOptionEntity() {
    }

    @Override
    public Class getClz(){
        return SelectedOptionEntity.class;
    }
}
