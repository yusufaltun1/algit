package com.altnsft.algit.repositories.entities;

/**
 * Created by yusufaltun on 8.07.2018.
 */
public enum CartStatus
{
    WAITING, CONVERTED_TO_ORDER;
}
