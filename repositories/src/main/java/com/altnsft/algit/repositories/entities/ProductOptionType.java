package com.altnsft.algit.repositories.entities;

/**
 * Created by yusufaltun on 13.06.2018.
 */
public enum ProductOptionType
{
    MULTIPLE_SELECT, ONE_SELECT;
}
