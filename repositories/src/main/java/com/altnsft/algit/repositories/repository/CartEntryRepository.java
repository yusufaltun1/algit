package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.CartEntity;
import com.altnsft.algit.repositories.entities.CartEntryEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yusufaltun on 14.06.2018.
 */
@Repository
public interface CartEntryRepository extends CrudRepository<CartEntryEntity, Long>
{
    List<CartEntryEntity> findAllByCartId(CartEntity cartEntity);
}
