package com.altnsft.algit.repositories.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * Created by yusufaltun on 13.06.2018.
 */
@Entity
@Table(name = "product_options")
public class ProductOptionEntity extends BaseEntity
{
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private ProductEntity productEntity;

    @Enumerated(EnumType.STRING)
    @Column(name = "option_type")
    private ProductOptionType optionType;

    private String name;
    private String text;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ProductEntity getProductEntity() {
        return productEntity;
    }

    public void setProductEntity(ProductEntity productEntity) {
        this.productEntity = productEntity;
    }

    public ProductOptionType getOptionType() {
        return optionType;
    }

    public void setOptionType(ProductOptionType optionType) {
        this.optionType = optionType;
    }

    public ProductOptionEntity(ProductEntity productEntity) {
        this.productEntity = productEntity;
    }

    public ProductOptionEntity(ProductEntity productEntity, ProductOptionType optionType, String name, String text) {
        this.productEntity = productEntity;
        this.optionType = optionType;
        this.name = name;
        this.text = text;
    }

    public ProductOptionEntity() {
    }

    @Override
    public Class getClz(){
        return ProductOptionEntity.class;
    }
}
