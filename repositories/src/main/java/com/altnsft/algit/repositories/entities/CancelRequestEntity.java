package com.altnsft.algit.repositories.entities;

import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * Created by yusufaltun on 21.07.2018.
 */
@Entity
@Table(name = "cancel_request")
public class CancelRequestEntity extends BaseEntity
{
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "order_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    private OrderEntity order;

    @Enumerated(EnumType.STRING)
    @Column(name = "cancel_request_status")
    private CancelRequestStatus cancelRequestStatus;

    public OrderEntity getOrder() {
        return order;
    }

    public void setOrder(OrderEntity order) {
        this.order = order;
    }

    public CancelRequestStatus getCancelRequestStatus() {
        return cancelRequestStatus;
    }

    public void setCancelRequestStatus(CancelRequestStatus cancelRequestStatus) {
        this.cancelRequestStatus = cancelRequestStatus;
    }
}
