package com.altnsft.algit.repositories.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by yusufaltun on 13.06.2018.
 */
@Entity
@Table(name = "menus")
public class MenuEntity extends BaseEntity
{
    private String name;
    private BigDecimal price;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "store_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private StoreEntity storeEntity;

    private String title;
    private String description;
    private boolean sellable;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private CategoryEntity categoryEntity;

    private String image;

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }
    @Column(name = "max_make_time")
    private long maxMakeTime;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "food_category_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private FoodCategoryEntity foodCategory;

    public FoodCategoryEntity getFoodCategory() {
        return foodCategory;
    }

    public void setFoodCategory(FoodCategoryEntity foodCategory) {
        this.foodCategory = foodCategory;
    }

    public long getMaxMakeTime() {
        return maxMakeTime;
    }

    public void setMaxMakeTime(long maxMakeTime) {
        this.maxMakeTime = maxMakeTime;
    }

    public CategoryEntity getCategoryEntity() {
        return categoryEntity;
    }

    public void setCategoryEntity(CategoryEntity categoryEntity) {
        this.categoryEntity = categoryEntity;
    }

    public boolean isSellable() {
        return sellable;
    }

    public void setSellable(boolean sellable) {
        this.sellable = sellable;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public StoreEntity getStoreEntity() {
        return storeEntity;
    }

    public void setStoreEntity(StoreEntity storeEntity) {
        this.storeEntity = storeEntity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public String getDescription() {
        return description;
    }

    public MenuEntity(String name, BigDecimal price, StoreEntity storeEntity, String title, String description, boolean sellable, CategoryEntity categoryEntity) {
        this.name = name;
        this.price = price;
        this.storeEntity = storeEntity;
        this.title = title;
        this.description = description;
        this.sellable = sellable;
        this.categoryEntity = categoryEntity;
    }

    public MenuEntity() {
    }

    @Override
    public Class getClz(){
        return MenuEntity.class;
    }
}
