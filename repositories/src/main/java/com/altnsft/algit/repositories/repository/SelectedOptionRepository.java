package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.CartEntryEntity;
import com.altnsft.algit.repositories.entities.SelectedOptionEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yusufaltun on 14.06.2018.
 */
@Repository
public interface SelectedOptionRepository extends CrudRepository<SelectedOptionEntity, Long>
{
    List<SelectedOptionEntity> findAllByCartEntryEntity(CartEntryEntity cartEntryEntity);
}
