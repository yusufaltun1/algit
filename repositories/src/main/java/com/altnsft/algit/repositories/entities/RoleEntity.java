package com.altnsft.algit.repositories.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

/**
 * Created by yusufaltun on 18.06.2018.
 */
@Entity
@Table(name = "role")
public class RoleEntity extends BaseEntity
{
    @Column(name="role")
    private String role;

    public String getRole() {
        return role;
    }
    public void setRole(String role) {
        this.role = role;
    }

    public RoleEntity(String role) {
        this.role = role;
    }

    public RoleEntity() {
    }

    @Override
    public Class getClz(){
        return RoleEntity.class;
    }
}
