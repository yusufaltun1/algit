package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.AddressEntity;
import com.altnsft.algit.repositories.entities.CustomerEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by yusufaltun on 12.09.2018.
 */
@Repository
public interface AddressRepository extends CrudRepository<AddressEntity, Long>
{
    AddressEntity findByCustomer(CustomerEntity customerEntity);
}
