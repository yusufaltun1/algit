package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.CommentsEntity;
import com.altnsft.algit.repositories.entities.StoreEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by yusufaltun on 29.07.2018.
 */
public interface CommentRepository extends CrudRepository<CommentsEntity, Long>
{
    List<CommentsEntity> findAllByStore(StoreEntity store);
}
