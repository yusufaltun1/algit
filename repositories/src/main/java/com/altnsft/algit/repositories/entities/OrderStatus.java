package com.altnsft.algit.repositories.entities;

/**
 * Created by yusufaltun on 8.07.2018.
 */
public enum OrderStatus
{
    WAITING, PREPAIRING, READY, COMPLETED, WAITING_FOR_CANCEL, CANCELLED, WAITING_FOR_REFUND, REFUNDED;
}
