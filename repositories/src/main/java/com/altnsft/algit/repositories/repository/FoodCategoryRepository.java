package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.FoodCategoryEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by yusufaltun on 20.08.2018.
 */
@Repository
public interface FoodCategoryRepository extends CrudRepository<FoodCategoryEntity, Long>{
}
