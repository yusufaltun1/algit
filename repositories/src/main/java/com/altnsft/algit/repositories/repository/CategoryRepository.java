package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.CategoryEntity;
import com.altnsft.algit.repositories.entities.StoreEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yusufaltun on 16.06.2018.
 */
@Repository
public interface CategoryRepository extends CrudRepository<CategoryEntity, Long>
{
    List<CategoryEntity> findAllByStoreEntity(StoreEntity storeEntity);
    List<CategoryEntity> findAllByName(String name);
}
