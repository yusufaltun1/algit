package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.ProductEntity;
import com.altnsft.algit.repositories.entities.ProductOptionEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yusufaltun on 14.06.2018.
 */
@Repository
public interface ProductOptionRepository extends CrudRepository<ProductOptionEntity, Long>
{
    List<ProductOptionEntity> findAllByProductEntity(ProductEntity productEntity);

}
