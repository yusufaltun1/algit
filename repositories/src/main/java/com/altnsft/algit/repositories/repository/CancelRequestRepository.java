package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.CancelRequestEntity;
import com.altnsft.algit.repositories.entities.CancelRequestStatus;
import com.altnsft.algit.repositories.entities.OrderEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by yusufaltun on 21.07.2018.
 */
public interface CancelRequestRepository extends CrudRepository<CancelRequestEntity, Long>
{
    CancelRequestEntity findByOrderAndCancelRequestStatus(OrderEntity order, CancelRequestStatus cancelRequestStatus);
}
