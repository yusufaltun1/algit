package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.StoreEntity;
import com.altnsft.algit.repositories.entities.StoreInfoEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by yusufaltun on 2.07.2018.
 */
public interface StoreInfoRepository extends CrudRepository<StoreInfoEntity, Long>
{
    StoreInfoEntity findByStore(StoreEntity storeEntity);

    @Query(value = "SELECT * FROM store_info s HAVING  ( 6371 * ACOS(COS(RADIANS(:lat)) * COS( RADIANS( s.map_lat ) ) * COS( RADIANS( s.map_lng ) - radians(:lng) ) + SIN( RADIANS(:lat) ) * SIN( RADIANS( s.map_lat ) ) ) ) < :maxRange and ( 6371 * ACOS(COS(RADIANS(:lat)) * COS( RADIANS( s.map_lat ) ) * COS( RADIANS( s.map_lng ) - radians(:lng) ) + SIN( RADIANS(:lat) ) * SIN( RADIANS( s.map_lat ) ) ) ) > :minRange", nativeQuery = true)
    List<StoreInfoEntity> getStoreByNearby(@Param("lat") float lat, @Param("lng") float lng, @Param("minRange") long minRange, @Param("maxRange") long maxRange);
}
