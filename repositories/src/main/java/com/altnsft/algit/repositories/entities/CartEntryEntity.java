package com.altnsft.algit.repositories.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.awt.*;
import java.math.BigDecimal;

/**
 * Created by yusufaltun on 13.06.2018.
 */
@Entity
@Table(name = "cart_entries")
public class CartEntryEntity extends BaseEntity
{
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cart_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private CartEntity cartId;

    private BigDecimal price;
    @Column(name = "price_without_discount")
    private BigDecimal priceWithoutDiscount;
    @Column(name = "total_price")
    private BigDecimal totalPrice;

    @Column(name = "total_price_without_discount")
    private BigDecimal totalPriceWithoutDiscount;

    @Column(name = "total_discount")
    private BigDecimal totalDiscount;

    private BigDecimal discount;

    @Column(name = "total_option_price")
    private BigDecimal totalOptionPrice;

    public MenuEntity getMenuEntity() {
        return menuEntity;
    }

    public void setMenuEntity(MenuEntity menuEntity) {
        this.menuEntity = menuEntity;
    }

    private Long quantity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private ProductEntity productEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "menu_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private MenuEntity menuEntity;

    public CartEntity getCartId() {
        return cartId;
    }

    public void setCartId(CartEntity cartId) {
        this.cartId = cartId;
    }

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public BigDecimal getPriceWithoutDiscount() {
        return priceWithoutDiscount;
    }

    public void setPriceWithoutDiscount(BigDecimal priceWithoutDiscount) {
        this.priceWithoutDiscount = priceWithoutDiscount;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getTotalPriceWithoutDiscount() {
        return totalPriceWithoutDiscount;
    }

    public void setTotalPriceWithoutDiscount(BigDecimal totalPriceWithoutDiscount) {
        this.totalPriceWithoutDiscount = totalPriceWithoutDiscount;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public ProductEntity getProductEntity() {
        return productEntity;
    }

    public void setProductEntity(ProductEntity productEntity) {
        this.productEntity = productEntity;
    }

    public BigDecimal getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(BigDecimal totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public BigDecimal getDiscount() {
        return discount;
    }

    public void setDiscount(BigDecimal discount) {
        this.discount = discount;
    }

    public BigDecimal getTotalOptionPrice() {
        return totalOptionPrice;
    }

    public void setTotalOptionPrice(BigDecimal totalOptionPrice) {
        this.totalOptionPrice = totalOptionPrice;
    }

    public CartEntryEntity(CartEntity cartId, BigDecimal price, BigDecimal priceWithoutDiscount, BigDecimal totalPrice, BigDecimal totalPriceWithoutDiscount, BigDecimal totalDiscount, BigDecimal discount, BigDecimal totalOptionPrice, Long quantity, ProductEntity productEntity, MenuEntity menuEntity) {
        this.cartId = cartId;
        this.price = price;
        this.priceWithoutDiscount = priceWithoutDiscount;
        this.totalPrice = totalPrice;
        this.totalPriceWithoutDiscount = totalPriceWithoutDiscount;
        this.totalDiscount = totalDiscount;
        this.discount = discount;
        this.totalOptionPrice = totalOptionPrice;
        this.quantity = quantity;
        this.productEntity = productEntity;
        this.menuEntity = menuEntity;
    }

    public CartEntryEntity() {
    }

    @Override
    public Class getClz(){
        return CartEntryEntity.class;
    }
}
