package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.UserEntity;
import com.altnsft.algit.repositories.entities.UserRoleEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yusufaltun on 18.06.2018.
 */
@Repository
public interface UserRoleRepository  extends CrudRepository<UserRoleEntity, Long>
{
    List<UserRoleEntity> findByUserEntity(UserEntity userEntity);
}
