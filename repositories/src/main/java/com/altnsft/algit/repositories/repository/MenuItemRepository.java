package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.MenuEntity;
import com.altnsft.algit.repositories.entities.MenuItemEntity;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

/**
 * Created by yusufaltun on 14.06.2018.
 */
public interface MenuItemRepository extends CrudRepository<MenuItemEntity, Long>{
    List<MenuItemEntity> findAllByMenuEntity(MenuEntity entity);
}
