package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.UserEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by yusufaltun on 14.06.2018.
 */
@Repository
public interface UserRepository extends CrudRepository<UserEntity, Long>
{
    UserEntity findByEmail(String email);
}
