package com.altnsft.algit.repositories.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.util.Set;

/**
 * Created by yusufaltun on 14.06.2018.
 */
@Entity
@Table(name = "users")
public class UserEntity extends BaseEntity
{
    private String name;
    @Column(name = "last_name")
    private String lastName;
    private String email;
    private String password;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "store_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private StoreEntity storeEntity;

    public StoreEntity getStoreEntity() {
        return storeEntity;
    }

    public void setStoreEntity(StoreEntity storeEntity) {
        this.storeEntity = storeEntity;
    }

    public int getActive() {
        return active;
    }

    public void setActive(int active) {
        this.active = active;
    }

    private int active;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public UserEntity(String name, String lastName, String email, String password, StoreEntity storeEntity, int active) {
        this.name = name;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
        this.storeEntity = storeEntity;
        this.active = active;
    }

    public UserEntity() {
    }

    @Override
    public Class getClz(){
        return UserEntity.class;
    }
}
