package com.altnsft.algit.repositories.entities;

import javax.persistence.*;

/**
 * Created by yusufaltun on 13.06.2018.
 */
@Entity
@Table(name = "stores")
public class StoreEntity extends BaseEntity
{
    private String name;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public StoreEntity(String name) {
        this.name = name;
    }

    public StoreEntity() {
    }

    @Override
    public Class getClz(){
        return StoreEntity.class;
    }
}
