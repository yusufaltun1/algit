package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.CustomerEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by yusufaltun on 25.06.2018.
 */
@Repository
public interface CustomerRepository  extends CrudRepository<CustomerEntity, Long>
{
    CustomerEntity findByEmail(String email);
}
