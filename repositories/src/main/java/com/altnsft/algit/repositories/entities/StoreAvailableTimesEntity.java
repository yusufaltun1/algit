package com.altnsft.algit.repositories.entities;


import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

@Entity
@Table(name = "store_available_times")
public class StoreAvailableTimesEntity extends BaseEntity
{
    private String monday;
    private String tuesday;
    private String thursday;
    private String wednesday;
    private String friday;
    private String saturday;
    private String sunday;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "store_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private StoreEntity storeEntity;

    public String getMonday() {
        return monday;
    }

    public void setMonday(String monday) {
        this.monday = monday;
    }

    public String getTuesday() {
        return tuesday;
    }

    public String getThursday() {
        return thursday;
    }

    public String getWednesday() {
        return wednesday;
    }

    public String getFriday() {
        return friday;
    }

    public String getSaturday() {
        return saturday;
    }

    public String getSunday() {
        return sunday;
    }

    public void setThursday(String thursday) {
        this.thursday = thursday;
    }

    public void setWednesday(String wednesday) {
        this.wednesday = wednesday;
    }

    public void setFriday(String friday) {
        this.friday = friday;
    }

    public void setSaturday(String saturday) {
        this.saturday = saturday;
    }

    public void setSunday(String sunday) {
        this.sunday = sunday;
    }

    public void setTuesday(String tuesday) {
        this.tuesday = tuesday;
    }

    public void setStoreEntity(StoreEntity storeEntity) {
        this.storeEntity = storeEntity;
    }

    public StoreEntity getStoreEntity() {
        return storeEntity;
    }

    @Override
    public Class getClz() {
        return StoreAvailableTimesEntity.class;
    }
}
