package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.CategoryEntity;
import com.altnsft.algit.repositories.entities.ProductEntity;
import com.altnsft.algit.repositories.entities.StoreEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yusufaltun on 14.06.2018.
 */
@Repository
public interface ProductRepository extends CrudRepository<ProductEntity, Long>
{
    List<ProductEntity> findAllByStoreEntity(StoreEntity storeEntity);

    List<ProductEntity> findAllByName(String name);

    List<ProductEntity> findAllByCodeContainsOrNameContainsOrDescriptionContains(String code, String name, String description);

    List<ProductEntity> findAllByCategoryEntity(CategoryEntity categoryEntity);

}
