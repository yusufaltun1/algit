package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.GalleryImageEntity;
import com.altnsft.algit.repositories.entities.StoreEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yusufaltun on 30.08.2018.
 */
@Repository
public interface GalleryImageRepository extends CrudRepository<GalleryImageEntity, Long>
{
    List<GalleryImageEntity> findAllByStore(StoreEntity store);
}
