package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.ProductOptionEntity;
import com.altnsft.algit.repositories.entities.ProductOptionValueEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yusufaltun on 14.06.2018.
 */
@Repository
public interface ProductOptionValueRepository extends CrudRepository<ProductOptionValueEntity, Long>
{
    List<ProductOptionValueEntity> findAllByProductOptionEntity(ProductOptionEntity entity);
}
