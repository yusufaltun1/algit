package com.altnsft.algit.repositories.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.math.BigDecimal;

/**
 * Created by yusufaltun on 13.06.2018.
 */
@Entity
@Table(name = "product_option_values")
public class ProductOptionValueEntity extends BaseEntity
{
    private String code;
    private String text;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "product_option_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private ProductOptionEntity productOptionEntity;

    private BigDecimal price;

    public BigDecimal getPrice() {
        return price;
    }

    public void setPrice(BigDecimal price) {
        this.price = price;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public ProductOptionEntity getProductOptionEntity() {
        return productOptionEntity;
    }

    public void setProductOptionEntity(ProductOptionEntity productOptionEntity) {
        this.productOptionEntity = productOptionEntity;
    }

    public ProductOptionValueEntity() {
    }

    public ProductOptionValueEntity(String code, String text, ProductOptionEntity productOptionEntity, BigDecimal price) {
        this.code = code;
        this.text = text;
        this.productOptionEntity = productOptionEntity;
        this.price = price;
    }

    @Override
    public Class getClz(){
        return ProductOptionValueEntity.class;
    }
}
