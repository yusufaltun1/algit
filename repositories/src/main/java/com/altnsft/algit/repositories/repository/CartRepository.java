package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.CartEntity;
import com.altnsft.algit.repositories.entities.CartStatus;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

/**
 * Created by yusufaltun on 14.06.2018.
 */
@Repository
public interface CartRepository extends CrudRepository<CartEntity, Long>
{
    public CartEntity findByIdAndCartStatus(long cartId, CartStatus cartStatus);
}
