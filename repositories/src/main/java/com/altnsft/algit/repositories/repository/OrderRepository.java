package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.CustomerEntity;
import com.altnsft.algit.repositories.entities.OrderEntity;
import com.altnsft.algit.repositories.entities.StoreEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yusufaltun on 10.07.2018.
 */
@Repository
public interface OrderRepository extends CrudRepository<OrderEntity, Long>
{
    List<OrderEntity> findAllByCustomerOrderByCreatedAtDesc(CustomerEntity customerEntity);

    List<OrderEntity> findAllByStoreOrderByCreatedAtDesc(StoreEntity storeEntity);
}
