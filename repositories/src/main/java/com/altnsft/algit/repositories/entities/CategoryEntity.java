package com.altnsft.algit.repositories.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * Created by yusufaltun on 16.06.2018.
 */
@Table(name = "category")
@Entity
public class CategoryEntity extends BaseEntity
{
    private String name;
    private String text;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "store_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private StoreEntity storeEntity;

    public StoreEntity getStoreEntity() {
        return storeEntity;
    }

    public void setStoreEntity(StoreEntity storeEntity) {
        this.storeEntity = storeEntity;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public CategoryEntity(String name, String text, StoreEntity storeEntity) {
        this.name = name;
        this.text = text;
        this.storeEntity = storeEntity;
    }

    public CategoryEntity() {
    }

    @Override
    public Class getClz(){
        return CategoryEntity.class;
    }
}
