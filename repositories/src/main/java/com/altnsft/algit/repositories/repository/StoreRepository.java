package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.StoreEntity;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

/**
 * Created by yusufaltun on 14.06.2018.
 */
@Repository
public interface StoreRepository extends CrudRepository<StoreEntity, Long>
{

}
