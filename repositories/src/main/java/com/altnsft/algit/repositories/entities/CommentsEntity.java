package com.altnsft.algit.repositories.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;

/**
 * Created by yusufaltun on 29.07.2018.
 */
@Entity
@Table(name = "comments")
public class CommentsEntity extends BaseEntity
{
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "store_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private StoreEntity store;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", nullable = false)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private CustomerEntity customer;

    private String comment;

    @Column(name = "food_quality_point")
    private int foodQualityPoint;

    @Column(name = "price_point")
    private int pricePoint;

    @Column(name = "punctuality_point")
    private int punctualityPoint;

    @Column(name = "courtesy_point")
    private int courtesyPoint;

    private String message;

    public int getCourtesyPoint() {
        return courtesyPoint;
    }

    public void setCourtesyPoint(int courtesyPoint) {
        this.courtesyPoint = courtesyPoint;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public StoreEntity getStore() {
        return store;
    }

    public void setStore(StoreEntity store) {
        this.store = store;
    }

    public CustomerEntity getCustomer() {
        return customer;
    }

    public void setCustomer(CustomerEntity customer) {
        this.customer = customer;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getFoodQualityPoint() {
        return foodQualityPoint;
    }

    public void setFoodQualityPoint(int foodQualityPoint) {
        this.foodQualityPoint = foodQualityPoint;
    }

    public int getPricePoint() {
        return pricePoint;
    }

    public void setPricePoint(int pricePoint) {
        this.pricePoint = pricePoint;
    }

    public int getPunctualityPoint() {
        return punctualityPoint;
    }

    public void setPunctualityPoint(int punctualityPoint) {
        this.punctualityPoint = punctualityPoint;
    }

    @Override
    public Class getClz() {
        return CommentsEntity.class;
    }
}
