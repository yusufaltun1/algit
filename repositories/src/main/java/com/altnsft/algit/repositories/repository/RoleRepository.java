package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.RoleEntity;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by yusufaltun on 16.06.2018.
 */
public interface RoleRepository extends CrudRepository<RoleEntity, Integer>
{
    RoleEntity findByRole(String role);
}
