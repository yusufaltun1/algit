package com.altnsft.algit.repositories.entities;

import com.fasterxml.jackson.annotation.JsonIgnore;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Date;


/**
 * Created by yusufaltun on 13.06.2018.
 */
@Entity
@Table(name = "carts")
public class CartEntity extends BaseEntity
{
    private BigDecimal totalPrice;
    private BigDecimal totalPriceWithoutDiscount;
    private BigDecimal totalDiscount;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "customer_id", nullable = true)
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private CustomerEntity customerEntity;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "store_id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    @JsonIgnore
    private StoreEntity storeEntity;


    @Enumerated(EnumType.STRING)
    @Column(name = "cart_status")
    private CartStatus cartStatus;

    @Temporal(TemporalType.TIMESTAMP)
    @Column(name = "take_away_time")
    private Date takeAwayTime;

    public Date getTakeAwayTime() {
        return takeAwayTime;
    }

    public void setTakeAwayTime(Date takeAwayTime) {
        this.takeAwayTime = takeAwayTime;
    }

    public CartStatus getCartStatus() {
        return cartStatus;
    }

    public void setCartStatus(CartStatus cartStatus) {
        this.cartStatus = cartStatus;
    }

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public BigDecimal getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(BigDecimal totalPrice) {
        this.totalPrice = totalPrice;
    }

    public BigDecimal getTotalPriceWithoutDiscount() {
        return totalPriceWithoutDiscount;
    }

    public void setTotalPriceWithoutDiscount(BigDecimal totalPriceWithoutDiscount) {
        this.totalPriceWithoutDiscount = totalPriceWithoutDiscount;
    }

    public BigDecimal getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(BigDecimal totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public StoreEntity getStoreEntity() {
        return storeEntity;
    }

    public void setStoreEntity(StoreEntity storeEntity) {
        this.storeEntity = storeEntity;
    }

    public CustomerEntity getCustomerEntity() {
        return customerEntity;
    }

    public void setCustomerEntity(CustomerEntity customerEntity) {
        this.customerEntity = customerEntity;
    }

    public CartEntity() {
    }

    public CartEntity(BigDecimal totalPrice, BigDecimal totalPriceWithoutDiscount, BigDecimal totalDiscount, CustomerEntity customerEntity, StoreEntity storeEntity, String description) {
        this.totalPrice = totalPrice;
        this.totalPriceWithoutDiscount = totalPriceWithoutDiscount;
        this.totalDiscount = totalDiscount;
        this.customerEntity = customerEntity;
        this.storeEntity = storeEntity;
        this.description = description;
    }

    @Override
    public Class getClz(){
        return CartEntity.class;
    }
}
