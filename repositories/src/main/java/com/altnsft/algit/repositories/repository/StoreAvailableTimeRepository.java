package com.altnsft.algit.repositories.repository;

import com.altnsft.algit.repositories.entities.StoreAvailableTimesEntity;
import com.altnsft.algit.repositories.entities.StoreEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface StoreAvailableTimeRepository extends CrudRepository<StoreAvailableTimesEntity, Long> {
    StoreAvailableTimesEntity findByStoreEntity(StoreEntity storeEntity);
}
