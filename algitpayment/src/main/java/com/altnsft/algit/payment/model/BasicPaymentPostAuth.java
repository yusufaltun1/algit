package com.altnsft.algit.payment.model;

import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.request.CreatePaymentPostAuthRequest;

public class BasicPaymentPostAuth extends BasicPaymentResource {

    public static BasicPaymentPostAuth create(CreatePaymentPostAuthRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/payment/postauth/basic",
                getHttpHeaders(request, options),
                request,
                BasicPaymentPostAuth.class);
    }
}
