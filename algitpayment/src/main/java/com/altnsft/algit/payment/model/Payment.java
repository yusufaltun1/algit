package com.altnsft.algit.payment.model;

import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.request.CreatePaymentRequest;
import com.altnsft.algit.payment.request.RetrievePaymentRequest;

public class Payment extends PaymentResource {

    public static Payment create(CreatePaymentRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/payment/auth",
                getHttpHeaders(request, options),
                request,
                Payment.class);
    }

    public static Payment retrieve(RetrievePaymentRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/payment/detail",
                getHttpHeaders(request, options),
                request,
                Payment.class);
    }
}
