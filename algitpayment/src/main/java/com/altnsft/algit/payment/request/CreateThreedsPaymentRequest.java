package com.altnsft.algit.payment.request;

import com.altnsft.algit.payment.Request;
import com.altnsft.algit.payment.ToStringRequestBuilder;

public class CreateThreedsPaymentRequest extends Request {

    private String paymentId;
    private String conversationData;

    public String getPaymentId() {
        return paymentId;
    }

    public void setPaymentId(String paymentId) {
        this.paymentId = paymentId;
    }

    public String getConversationData() {
        return conversationData;
    }

    public void setConversationData(String conversationData) {
        this.conversationData = conversationData;
    }

    @Override
    public String toString() {
        return new ToStringRequestBuilder(this)
                .appendSuper(super.toString())
                .append("paymentId", paymentId)
                .append("conversationData", conversationData)
                .toString();
    }
}
