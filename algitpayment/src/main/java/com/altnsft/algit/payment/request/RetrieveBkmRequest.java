package com.altnsft.algit.payment.request;

import com.altnsft.algit.payment.Request;
import com.altnsft.algit.payment.ToStringRequestBuilder;

public class RetrieveBkmRequest extends Request {

    private String token;

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    @Override
    public String toString() {
        return new ToStringRequestBuilder(this)
                .appendSuper(super.toString())
                .append("token", token)
                .toString();
    }
}
