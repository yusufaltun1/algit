package com.altnsft.algit.payment.model;

public enum ApmType {
    SOFORT,
    IDEAL,
    QIWI,
    GIROPAY
}
