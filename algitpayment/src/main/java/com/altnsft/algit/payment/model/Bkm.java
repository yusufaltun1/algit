package com.altnsft.algit.payment.model;

import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.request.RetrieveBkmRequest;

public class Bkm extends PaymentResource {

    private String token;
    private String callbackUrl;

    public static Bkm retrieve(RetrieveBkmRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/payment/bkm/auth/detail",
                getHttpHeaders(request, options),
                request,
                Bkm.class);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getCallbackUrl() {
        return callbackUrl;
    }

    public void setCallbackUrl(String callbackUrl) {
        this.callbackUrl = callbackUrl;
    }
}
