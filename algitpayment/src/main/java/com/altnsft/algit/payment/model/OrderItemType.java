package com.altnsft.algit.payment.model;

public enum OrderItemType {
    PHYSICAL,
    VIRTUAL
}
