package com.altnsft.algit.payment.request;


import com.altnsft.algit.payment.Request;
import com.altnsft.algit.payment.ToStringRequestBuilder;

public class RetrieveCardManagementPageCardRequest extends Request {

    private String pageToken;

    public String getPageToken() {
        return pageToken;
    }

    public void setPageToken(String pageToken) {
        this.pageToken = pageToken;
    }

    @Override
    public String toString() {
        return new ToStringRequestBuilder(this)
                .appendSuper(super.toString())
                .append("token", pageToken).toString();
    }
}
