package com.altnsft.algit.payment.model;

import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.request.CreatePaymentRequest;
import com.altnsft.algit.payment.request.RetrievePaymentRequest;

public class PaymentPreAuth extends PaymentResource {

    public static PaymentPreAuth create(CreatePaymentRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/payment/preauth",
                getHttpHeaders(request, options),
                request,
                PaymentPreAuth.class);
    }

    public static PaymentPreAuth retrieve(RetrievePaymentRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/payment/detail",
                getHttpHeaders(request, options),
                request,
                PaymentPreAuth.class);
    }
}
