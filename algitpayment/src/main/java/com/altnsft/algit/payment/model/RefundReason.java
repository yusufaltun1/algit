package com.altnsft.algit.payment.model;

public enum RefundReason {
    DOUBLE_PAYMENT,
    BUYER_REQUEST,
    FRAUD,
    OTHER
}
