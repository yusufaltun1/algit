package com.altnsft.algit.payment.model;

import com.google.gson.annotations.SerializedName;
import com.altnsft.algit.payment.DigestHelper;
import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.IyzipayResource;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.request.CreatePaymentRequest;

public class ThreedsInitializePreAuth extends IyzipayResource {

    @SerializedName("threeDSHtmlContent")
    private String htmlContent;

    public static ThreedsInitializePreAuth create(CreatePaymentRequest request, Options options) {
        ThreedsInitializePreAuth response = HttpClient.create().post(options.getBaseUrl() + "/payment/3dsecure/initialize/preauth",
                getHttpHeaders(request, options),
                request,
                ThreedsInitializePreAuth.class);
        if (response != null) {
            response.setHtmlContent(DigestHelper.decodeString(response.getHtmlContent()));
        }
        return response;
    }

    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }
}
