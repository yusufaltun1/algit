package com.altnsft.algit.payment.model;

import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.IyzipayResource;
import com.altnsft.algit.payment.Options;

public class ApiTest extends IyzipayResource {

    public static IyzipayResource retrieve(Options options) {
        return HttpClient.create().get(options.getBaseUrl() + "/payment/test", IyzipayResource.class);
    }
}
