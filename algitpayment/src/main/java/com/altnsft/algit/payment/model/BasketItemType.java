package com.altnsft.algit.payment.model;

public enum BasketItemType {
    PHYSICAL,
    VIRTUAL
}
