package com.altnsft.algit.payment.model;

public enum Currency {
    TRY,
    EUR,
    USD,
    GBP,
    IRR,
    NOK,
    RUB,
    CHF
}
