package com.altnsft.algit.payment.model;

import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.request.CreatePaymentPostAuthRequest;

public class PaymentPostAuth extends PaymentResource {

    public static PaymentPostAuth create(CreatePaymentPostAuthRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/payment/postauth",
                getHttpHeaders(request, options),
                request,
                PaymentPostAuth.class);
    }
}
