package com.altnsft.algit.payment.model;

import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.request.CreatePeccoPaymentRequest;

public class PeccoPayment extends PaymentResource {

    private String token;

    public static PeccoPayment create(CreatePeccoPaymentRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/payment/pecco/auth",
                getHttpHeaders(request, options),
                request,
                PeccoPayment.class);
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
