package com.altnsft.algit.payment.model;

public enum PaymentGroup {
    PRODUCT,
    LISTING,
    SUBSCRIPTION
}
