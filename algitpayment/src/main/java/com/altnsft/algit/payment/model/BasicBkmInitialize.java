package com.altnsft.algit.payment.model;

import com.altnsft.algit.payment.DigestHelper;
import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.IyzipayResource;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.request.CreateBasicBkmInitializeRequest;

public class BasicBkmInitialize extends IyzipayResource {

    private String htmlContent;
    private String token;

    public static BasicBkmInitialize create(CreateBasicBkmInitializeRequest request, Options options) {
        BasicBkmInitialize response = HttpClient.create().post(options.getBaseUrl() + "/payment/bkm/initialize/basic",
                getHttpHeaders(request, options),
                request,
                BasicBkmInitialize.class);
        if (response != null) {
            response.setHtmlContent(DigestHelper.decodeString(response.getHtmlContent()));
        }
        return response;
    }

    public String getHtmlContent() {
        return htmlContent;
    }

    public void setHtmlContent(String htmlContent) {
        this.htmlContent = htmlContent;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
