package com.altnsft.algit.payment.model;

import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.request.CreateCheckoutFormInitializeRequest;

public class CheckoutFormInitializePreAuth extends CheckoutFormInitializeResource {

    public static CheckoutFormInitializePreAuth create(CreateCheckoutFormInitializeRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/payment/iyzipos/checkoutform/initialize/preauth/ecom",
                getHttpHeaders(request, options),
                request,
                CheckoutFormInitializePreAuth.class);
    }
}
