package com.altnsft.algit.payment.model;

import com.google.gson.annotations.SerializedName;
import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.IyzipayResource;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.request.RetrieveTransactionsRequest;

import java.util.List;

public class BouncedBankTransferList extends IyzipayResource {

    @SerializedName("bouncedRows")
    private List<BankTransfer> bankTransfers;

    public static BouncedBankTransferList retrieve(RetrieveTransactionsRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/reporting/settlement/bounced",
                getHttpHeaders(request, options),
                request,
                BouncedBankTransferList.class);
    }

    public List<BankTransfer> getBankTransfers() {
        return bankTransfers;
    }

    public void setBankTransfers(List<BankTransfer> bankTransfers) {
        this.bankTransfers = bankTransfers;
    }
}
