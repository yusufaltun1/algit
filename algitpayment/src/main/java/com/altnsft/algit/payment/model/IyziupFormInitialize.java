package com.altnsft.algit.payment.model;

import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.request.CreateIyziupFormInitializeRequest;

public class IyziupFormInitialize extends IyziupFormInitializeResource {

    public static IyziupFormInitialize create(CreateIyziupFormInitializeRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/v1/iyziup/form/initialize",
                getHttpHeaders(request, options),
                request,
                IyziupFormInitialize.class);
    }
}
