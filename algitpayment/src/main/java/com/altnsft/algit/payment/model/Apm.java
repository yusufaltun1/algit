package com.altnsft.algit.payment.model;

import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.request.CreateApmInitializeRequest;
import com.altnsft.algit.payment.request.RetrieveApmRequest;

public class Apm extends ApmResource {

    public static Apm create(CreateApmInitializeRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/payment/apm/initialize",
                getHttpHeaders(request, options),
                request,
                Apm.class);
    }

    public static Apm retrieve(RetrieveApmRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/payment/apm/retrieve",
                getHttpHeaders(request, options),
                request,
                Apm.class);
    }
}
