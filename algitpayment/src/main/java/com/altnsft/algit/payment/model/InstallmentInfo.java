package com.altnsft.algit.payment.model;

import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.IyzipayResource;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.request.RetrieveInstallmentInfoRequest;

import java.util.List;

public class InstallmentInfo extends IyzipayResource {

    private List<InstallmentDetail> installmentDetails;

    public static InstallmentInfo retrieve(RetrieveInstallmentInfoRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/payment/iyzipos/installment",
                getHttpHeaders(request, options),
                request,
                InstallmentInfo.class);
    }

    public List<InstallmentDetail> getInstallmentDetails() {
        return installmentDetails;
    }

    public void setInstallmentDetails(List<InstallmentDetail> installmentDetails) {
        this.installmentDetails = installmentDetails;
    }
}
