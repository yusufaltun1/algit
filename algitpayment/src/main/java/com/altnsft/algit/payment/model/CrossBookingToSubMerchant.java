package com.altnsft.algit.payment.model;

import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.IyzipayResource;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.request.CreateCrossBookingRequest;

public class CrossBookingToSubMerchant extends IyzipayResource {

    public static CrossBookingToSubMerchant create(CreateCrossBookingRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/crossbooking/send",
                getHttpHeaders(request, options),
                request,
                CrossBookingToSubMerchant.class);
    }
}
