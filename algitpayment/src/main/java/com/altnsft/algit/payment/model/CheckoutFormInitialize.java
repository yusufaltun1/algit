package com.altnsft.algit.payment.model;

import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.request.CreateCheckoutFormInitializeRequest;

public class CheckoutFormInitialize extends CheckoutFormInitializeResource {

    public static CheckoutFormInitialize create(CreateCheckoutFormInitializeRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/payment/iyzipos/checkoutform/initialize/auth/ecom",
                getHttpHeaders(request, options),
                request,
                CheckoutFormInitialize.class);
    }
}
