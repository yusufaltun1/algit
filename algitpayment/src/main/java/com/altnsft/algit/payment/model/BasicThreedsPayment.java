package com.altnsft.algit.payment.model;

import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.request.CreateThreedsPaymentRequest;

public class BasicThreedsPayment extends BasicPaymentResource {

    public static BasicThreedsPayment create(CreateThreedsPaymentRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/payment/3dsecure/auth/basic",
                getHttpHeaders(request, options),
                request,
                BasicThreedsPayment.class);
    }
}
