package com.altnsft.algit.payment.request;

import com.altnsft.algit.payment.Request;
import com.altnsft.algit.payment.ToStringRequestBuilder;

public class RetrieveTransactionsRequest extends Request {

    private String date;

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return new ToStringRequestBuilder(this)
                .appendSuper(super.toString())
                .append("date", date)
                .toString();
    }
}
