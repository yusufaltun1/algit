package com.altnsft.algit.payment.model;

import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.request.CreateBasicPaymentRequest;

public class BasicPayment extends BasicPaymentResource {

    public static BasicPayment create(CreateBasicPaymentRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/payment/auth/basic",
                getHttpHeaders(request, options),
                request,
                BasicPayment.class);
    }
}
