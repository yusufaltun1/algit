package com.altnsft.algit.payment.model;

import com.altnsft.algit.payment.HttpClient;
import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.request.CreateBasicPaymentRequest;

public class BasicPaymentPreAuth extends BasicPaymentResource {

    public static BasicPaymentPreAuth create(CreateBasicPaymentRequest request, Options options) {
        return HttpClient.create().post(options.getBaseUrl() + "/payment/preauth/basic",
                getHttpHeaders(request, options),
                request,
                BasicPaymentPreAuth.class);
    }
}
