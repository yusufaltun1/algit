package com.altnsft.algit.models.dtos;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.List;

/**
 * Created by yusufaltun on 14.06.2018.
 */
public class ProductDTO implements Dto
{
    private long id;
    private String productCode;
    private String title;
    private String description;
    private Double price;

    private List<ProductOptionDTO> options;

    public List<ProductOptionDTO> getOptions() {
        return options;
    }

    public void setOptions(List<ProductOptionDTO> options) {
        this.options = options;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ProductDTO(long id, String productCode, String title, String description, Double price, List<ProductOptionDTO> options) {
        this.id = id;
        this.productCode = productCode;
        this.title = title;
        this.description = description;
        this.price = price;
        this.options = options;
    }

    public ProductDTO() {
    }
}
