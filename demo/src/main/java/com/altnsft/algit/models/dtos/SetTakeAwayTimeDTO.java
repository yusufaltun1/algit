package com.altnsft.algit.models.dtos;

import java.util.Date;

/**
 * Created by yusufaltun on 8.07.2018.
 */
public class SetTakeAwayTimeDTO
{
    private Date takeAwayTime;
    private long cartId;

    public Date getTakeAwayTime() {
        return takeAwayTime;
    }

    public void setTakeAwayTime(Date takeAwayTime) {
        this.takeAwayTime = takeAwayTime;
    }

    public long getCartId() {
        return cartId;
    }

    public void setCartId(long cartId) {
        this.cartId = cartId;
    }
}
