package com.altnsft.algit.models.dtos;

import org.springframework.data.elasticsearch.annotations.Document;

/**
 * Created by yusufaltun on 2.07.2018.
 */
public class StoreInfoDTO implements Dto
{
    private long id;

    private String address;

    private String mapLat;

    private String mapLng;

    private String image;

    private Long maxCancelTime;

    private String description;

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getMaxCancelTime() {
        return maxCancelTime;
    }

    public void setMaxCancelTime(Long maxCancelTime) {
        this.maxCancelTime = maxCancelTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getMapLat() {
        return mapLat;
    }

    public void setMapLat(String mapLat) {
        this.mapLat = mapLat;
    }

    public String getMapLng() {
        return mapLng;
    }

    public void setMapLng(String mapLng) {
        this.mapLng = mapLng;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public StoreInfoDTO(long id, String address, String mapLat, String mapLng, String image) {
        this.id = id;
        this.address = address;
        this.mapLat = mapLat;
        this.mapLng = mapLng;
        this.image = image;
    }

    public StoreInfoDTO() {
    }
}
