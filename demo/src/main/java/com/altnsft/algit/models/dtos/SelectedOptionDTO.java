package com.altnsft.algit.models.dtos;

import java.util.List;

/**
 * Created by yusufaltun on 1.07.2018.
 */
public class SelectedOptionDTO implements Dto
{
    private long optionId;
    private List<SelectedOptionValuDTO> selectedValues;

    public long getOptionId() {
        return optionId;
    }

    public void setOptionId(long optionId) {
        this.optionId = optionId;
    }

    public List<SelectedOptionValuDTO> getSelectedValues() {
        return selectedValues;
    }

    public void setSelectedValues(List<SelectedOptionValuDTO> selectedValues) {
        this.selectedValues = selectedValues;
    }

    public SelectedOptionDTO(long optionId, List<SelectedOptionValuDTO> selectedValues) {
        this.optionId = optionId;
        this.selectedValues = selectedValues;
    }

    public SelectedOptionDTO() {
    }
}
