package com.altnsft.algit.models.dtos;

/**
 * Created by yusufaltun on 3.07.2018.
 */
public class SelectedOptionValuDTO implements Dto
{
    private long selectedValue;
    private String text;
    private double price;

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public long getSelectedValue() {
        return selectedValue;
    }

    public void setSelectedValue(long selectedValue) {
        this.selectedValue = selectedValue;
    }

    public SelectedOptionValuDTO(long selectedValue) {
        this.selectedValue = selectedValue;
    }

    public SelectedOptionValuDTO() {
    }
}

