package com.altnsft.algit.models.dtos;

/**
 * Created by yusufaltun on 16.09.2018.
 */
public class CreateAddressDTO implements Dto
{
    private long customerId;
    private AddressDTO addressDTO;

    public long getCustomerId() {
        return customerId;
    }

    public void setCustomerId(long customerId) {
        this.customerId = customerId;
    }

    public AddressDTO getAddressDTO() {
        return addressDTO;
    }

    public void setAddressDTO(AddressDTO addressDTO) {
        this.addressDTO = addressDTO;
    }
}
