package com.altnsft.algit.models.dtos;

public class CommentDTO
{
    private long store;

    private long customer;

    private String comment;

    private int foodQualityPoint;

    private int pricePoint;

    private int punctualityPoint;

    private int courtesyPoint;

    private String message;

    public long getStore() {
        return store;
    }

    public void setStore(long store) {
        this.store = store;
    }

    public long getCustomer() {
        return customer;
    }

    public void setCustomer(long customer) {
        this.customer = customer;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getFoodQualityPoint() {
        return foodQualityPoint;
    }

    public void setFoodQualityPoint(int foodQualityPoint) {
        this.foodQualityPoint = foodQualityPoint;
    }

    public int getPricePoint() {
        return pricePoint;
    }

    public void setPricePoint(int pricePoint) {
        this.pricePoint = pricePoint;
    }

    public int getPunctualityPoint() {
        return punctualityPoint;
    }

    public void setPunctualityPoint(int punctualityPoint) {
        this.punctualityPoint = punctualityPoint;
    }

    public int getCourtesyPoint() {
        return courtesyPoint;
    }

    public void setCourtesyPoint(int courtesyPoint) {
        this.courtesyPoint = courtesyPoint;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
