package com.altnsft.algit.models.dtos;

/**
 * Created by yusufaltun on 8.07.2018.
 */
public class SetUserToCartDTO
{
    private long userId;
    private long cartId;

    public long getUserId() {
        return userId;
    }

    public void setUserId(long userId) {
        this.userId = userId;
    }

    public long getCartId() {
        return cartId;
    }

    public void setCartId(long cartId) {
        this.cartId = cartId;
    }
}
