package com.altnsft.algit.models.dtos;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.List;

/**
 * Created by yusufaltun on 30.06.2018.
 */
public class ProductOptionDTO implements Dto
{
    private int id;
    private String name;
    private String type;
    private List<ProductOptionValueDTO> values;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<ProductOptionValueDTO> getValues() {
        return values;
    }

    public void setValues(List<ProductOptionValueDTO> values) {
        this.values = values;
    }

    public ProductOptionDTO(int id, String name, String type, List<ProductOptionValueDTO> values) {
        this.id = id;
        this.name = name;
        this.type = type;
        this.values = values;
    }

    public ProductOptionDTO() {
    }
}
