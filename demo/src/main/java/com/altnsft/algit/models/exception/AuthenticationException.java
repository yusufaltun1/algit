package com.altnsft.algit.models.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by yusufaltun on 26.06.2018.
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class AuthenticationException extends RuntimeException
{
    public AuthenticationException() {
        super("Authentication failed");
    }
}
