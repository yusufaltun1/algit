package com.altnsft.algit.models.dtos;

import java.util.List;

/**
 * Created by yusufaltun on 14.06.2018.
 */
public class CartDTO implements Dto
{
    private long id;

    private Double totalPrice;
    private Double totalPriceWithoutDiscount;
    private Double totalDiscount;
    private StoreDTO store;
    private List<CartEntryDTO> cartEntries;
    private String takeAwayTime;
    private CustomerDTO customerDTO;

    public CustomerDTO getCustomerDTO() {
        return customerDTO;
    }

    public void setCustomerDTO(CustomerDTO customerDTO) {
        this.customerDTO = customerDTO;
    }

    public String getTakeAwayTime() {
        return takeAwayTime;
    }

    public void setTakeAwayTime(String takeAwayTime) {
        this.takeAwayTime = takeAwayTime;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Double getTotalPriceWithoutDiscount() {
        return totalPriceWithoutDiscount;
    }

    public void setTotalPriceWithoutDiscount(Double totalPriceWithoutDiscount) {
        this.totalPriceWithoutDiscount = totalPriceWithoutDiscount;
    }

    public Double getTotalDiscount() {
        return totalDiscount;
    }

    public void setTotalDiscount(Double totalDiscount) {
        this.totalDiscount = totalDiscount;
    }

    public List<CartEntryDTO> getCartEntries() {
        return cartEntries;
    }

    public void setCartEntries(List<CartEntryDTO> cartEntries) {
        this.cartEntries = cartEntries;
    }

    public CartDTO() {
    }

    public StoreDTO getStore() {
        return store;
    }

    public void setStore(StoreDTO store) {
        this.store = store;
    }

    public CartDTO(long id, Double totalPrice, Double totalPriceWithoutDiscount, Double totalDiscount, List<CartEntryDTO> cartEntries) {
        this.id = id;
        this.totalPrice = totalPrice;
        this.totalPriceWithoutDiscount = totalPriceWithoutDiscount;
        this.totalDiscount = totalDiscount;
        this.cartEntries = cartEntries;
    }
}
