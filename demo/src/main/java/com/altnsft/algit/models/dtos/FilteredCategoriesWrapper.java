package com.altnsft.algit.models.dtos;

import java.util.List;

/**
 * Created by yusufaltun on 20.08.2018.
 */
public class FilteredCategoriesWrapper
{
    private List<FilteredCategoryDTO> categories;

    public List<FilteredCategoryDTO> getCategories() {
        return categories;
    }

    public void setCategories(List<FilteredCategoryDTO> categories) {
        this.categories = categories;
    }
}
