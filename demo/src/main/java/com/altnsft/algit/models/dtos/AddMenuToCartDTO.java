package com.altnsft.algit.models.dtos;

import java.util.List;

/**
 * Created by yusufaltun on 1.07.2018.
 */
public class AddMenuToCartDTO
{
    private long cartId;
    private long menuId;
    private long quantity;

    private List<SelectedOptionDTO> selectedOptions;

    public List<SelectedOptionDTO> getSelectedOptions() {
        return selectedOptions;
    }

    public void setSelectedOptions(List<SelectedOptionDTO> selectedOptions) {
        this.selectedOptions = selectedOptions;
    }

    public long getCartId() {
        return cartId;
    }

    public void setCartId(long cartId) {
        this.cartId = cartId;
    }

    public long getMenuId() {
        return menuId;
    }

    public void setMenuId(long menuId) {
        this.menuId = menuId;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public AddMenuToCartDTO() {
    }

    public AddMenuToCartDTO(long cartId, long menuId, long quantity, List<SelectedOptionDTO> selectedOptions) {
        this.cartId = cartId;
        this.menuId = menuId;
        this.quantity = quantity;
        this.selectedOptions = selectedOptions;
    }
}
