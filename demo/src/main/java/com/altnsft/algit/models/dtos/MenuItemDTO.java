package com.altnsft.algit.models.dtos;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

/**
 * Created by yusufaltun on 30.06.2018.
 */
public class MenuItemDTO implements Dto
{
    private int id;
    private ProductDTO product;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public ProductDTO getProduct() {
        return product;
    }

    public void setProduct(ProductDTO product) {
        this.product = product;
    }

    public MenuItemDTO(int id, ProductDTO product) {
        this.id = id;
        this.product = product;
    }

    public MenuItemDTO() {
    }
}
