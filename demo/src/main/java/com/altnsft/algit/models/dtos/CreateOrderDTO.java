package com.altnsft.algit.models.dtos;

/**
 * Created by yusufaltun on 10.07.2018.
 */
public class CreateOrderDTO
{
    private long cartId;

    private PaymentMethod paymentMethod;

    private PaymentTransactionDTO paymentTransactionDTO;

    public PaymentTransactionDTO getPaymentTransactionDTO() {
        return paymentTransactionDTO;
    }

    public void setPaymentTransactionDTO(PaymentTransactionDTO paymentTransactionDTO) {
        this.paymentTransactionDTO = paymentTransactionDTO;
    }

    public PaymentMethod getPaymentMethod() {
        return paymentMethod;
    }

    public void setPaymentMethod(PaymentMethod paymentMethod) {
        this.paymentMethod = paymentMethod;
    }

    public long getCartId() {
        return cartId;
    }

    public void setCartId(long cartId) {
        this.cartId = cartId;
    }
}
