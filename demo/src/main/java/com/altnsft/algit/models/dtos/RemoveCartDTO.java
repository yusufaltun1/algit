package com.altnsft.algit.models.dtos;

/**
 * Created by yusufaltun on 14.07.2018.
 */
public class RemoveCartDTO
{
    private long cartId;

    public long getCartId() {
        return cartId;
    }

    public void setCartId(long cartId) {
        this.cartId = cartId;
    }
}
