package com.altnsft.algit.models.dtos;

/**
 * Created by yusufaltun on 15.09.2018.
 */
public class CreateCancelRequestDTO implements Dto
{
    private long orderId;

    public long getOrderId() {
        return orderId;
    }

    public void setOrderId(long orderId) {
        this.orderId = orderId;
    }
}
