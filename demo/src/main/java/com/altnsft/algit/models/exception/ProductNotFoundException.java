package com.altnsft.algit.models.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by yusufaltun on 30.06.2018.
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class ProductNotFoundException extends  RuntimeException
{
    public ProductNotFoundException(long id)
    {
        super("Product not found with id"+ id);
    }
}
