package com.altnsft.algit.models.dtos;

/**
 * Created by yusufaltun on 29.07.2018.
 */
public class CommentDisplayDTO implements Dto
{
    private String customerName;

    private String comment;

    private int foodQualityPoint;

    private int pricePoint;

    private int punctualityPoint;

    private int courtesyPoint;

    private String message;

    private String createdTime;

    public String getCreatedTime() {
        return createdTime;
    }

    public void setCreatedTime(String createdTime) {
        this.createdTime = createdTime;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getComment() {
        return comment;
    }

    public void setComment(String comment) {
        this.comment = comment;
    }

    public int getFoodQualityPoint() {
        return foodQualityPoint;
    }

    public void setFoodQualityPoint(int foodQualityPoint) {
        this.foodQualityPoint = foodQualityPoint;
    }

    public int getPricePoint() {
        return pricePoint;
    }

    public void setPricePoint(int pricePoint) {
        this.pricePoint = pricePoint;
    }

    public int getPunctualityPoint() {
        return punctualityPoint;
    }

    public void setPunctualityPoint(int punctualityPoint) {
        this.punctualityPoint = punctualityPoint;
    }

    public int getCourtesyPoint() {
        return courtesyPoint;
    }

    public void setCourtesyPoint(int courtesyPoint) {
        this.courtesyPoint = courtesyPoint;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
