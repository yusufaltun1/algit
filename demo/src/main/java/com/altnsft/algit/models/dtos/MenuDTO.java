package com.altnsft.algit.models.dtos;

import java.util.List;

/**
 * Created by yusufaltun on 30.06.2018.
 */
public class MenuDTO implements Dto
{
    private int id;
    private String name;
    private Double price;

    private String title;
    private String description;
    private boolean sellable;
    private String categoryName;

    private List<MenuItemDTO> items;

    private List<ProductOptionDTO> options;

    private long maxMakeTime;

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

    public long getMaxMakeTime() {
        return maxMakeTime;
    }

    public void setMaxMakeTime(long maxMakeTime) {
        this.maxMakeTime = maxMakeTime;
    }

    public List<ProductOptionDTO> getOptions() {
        return options;
    }

    public void setOptions(List<ProductOptionDTO> options) {
        this.options = options;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isSellable() {
        return sellable;
    }

    public void setSellable(boolean sellable) {
        this.sellable = sellable;
    }

    public List<MenuItemDTO> getItems() {
        return items;
    }

    public void setItems(List<MenuItemDTO> items) {
        this.items = items;
    }

    public MenuDTO(int id, String name, Double price, String title, String description, boolean sellable, List<MenuItemDTO> items, List<ProductOptionDTO> options) {
        this.id = id;
        this.name = name;
        this.price = price;
        this.title = title;
        this.description = description;
        this.sellable = sellable;
        this.items = items;
        this.options = options;
    }

    public MenuDTO() {
    }
}
