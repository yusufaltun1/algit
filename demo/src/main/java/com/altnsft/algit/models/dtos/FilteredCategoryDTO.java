package com.altnsft.algit.models.dtos;

/**
 * Created by yusufaltun on 20.08.2018.
 */
public class FilteredCategoryDTO implements Dto
{
    private long id;
    private String name;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
