package com.altnsft.algit.models.dtos;

import java.util.List;

/**
 * Created by yusufaltun on 15.07.2018.
 */
public class OrderWrapper
{
    private List<OrderDTO> orders;

    public List<OrderDTO> getOrders() {
        return orders;
    }

    public void setOrders(List<OrderDTO> orders) {
        this.orders = orders;
    }
}
