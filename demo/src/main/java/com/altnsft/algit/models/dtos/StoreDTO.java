package com.altnsft.algit.models.dtos;

import java.util.List;

/**
 * Created by yusufaltun on 30.06.2018.
 */
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.*;

@Document(indexName = "algit", type = "store")
public class StoreDTO implements Dto
{
    @Id
    private int storeId;

    private String name;

    private StoreInfoDTO storeInfo;

    private List<CategoryDTO> categories;

    private List<CommentDisplayDTO> comments;

    private StoreReviewSummaryDTO reviewSummary;

    private StoreAvailableTimesDTO storeAvailableTimes;

    private List<String> galleryImages;

    public List<String> getGalleryImages() {
        return galleryImages;
    }

    public void setGalleryImages(List<String> galleryImages) {
        this.galleryImages = galleryImages;
    }

    public StoreAvailableTimesDTO getStoreAvailableTimes() {
        return storeAvailableTimes;
    }

    public void setStoreAvailableTimes(StoreAvailableTimesDTO storeAvailableTimes) {
        this.storeAvailableTimes = storeAvailableTimes;
    }

    public StoreReviewSummaryDTO getReviewSummary() {
        return reviewSummary;
    }

    public void setReviewSummary(StoreReviewSummaryDTO reviewSummary) {
        this.reviewSummary = reviewSummary;
    }

    public List<CategoryDTO> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryDTO> categories) {
        this.categories = categories;
    }

    public StoreInfoDTO getStoreInfo() {
        return storeInfo;
    }

    public void setStoreInfo(StoreInfoDTO storeInfo) {
        this.storeInfo = storeInfo;
    }

    public int getStoreId() {
        return storeId;
    }

    public void setStoreId(int storeId) {
        this.storeId = storeId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<CommentDisplayDTO> getComments() {
        return comments;
    }

    public void setComments(List<CommentDisplayDTO> comments) {
        this.comments = comments;
    }

    public StoreDTO() {
    }

    public StoreDTO(int storeId, String name, StoreInfoDTO storeInfo, List<CategoryDTO> categories) {
        this.storeId = storeId;
        this.name = name;
        this.storeInfo = storeInfo;
        this.categories = categories;
    }
}
