package com.altnsft.algit.models.dtos;

import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;

import java.util.List;

/**
 * Created by yusufaltun on 3.07.2018.
 */
public class CategoryDTO implements Dto
{
    private long id;
    private String name;
    private String text;

    private List<MenuDTO> menus;
    private List<ProductDTO> products;

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<MenuDTO> getMenus() {
        return menus;
    }

    public void setMenus(List<MenuDTO> menus) {
        this.menus = menus;
    }

    public List<ProductDTO> getProducts() {
        return products;
    }

    public void setProducts(List<ProductDTO> products) {
        this.products = products;
    }

    public CategoryDTO() {
    }

    public CategoryDTO(long id, String name, String text, List<MenuDTO> menus, List<ProductDTO> products) {
        this.id = id;
        this.name = name;
        this.text = text;
        this.menus = menus;
        this.products = products;
    }
}
