package com.altnsft.algit.models.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by yusufaltun on 30.06.2018.
 */
@ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
public class CartNotFoundException extends RuntimeException
{
    public CartNotFoundException(long cartId) {
        super("Cart not found with id"+ cartId);
    }
}
