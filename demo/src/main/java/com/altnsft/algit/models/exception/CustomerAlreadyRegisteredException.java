package com.altnsft.algit.models.exception;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

/**
 * Created by yusufaltun on 25.06.2018.
 */
@ResponseStatus(value = HttpStatus.INTERNAL_SERVER_ERROR, reason = "CUSTOMER_ALREADY_REGISTERED")
public class CustomerAlreadyRegisteredException extends RuntimeException
{
    public CustomerAlreadyRegisteredException(String email) {
        super("Customer already registered with email "+ email);
    }
}
