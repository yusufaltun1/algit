package com.altnsft.algit.models.dtos;

/**
 * Created by yusufaltun on 2.07.2018.
 */
public class RemoveCartEntryDTO
{
    private long cartId;
    private long cartEntryId;

    public long getCartId() {
        return cartId;
    }

    public void setCartId(long cartId) {
        this.cartId = cartId;
    }

    public long getCartEntryId() {
        return cartEntryId;
    }

    public void setCartEntryId(long cartEntryId) {
        this.cartEntryId = cartEntryId;
    }

    public RemoveCartEntryDTO(long cartId, long cartEntryId) {
        this.cartId = cartId;
        this.cartEntryId = cartEntryId;
    }

    public RemoveCartEntryDTO() {
    }
}
