package com.altnsft.algit.models.dtos;

/**
 * Created by yusufaltun on 26.08.2018.
 */
public class Day implements Dto
{
    private String dayName;
    private String openTime;
    private String closeTime;

    public String getDayName() {
        return dayName;
    }

    public void setDayName(String dayName) {
        this.dayName = dayName;
    }

    public String getOpenTime() {
        return openTime;
    }

    public void setOpenTime(String openTime) {
        this.openTime = openTime;
    }

    public String getCloseTime() {
        return closeTime;
    }

    public void setCloseTime(String closeTime) {
        this.closeTime = closeTime;
    }
}
