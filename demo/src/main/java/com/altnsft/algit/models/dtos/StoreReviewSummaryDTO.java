package com.altnsft.algit.models.dtos;

/**
 * Created by yusufaltun on 29.07.2018.
 */
public class StoreReviewSummaryDTO implements Dto
{
    private int totalAvarage;

    private int foodQualityPoint;

    private int pricePoint;

    private int punctualityPoint;

    private int courtesyPoint;

    public int getTotalAvarage() {
        return totalAvarage;
    }

    public void setTotalAvarage(int totalAvarage) {
        this.totalAvarage = totalAvarage;
    }

    public int getFoodQualityPoint() {
        return foodQualityPoint;
    }

    public void setFoodQualityPoint(int foodQualityPoint) {
        this.foodQualityPoint = foodQualityPoint;
    }

    public int getPricePoint() {
        return pricePoint;
    }

    public void setPricePoint(int pricePoint) {
        this.pricePoint = pricePoint;
    }

    public int getPunctualityPoint() {
        return punctualityPoint;
    }

    public void setPunctualityPoint(int punctualityPoint) {
        this.punctualityPoint = punctualityPoint;
    }

    public int getCourtesyPoint() {
        return courtesyPoint;
    }

    public void setCourtesyPoint(int courtesyPoint) {
        this.courtesyPoint = courtesyPoint;
    }
}
