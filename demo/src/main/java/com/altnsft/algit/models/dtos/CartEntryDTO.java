package com.altnsft.algit.models.dtos;

import java.util.List;

/**
 * Created by yusufaltun on 14.06.2018.
 */
public class CartEntryDTO implements Dto
{
    private long id;
    private Double price;
    private Double priceWithoutDiscount;
    private Double totalPrice;
    private Double totalPriceWithoutDiscount;
    private Long quantity;

    private ProductDTO productDTO;
    private MenuDTO menuDTO;

    private List<SelectedOptionDTO> options;

    public List<SelectedOptionDTO> getOptions() {
        return options;
    }

    public void setOptions(List<SelectedOptionDTO> options) {
        this.options = options;
    }

    public MenuDTO getMenuDTO() {
        return menuDTO;
    }

    public void setMenuDTO(MenuDTO menuDTO) {
        this.menuDTO = menuDTO;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public ProductDTO getProductDTO() {
        return productDTO;
    }

    public void setProductDTO(ProductDTO productDTO) {
        this.productDTO = productDTO;
    }

    public Double getPrice() {
        return price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public Double getPriceWithoutDiscount() {
        return priceWithoutDiscount;
    }

    public void setPriceWithoutDiscount(Double priceWithoutDiscount) {
        this.priceWithoutDiscount = priceWithoutDiscount;
    }

    public Double getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Double totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Double getTotalPriceWithoutDiscount() {
        return totalPriceWithoutDiscount;
    }

    public void setTotalPriceWithoutDiscount(Double totalPriceWithoutDiscount) {
        this.totalPriceWithoutDiscount = totalPriceWithoutDiscount;
    }

    public Long getQuantity() {
        return quantity;
    }

    public void setQuantity(Long quantity) {
        this.quantity = quantity;
    }

    public CartEntryDTO() {
    }

    public CartEntryDTO(long id, Double price, Double priceWithoutDiscount, Double totalPrice, Double totalPriceWithoutDiscount, Long quantity, ProductDTO productDTO, MenuDTO menuDTO, List<SelectedOptionDTO> options) {
        this.id = id;
        this.price = price;
        this.priceWithoutDiscount = priceWithoutDiscount;
        this.totalPrice = totalPrice;
        this.totalPriceWithoutDiscount = totalPriceWithoutDiscount;
        this.quantity = quantity;
        this.productDTO = productDTO;
        this.menuDTO = menuDTO;
        this.options = options;
    }
}
