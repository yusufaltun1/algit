package com.altnsft.algit.models.dtos;

/**
 * Created by yusufaltun on 26.08.2018.
 */
public enum PaymentMethod
{
    CARD, CASH
}
