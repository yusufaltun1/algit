package com.altnsft.algit.models.dtos;

import java.util.List;

/**
 * Created by yusufaltun on 7.07.2018.
 */
public class StoreWrapper implements Dto
{
    private List<StoreDTO> stores;

    public List<StoreDTO> getStores() {
        return stores;
    }

    public void setStores(List<StoreDTO> stores) {
        this.stores = stores;
    }
}
