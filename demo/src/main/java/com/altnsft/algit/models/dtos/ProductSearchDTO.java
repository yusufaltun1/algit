package com.altnsft.algit.models.dtos;

/**
 * Created by yusufaltun on 30.06.2018.
 */
public class ProductSearchDTO implements Dto
{
    private String keyword;

    public String getKeyword() {
        return keyword;
    }

    public void setKeyword(String keyword) {
        this.keyword = keyword;
    }

    public ProductSearchDTO(String keyword) {
        this.keyword = keyword;
    }

    public ProductSearchDTO() {
    }
}
