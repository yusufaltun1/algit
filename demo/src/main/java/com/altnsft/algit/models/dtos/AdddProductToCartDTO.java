package com.altnsft.algit.models.dtos;

import java.util.List;

/**
 * Created by yusufaltun on 1.07.2018.
 */
public class AdddProductToCartDTO
{
    private long cartId;
    private long productId;
    private long quantity;

    private List<SelectedOptionDTO> selectedOptions;

    public List<SelectedOptionDTO> getSelectedOptions() {
        return selectedOptions;
    }

    public void setSelectedOptions(List<SelectedOptionDTO> selectedOptions) {
        this.selectedOptions = selectedOptions;
    }

    public long getCartId() {
        return cartId;
    }

    public void setCartId(long cartId) {
        this.cartId = cartId;
    }

    public long getProductId() {
        return productId;
    }

    public void setProductId(long productId) {
        this.productId = productId;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public AdddProductToCartDTO() {
    }

    public AdddProductToCartDTO(long cartId, long productId, long quantity, List<SelectedOptionDTO> selectedOptions) {
        this.cartId = cartId;
        this.productId = productId;
        this.quantity = quantity;
        this.selectedOptions = selectedOptions;
    }
}
