package com.altnsoft.algit.webservices;

import com.altnsft.algit.models.dtos.*;
import com.altnsft.algit.repositories.entities.*;
import org.assertj.core.util.Lists;

import java.math.BigDecimal;

/**
 * Created by yusufaltun on 5.07.2018.
 */
public class Data
{
    public static final StoreEntity STORE_ENTITY = new StoreEntity("Altun FOOD");
    public static final UserEntity USER_ENTITY = new UserEntity("hasan", "altun","hasan@gmail.com", "123", STORE_ENTITY, 1);
    public static final CategoryEntity CATEGORY_ENTITY = new CategoryEntity("Icecekler", "Icecekler", STORE_ENTITY);
    public static final ProductEntity PRODUCT_ENTITY = new ProductEntity("coca-cola", "Coca Cola", 0l, BigDecimal.ONE, true, "Coca Cola", "Description", STORE_ENTITY, CATEGORY_ENTITY);
    public static final MenuEntity MENU_ENTITY = new MenuEntity("Icecekli Menu", BigDecimal.valueOf(4), STORE_ENTITY, "Icecekli Menu", "Description", true, CATEGORY_ENTITY);
    public static final MenuItemEntity MENU_ITEM_ENTITY = new MenuItemEntity(MENU_ENTITY, PRODUCT_ENTITY);
    public static final ProductOptionEntity PRODUCT_OPTION_ENTITY_1 = new ProductOptionEntity(PRODUCT_ENTITY, ProductOptionType.ONE_SELECT, "icecek", "Icecek");
    public static final ProductOptionEntity PRODUCT_OPTION_ENTITY_2 = new ProductOptionEntity(PRODUCT_ENTITY, ProductOptionType.MULTIPLE_SELECT, "sos", "sos");
    public static final ProductOptionValueEntity PRODUCT_OPTION_VALUE_ENTITY_1_FOR_1 = new ProductOptionValueEntity("1 lt", "1 lt", PRODUCT_OPTION_ENTITY_1, BigDecimal.ZERO);
    public static final ProductOptionValueEntity PRODUCT_OPTION_VALUE_ENTITY_2_FOR_1 = new ProductOptionValueEntity("2.5 lt", "2.5 lt", PRODUCT_OPTION_ENTITY_1, BigDecimal.ZERO);

    public static final ProductOptionValueEntity PRODUCT_OPTION_VALUE_ENTITY_1_FOR_2 = new ProductOptionValueEntity("sutlac", "sutlac", PRODUCT_OPTION_ENTITY_2, BigDecimal.ZERO);
    public static final ProductOptionValueEntity PRODUCT_OPTION_VALUE_ENTITY_2_FOR_2 = new ProductOptionValueEntity("kazandibi", "kazandibi", PRODUCT_OPTION_ENTITY_2, BigDecimal.ZERO);
    public static final CartEntity CART_ENTITY = new CartEntity(BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, null, STORE_ENTITY, "");
    public static final CartEntryEntity CART_ENTRY_ENTITY = new CartEntryEntity(CART_ENTITY, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, BigDecimal.ZERO, 0l, null, MENU_ENTITY);
    public static final SelectedOptionEntity SELECTED_OPTION_ENTITY_1 = new SelectedOptionEntity(PRODUCT_OPTION_VALUE_ENTITY_1_FOR_1, CART_ENTRY_ENTITY);
    public static final SelectedOptionEntity SELECTED_OPTION_ENTITY_2 = new SelectedOptionEntity(PRODUCT_OPTION_VALUE_ENTITY_2_FOR_2, CART_ENTRY_ENTITY);
    public static final StoreInfoEntity STORE_INFO_ENTITY = new StoreInfoEntity(STORE_ENTITY, "Address", 0f, 0f, "");


    public static final ProductOptionValueDTO PRODUCT_OPTION_VALUE_DTO = new ProductOptionValueDTO(1, PRODUCT_OPTION_VALUE_ENTITY_1_FOR_1.getCode(), PRODUCT_OPTION_VALUE_ENTITY_1_FOR_1.getCode(), PRODUCT_OPTION_VALUE_ENTITY_1_FOR_1.getPrice().doubleValue());
    public static final ProductOptionValueDTO PRODUCT_OPTION_VALUE_DTO_1 = new ProductOptionValueDTO(1, PRODUCT_OPTION_VALUE_ENTITY_2_FOR_1.getCode(), PRODUCT_OPTION_VALUE_ENTITY_2_FOR_1.getCode(), PRODUCT_OPTION_VALUE_ENTITY_2_FOR_1.getPrice().doubleValue());

    public static final ProductOptionValueDTO PRODUCT_OPTION_VALUE_DTO_2_1 = new ProductOptionValueDTO(1, PRODUCT_OPTION_VALUE_ENTITY_2_FOR_1.getCode(), PRODUCT_OPTION_VALUE_ENTITY_2_FOR_1.getCode(), PRODUCT_OPTION_VALUE_ENTITY_2_FOR_1.getPrice().doubleValue());
    public static final ProductOptionValueDTO PRODUCT_OPTION_VALUE_DTO_2_2 = new ProductOptionValueDTO(1, PRODUCT_OPTION_VALUE_ENTITY_2_FOR_2.getCode(), PRODUCT_OPTION_VALUE_ENTITY_2_FOR_2.getCode(), PRODUCT_OPTION_VALUE_ENTITY_2_FOR_2.getPrice().doubleValue());

    public static final ProductOptionDTO PRODUCT_OPTION_DTO = new ProductOptionDTO(1, PRODUCT_OPTION_ENTITY_1.getName(), PRODUCT_OPTION_ENTITY_1.getOptionType().name(), Lists.newArrayList(PRODUCT_OPTION_VALUE_DTO, PRODUCT_OPTION_VALUE_DTO_1));
    public static final ProductOptionDTO PRODUCT_OPTION_DTO_1 = new ProductOptionDTO(2, PRODUCT_OPTION_ENTITY_2.getName(), PRODUCT_OPTION_ENTITY_2.getOptionType().name(), Lists.newArrayList(PRODUCT_OPTION_VALUE_DTO_2_1, PRODUCT_OPTION_VALUE_DTO_2_2));
    public static final ProductDTO PRODUCT_DTO = new ProductDTO(1l, PRODUCT_ENTITY.getCode(), PRODUCT_ENTITY.getTitle(), PRODUCT_ENTITY.getDescription(), PRODUCT_ENTITY.getPrice().doubleValue(), Lists.newArrayList(PRODUCT_OPTION_DTO, PRODUCT_OPTION_DTO_1));
    public static final MenuItemDTO MENU_ITEM_DTO = new MenuItemDTO(1, PRODUCT_DTO);
    public static final MenuDTO MENU_DTO = new MenuDTO(1, MENU_ENTITY.getName(), MENU_ENTITY.getPrice().doubleValue(), MENU_ENTITY.getTitle(), MENU_ENTITY.getDescription(), MENU_ENTITY.isSellable(), Lists.newArrayList(MENU_ITEM_DTO), Lists.newArrayList(PRODUCT_OPTION_DTO));
    public static final StoreInfoDTO STORE_INFO_DTO = new StoreInfoDTO(1l, STORE_INFO_ENTITY.getAddress(), STORE_INFO_ENTITY.getMapLat().toString(), STORE_INFO_ENTITY.getMapLng().toString(), STORE_INFO_ENTITY.getImage());
    public static final CategoryDTO CATEGORY_DTO = new CategoryDTO(1l, CATEGORY_ENTITY.getName(), CATEGORY_ENTITY.getText(), Lists.newArrayList(MENU_DTO), Lists.newArrayList());
    public static final StoreDTO STORE_DTO = new StoreDTO(1, STORE_ENTITY.getName(), STORE_INFO_DTO, Lists.newArrayList(CATEGORY_DTO));
}
