package com.altnsoft.algit.webservices.form;

/**
 * Created by yusufaltun on 4.07.2018.
 */
public class AddMenuForm
{
    private String queryData;

    public String getQueryData() {
        return queryData;
    }

    public void setQueryData(String queryData) {
        this.queryData = queryData;
    }
}
