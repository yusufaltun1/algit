package com.altnsoft.algit.webservices.events.order;

import com.altnsft.algit.processes.thejava.events.EventListener;
import com.altnsft.algit.processes.thejava.events.EventSource;
import com.altnsft.algit.repositories.entities.OrderEntity;
import com.altnsft.algit.repositories.entities.OrderStatus;
import org.springframework.stereotype.Component;

/**
 * Created by yusufaltun on 22.07.2018.
 */
@Component
public class OrderReadyEvent implements EventListener
{
    @Override
    public String getBeanName() {
        return "orderReadyEvent";
    }

    @Override
    public String perform(EventSource eventSource)
    {
        OrderEntity orderEntity = (OrderEntity) eventSource.get("order_id");
        orderEntity.setOrderStatus(OrderStatus.READY);
        return "OK";
    }
}
