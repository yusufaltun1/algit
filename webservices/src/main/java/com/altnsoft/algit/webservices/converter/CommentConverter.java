package com.altnsoft.algit.webservices.converter;

import com.altnsft.algit.models.dtos.CommentDisplayDTO;
import com.altnsft.algit.repositories.entities.CommentsEntity;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

/**
 * Created by yusufaltun on 29.07.2018.
 */
@Component
public class CommentConverter implements Converter<CommentsEntity,CommentDisplayDTO> {
    @Override
    public CommentDisplayDTO convert(CommentsEntity entity) {
        CommentDisplayDTO dto = new CommentDisplayDTO();

        dto.setMessage(entity.getMessage());
        dto.setCourtesyPoint(entity.getCourtesyPoint());
        dto.setFoodQualityPoint(entity.getFoodQualityPoint());
        dto.setPricePoint(entity.getPricePoint());
        dto.setPunctualityPoint(entity.getPunctualityPoint());
        dto.setCustomerName(entity.getCustomer().getName() + " " + entity.getCustomer().getLastName());

        String date = new SimpleDateFormat("dd-MM-yyyy").format(entity.getCreatedAt());
        dto.setCreatedTime(date);
        return dto;
    }

    @Override
    public CommentsEntity reConvert(CommentDisplayDTO commentDisplayDTO) {
        return null;
    }
}
