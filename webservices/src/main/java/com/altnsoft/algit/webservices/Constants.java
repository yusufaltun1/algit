package com.altnsoft.algit.webservices;

/**
 * Created by yusufaltun on 22.07.2018.
 */
public class Constants
{
    public static final String REQUIREMENT_STORE_RESPONSE_KEY = "store_response";
    public static final String REQUIREMENT_STORE_RESPONSE_FOR_DELIVERED_KEY = "store_response_for_delivered";
    public static final String REQUIREMENT_STORE_RESPONSE_FOR_READY_KEY = "store_response_for_ready";

    public static final String CANCEL_REQUEST_PARAMETER = "cancel_request_id";
}
