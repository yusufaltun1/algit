package com.altnsoft.algit.webservices.service;

import com.altnsft.algit.repositories.entities.CustomerEntity;
import com.altnsft.algit.repositories.repository.CustomerRepository;
import com.altnsoft.algit.webservices.adapter.ConverterAdapter;
import com.altnsft.algit.models.dtos.CustomerDTO;
import com.altnsft.algit.models.dtos.LoginDTO;
import com.altnsft.algit.models.exception.AuthenticationException;
import com.altnsft.algit.models.exception.CustomerNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by yusufaltun on 25.06.2018.
 */
@Service
public class CustomerService
{
    @Autowired
    private ConverterAdapter converterAdapter;

    @Autowired
    private CustomerRepository customerRepository;

    public CustomerDTO createCustomer(CustomerDTO customerDTO)
    {
        CustomerEntity customer = (CustomerEntity) converterAdapter.get(new CustomerEntity()).reConvert(customerDTO);
        customer = customerRepository.save(customer);

        customerDTO = converterAdapter.convert(customer);

        return customerDTO;
    }

    public boolean isCustomerExist(String email)
    {
        CustomerEntity customer = customerRepository.findByEmail(email);
        return customer != null;
    }

    public boolean isCustomerExist(int userId)
    {
        CustomerEntity customer = customerRepository.findById(Long.valueOf(userId)).orElse(null);

        return (customer != null && customer.getActive());
    }

    public CustomerDTO updateCustomer(CustomerDTO customerDTO)
    {
        CustomerEntity customer = (CustomerEntity) converterAdapter.get(new CustomerEntity()).reConvert(customerDTO);

        customerRepository.save(customer);
        return (CustomerDTO) converterAdapter.convert(customer);
    }

    public List<CustomerDTO> getAll()
    {

        List<CustomerDTO> lst = new ArrayList<>();
        for(CustomerEntity entity : customerRepository.findAll())
        {
            lst.add(converterAdapter.convert(entity));
        }

        return lst;
    }

    public CustomerDTO login(LoginDTO login)
    {
        CustomerEntity customer = customerRepository.findByEmail(login.getEmail());
        if(customer == null)
        {
            throw new CustomerNotFoundException(login.getEmail());
        }

        if(!customer.getPassword().equals(login.getPassword()) || !customer.getActive())
        {
            throw new AuthenticationException();
        }

        customer.setLastLoginDate(new Date());
        customerRepository.save(customer);

        return (CustomerDTO) converterAdapter.convert(customer);
    }
}
