package com.altnsoft.algit.webservices.converter;

import com.altnsft.algit.repositories.entities.MenuItemEntity;
import com.altnsft.algit.models.dtos.MenuItemDTO;
import org.springframework.stereotype.Component;

/**
 * Created by yusufaltun on 30.06.2018.
 */
@Component
public class MenuItemConverter implements Converter<MenuItemEntity, MenuItemDTO> {

    @Override
    public MenuItemDTO convert(MenuItemEntity entity)
    {
        MenuItemDTO dto = new MenuItemDTO();
        dto.setId(entity.getId().intValue());

        return dto;
    }

    @Override
    public MenuItemEntity reConvert(MenuItemDTO menuItemDTO) {
        return null;
    }
}
