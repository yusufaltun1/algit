package com.altnsoft.algit.webservices.service;

import com.altnsft.algit.repositories.entities.*;
import com.altnsft.algit.repositories.repository.*;
import com.altnsoft.algit.webservices.adapter.ConverterAdapter;
import com.altnsft.algit.models.dtos.*;
import com.altnsft.algit.models.exception.CartNotFoundException;
import com.altnsft.algit.models.exception.CustomerNotFoundException;
import com.altnsft.algit.models.exception.MenuNotFoundException;
import com.altnsft.algit.models.exception.ProductNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;

/**
 * Created by yusufaltun on 26.06.2018.
 */
@Service
public class CartService
{
    private static final Logger LOGGER = LoggerFactory.getLogger(CartService.class);
    @Autowired
    private CartRepository cartRepository;
    @Autowired
    private CustomerService customerService;

    @Autowired
    private ConverterAdapter converterAdapter;

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private CartEntryRepository cartEntryRepository;

    @Autowired
    private ProductOptionValueRepository productOptionValueRepository;

    @Autowired
    private SelectedOptionRepository selectedOptionRepository;

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private CalculationService calculationService;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private StoreRepository storeRepository;

    public CartDTO createCart(int userId, long storeId)
    {
        boolean exist = customerService.isCustomerExist(userId);
        if(!exist)
        {
            throw new CustomerNotFoundException(Long.valueOf(userId));
        }

        CustomerEntity customerEntity = customerRepository.findById(Long.valueOf(userId)).get();

        CartEntity cart = new CartEntity();
        cart.setCustomerEntity(customerEntity);
        cart.setTotalDiscount(BigDecimal.ZERO);
        cart.setTotalPrice(BigDecimal.ZERO);
        cart.setTotalPriceWithoutDiscount(BigDecimal.ZERO);
        cart.setCartStatus(CartStatus.WAITING);
        StoreEntity storeEntity = storeRepository.findById(storeId).get();
        cart.setStoreEntity(storeEntity);

        cart = cartRepository.save(cart);

        CartDTO dto = converterAdapter.convert(cart);

        return dto;
    }

    public CartDTO getCart(CartEntity cart)
    {
        cart = calculationService.calculateCart(cart);
        CartDTO dto = converterAdapter.convert(cart);

        return  dto;
    }

    public CartDTO getCart(long cartId)
    {
        CartEntity cartEntity = cartRepository.findById(cartId).orElse(null);
        if(cartEntity == null)
        {
            throw new CartNotFoundException(cartId);
        }

        return getCart(cartEntity);
    }


    public CartDTO getAwailableCart(long cartId)
    {
        CartEntity cartEntity = cartRepository.findById(cartId).orElse(null);
        if(cartEntity == null)
        {
            throw new CartNotFoundException(cartId);
        }

        if(cartEntity.getCartStatus().equals(CartStatus.WAITING))
        {
            return getCart(cartEntity);
        }
        return null;
    }

    public CartDTO createCart(long storeId)
    {
        CartEntity cart = new CartEntity();
        cart.setTotalDiscount(BigDecimal.ZERO);
        cart.setTotalPrice(BigDecimal.ZERO);
        cart.setTotalPriceWithoutDiscount(BigDecimal.ZERO);
        cart.setCartStatus(CartStatus.WAITING);

        StoreEntity storeEntity = storeRepository.findById(storeId).get();
        cart.setStoreEntity(storeEntity);
        cart = cartRepository.save(cart);

        CartDTO dto = converterAdapter.convert(cart);

        return dto;
    }

    public CartDTO addMenuToCart(AddMenuToCartDTO addMenuToCartDTO)
    {
        CartEntity cartEntity = cartRepository.findById(addMenuToCartDTO.getCartId()).orElse(null);
        if(cartEntity == null)
        {
            throw new CartNotFoundException(addMenuToCartDTO.getCartId());
        }

        MenuEntity menuEntity = menuRepository.findById(addMenuToCartDTO.getMenuId()).orElse(null);
        if (menuEntity == null)
        {
            throw new MenuNotFoundException(addMenuToCartDTO.getMenuId());
        }

        createCartEntry(cartEntity, addMenuToCartDTO.getQuantity(), null, menuEntity, addMenuToCartDTO.getSelectedOptions());

        return getCart(cartEntity);
    }

    public CartDTO addProductToCart(AdddProductToCartDTO adddProductToCartDTO)
    {
        CartEntity cartEntity = cartRepository.findById(adddProductToCartDTO.getCartId()).orElse(null);
        if(cartEntity == null)
        {
            throw new CartNotFoundException(adddProductToCartDTO.getCartId());
        }

        ProductEntity productEntity = productRepository.findById(adddProductToCartDTO.getProductId()).orElse(null);

        if (productEntity == null)
        {
            throw new ProductNotFoundException(adddProductToCartDTO.getProductId());
        }

        createCartEntry(cartEntity, adddProductToCartDTO.getQuantity(), productEntity, null, adddProductToCartDTO.getSelectedOptions());

        return getCart(cartEntity);
    }

    private CartEntryEntity createCartEntry(CartEntity cartEntity, long quantity, ProductEntity productEntity, MenuEntity menuEntity, List<SelectedOptionDTO> selectedOptionDTOS)
    {
        CartEntryEntity cartEntryEntity = new CartEntryEntity();
        cartEntryEntity.setCartId(cartEntity);
        cartEntryEntity.setQuantity(quantity);
        cartEntryEntity.setProductEntity(productEntity);
        cartEntryEntity.setMenuEntity(menuEntity);
        cartEntryEntity.setPrice(BigDecimal.ZERO);
        cartEntryEntity.setPriceWithoutDiscount(BigDecimal.ZERO);
        cartEntryEntity.setTotalPrice(BigDecimal.ZERO);
        cartEntryEntity.setTotalPriceWithoutDiscount(BigDecimal.ZERO);
        cartEntryEntity.setDiscount(BigDecimal.ZERO);
        cartEntryEntity.setTotalOptionPrice(BigDecimal.ZERO);
        cartEntryEntity.setTotalDiscount(BigDecimal.ZERO);
        cartEntryRepository.save(cartEntryEntity);

        selectedOptionDTOS.stream()
                .forEach(option -> {
                    option.getSelectedValues().stream().forEach( value ->
                    {
                        SelectedOptionEntity selectedOptionEntity = new SelectedOptionEntity();
                        selectedOptionEntity.setCartEntryEntity(cartEntryEntity);
                        ProductOptionValueEntity optionValueEntity = productOptionValueRepository.findById(value.getSelectedValue()).get();
                        selectedOptionEntity.setProductOptionValueEntity(optionValueEntity);

                        selectedOptionRepository.save(selectedOptionEntity);
                    });

                });

        return cartEntryEntity;
    }

    public CartDTO removeCartEntry(RemoveCartEntryDTO removeCartEntryDTO)
    {
        CartEntity cart = cartRepository.findById(removeCartEntryDTO.getCartId()).orElse(null);
        if(cart == null)
        {
            throw new CartNotFoundException(removeCartEntryDTO.getCartId());
        }

        try
        {
            cartEntryRepository.deleteById(removeCartEntryDTO.getCartEntryId());
        }
        catch (EmptyResultDataAccessException e)
        {
            LOGGER.error("Cart entry not found cartId"+ removeCartEntryDTO.getCartEntryId());
        }

        return getCart(cart);
    }

    public CartDTO setUserToCart(SetUserToCartDTO userToCart)
    {
        CartEntity cart = cartRepository.findById(userToCart.getCartId()).orElse(null);
        if(cart == null)
        {
            throw new CartNotFoundException(userToCart.getCartId());
        }

        CustomerEntity customerEntity = customerRepository.findById(userToCart.getUserId()).orElse(null);
        if(customerEntity == null)
        {
            throw new CustomerNotFoundException((userToCart.getUserId()));
        }

        cart.setCustomerEntity(customerEntity);
        cart = cartRepository.save(cart);

        return getCart(cart);
    }

    public CartDTO setTakeAwayTime(SetTakeAwayTimeDTO takeAwayTime)
    {
        CartEntity cart = cartRepository.findById(takeAwayTime.getCartId()).orElse(null);
        if(cart == null)
        {
            throw new CartNotFoundException(takeAwayTime.getCartId());
        }

        cart.setTakeAwayTime(takeAwayTime.getTakeAwayTime());
        cart = cartRepository.save(cart);

        return getCart(cart);
    }

    public void removeCart(RemoveCartDTO dto)
    {
        CartEntity cartEntity = cartRepository.findById(dto.getCartId()).orElse(null);
        if(cartEntity == null)
        {
            throw new CartNotFoundException(dto.getCartId());
        }

        cartRepository.delete(cartEntity);
    }
}
