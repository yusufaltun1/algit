package com.altnsoft.algit.webservices.service;

import com.altnsft.algit.models.dtos.PaymentTransactionDTO;
import com.altnsft.algit.repositories.entities.PaymentTransactionEntity;
import com.altnsft.algit.repositories.repository.PaymentTransactionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by yusufaltun on 15.09.2018.
 */
@Service
public class PaymentTransactionService
{
    @Autowired
    private PaymentTransactionRepository paymentTransactionRepository;

    public void createPaymentTransaction(PaymentTransactionDTO paymentTransactionDTO)
    {
        if(paymentTransactionDTO != null)
        {
            PaymentTransactionEntity entity = new PaymentTransactionEntity();
            entity.setConversationId(paymentTransactionDTO.getConversationId());
            entity.setErrorCode(paymentTransactionDTO.getErrorCode());
            entity.setErrorGroup(paymentTransactionDTO.getErrorGroup());
            entity.setErrorMessage(paymentTransactionDTO.getErrorMessage());
            entity.setLocale(paymentTransactionDTO.getLocale());
            entity.setSystemTime(paymentTransactionDTO.getSystemTime());
            entity.setStatus(paymentTransactionDTO.getStatus());
            entity.setPaymentId(paymentTransactionDTO.getPaymentId());

            paymentTransactionRepository.save(entity);
        }
    }
}
