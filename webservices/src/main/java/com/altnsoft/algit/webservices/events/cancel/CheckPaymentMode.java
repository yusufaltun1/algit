package com.altnsoft.algit.webservices.events.cancel;

import com.altnsft.algit.processes.thejava.events.EventListener;
import com.altnsft.algit.processes.thejava.events.EventSource;
import com.altnsft.algit.repositories.entities.CancelRequestEntity;
import com.altnsft.algit.repositories.entities.OrderEntity;
import com.altnsft.algit.repositories.entities.PaymentMethod;
import com.altnsft.algit.repositories.repository.CancelRequestRepository;
import com.altnsoft.algit.webservices.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by yusufaltun on 16.09.2018.
 */
@Component
public class CheckPaymentMode implements EventListener {

    @Autowired
    private CancelRequestRepository cancelRequestRepository;

    @Override
    public String getBeanName() {
        return "checkPaymentMode";
    }

    @Override
    public String perform(EventSource eventSource) {
        long id = Long.valueOf((String)eventSource.get(Constants.CANCEL_REQUEST_PARAMETER));
        CancelRequestEntity cancelRequestEntity = cancelRequestRepository.findById(id).get();

        OrderEntity orderEntity = cancelRequestEntity.getOrder();

        PaymentMethod paymentMethod = orderEntity.getPaymentMethod();
        return paymentMethod.name();
    }
}
