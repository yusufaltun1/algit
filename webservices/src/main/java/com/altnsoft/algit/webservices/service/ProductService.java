package com.altnsoft.algit.webservices.service;

import com.altnsft.algit.repositories.entities.ProductEntity;
import com.altnsft.algit.repositories.entities.StoreEntity;
import com.altnsft.algit.repositories.repository.ProductRepository;
import com.altnsft.algit.repositories.repository.StoreRepository;
import com.altnsoft.algit.webservices.adapter.ConverterAdapter;
import com.altnsft.algit.models.dtos.ProductDTO;
import com.altnsft.algit.models.dtos.ProductSearchDTO;
import com.altnsft.algit.models.exception.ProductNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 30.06.2018.
 */
@Service
public class ProductService
{
    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private ConverterAdapter converterAdapter;

    @Autowired
    private StoreRepository storeRepository;

    public ProductDTO getProduct(int id)
    {
        ProductEntity productEntity = productRepository.findById(Long.valueOf(id)).orElse(null);

        if(productEntity == null)
        {
            throw new ProductNotFoundException(id);
        }

        return getProduct(productEntity);
    }

    public ProductDTO getProduct(ProductEntity productEntity)
    {
        ProductDTO productDTO = converterAdapter.convert(productEntity);

        return productDTO;
    }
    public List<ProductDTO> getProductsByStore(int storeId)
    {
        StoreEntity storeEntity = storeRepository.findById(Long.valueOf(storeId)).orElse(null);
        if(storeEntity != null)
        {
            List<ProductDTO> products = new ArrayList<>();

            List<ProductEntity> productEntities = productRepository.findAllByStoreEntity(storeEntity);
            productEntities.stream().forEach(entity -> {
                ProductDTO dto = getProduct(entity);
                products.add(dto);
            });

            return products;
        }

        return new ArrayList<>();
    }

    public List<ProductDTO> searchProduct(ProductSearchDTO searchDTO)
    {
        List<ProductEntity> productEntities = productRepository.findAllByCodeContainsOrNameContainsOrDescriptionContains(searchDTO.getKeyword(), searchDTO.getKeyword(), searchDTO.getKeyword());
        List<ProductDTO> products = new ArrayList<>();
        productEntities.stream().forEach(entity ->
        {
            ProductDTO dto = getProduct(entity);
            products.add(dto);
        });

        return products;
    }
}
