package com.altnsoft.algit.webservices.controller;

import com.altnsft.algit.models.dtos.*;
import com.altnsoft.algit.webservices.service.CartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * Created by yusufaltun on 14.06.2018.
 */
@RestController
@RequestMapping("/carts")
public class CartController
{
    @Autowired
    private CartService cartService;

    @PostMapping("/{userId}/create/{store-id}")
    public ResponseEntity createCart(@PathVariable int userId, @PathVariable("store-id") long storeId)
    {
        return ResponseEntity.ok(cartService.createCart(userId, storeId));
    }

    @PostMapping("/create/guest/{store-id}")
    public ResponseEntity<CartDTO> createCartForGuest(@PathVariable("store-id") long storeId)
    {
        return ResponseEntity.ok(cartService.createCart(storeId));
    }

    @GetMapping("/{cart-id}")
    public ResponseEntity<CartDTO> getCart(@PathVariable("cart-id") long cartId)
    {
        return ResponseEntity.ok(cartService.getAwailableCart(cartId));
    }

    @PostMapping("/add-menu")
    public ResponseEntity<CartDTO> addMenuToCart(@RequestBody AddMenuToCartDTO addMenuToCartDTO)
    {
        return ResponseEntity.ok(cartService.addMenuToCart(addMenuToCartDTO));
    }

    @PostMapping("/add-product")
    public ResponseEntity<CartDTO> addProductToCart(@RequestBody AdddProductToCartDTO adddProductToCartDTO)
    {
        return ResponseEntity.ok(cartService.addProductToCart(adddProductToCartDTO));
    }

    @PostMapping("/remove-cart-entry")
    public ResponseEntity<CartDTO> removeCartEntry(@RequestBody RemoveCartEntryDTO removeCartEntryDTO)
    {
        return ResponseEntity.ok(cartService.removeCartEntry(removeCartEntryDTO));
    }

    @PostMapping("/set-user")
    public ResponseEntity<CartDTO> setUserToCart(@RequestBody SetUserToCartDTO setUserToCartDTO)
    {
        return ResponseEntity.ok(cartService.setUserToCart(setUserToCartDTO));
    }

    @PostMapping("/remove")
    public void removeCart(@RequestBody RemoveCartDTO dto)
    {
        cartService.removeCart(dto);
    }

    @PostMapping("/set-take-away-time")
    public ResponseEntity<CartDTO> setTakeAwayTime(@RequestBody SetTakeAwayTimeDTO setTakeAwayTimeDTO)
    {
        return ResponseEntity.ok(cartService.setTakeAwayTime(setTakeAwayTimeDTO));
    }
}
