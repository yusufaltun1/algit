package com.altnsoft.algit.webservices.service;

import com.altnsft.algit.repositories.entities.*;
import com.altnsft.algit.repositories.repository.*;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.function.Function;

/**
 * Created by yusufaltun on 1.07.2018.
 */
@Service
public class CalculationService
{
    @Autowired
    private CartEntryRepository cartEntryRepository;

    @Autowired
    private SelectedOptionRepository selectedOptionRepository;

    @Autowired
    private CartRepository cartRepository;

    public CartEntity calculateCart(CartEntity cart)
    {
        List<CartEntryEntity> items = cartEntryRepository.findAllByCartId(cart);

        items.stream().forEach(cartEntry -> {
            cartEntry = calculateCartEntry(cartEntry);
            cartEntryRepository.save(cartEntry);
        });

        items = cartEntryRepository.findAllByCartId(cart);

        cart.setTotalPrice(addBigDecimals(CartEntryEntity::getTotalPrice, items));
        cart.setTotalPriceWithoutDiscount(addBigDecimals(CartEntryEntity::getTotalPriceWithoutDiscount, items));
        cart.setTotalDiscount(addBigDecimals(CartEntryEntity::getTotalDiscount, items));

        return cartRepository.save(cart);
    }

    private BigDecimal addBigDecimals(Function<CartEntryEntity, BigDecimal> function, List<CartEntryEntity> entities)
    {
        return entities.stream().map(function).reduce(BigDecimal.ZERO, BigDecimal::add);
    }


    private CartEntryEntity calculateCartEntry(CartEntryEntity cartEntry)
    {
        BigDecimal totalOptionsPrice = calculateAllOptions(cartEntry);
        BigDecimal price = null;
        BigDecimal priceWithoutDiscount = null;
        BigDecimal totalPrice = null;
        BigDecimal totalPriceWithoutDiscount = null;
        BigDecimal totalDiscount = cartEntry.getDiscount().multiply(BigDecimal.valueOf(cartEntry.getQuantity()));


        if(cartEntry.getProductEntity() != null)
        {
            ProductEntity product = cartEntry.getProductEntity();
            price = product.getPrice().add(totalOptionsPrice);
        }
        else if(cartEntry.getMenuEntity() != null)
        {
            MenuEntity menu =  cartEntry.getMenuEntity();
            price = menu.getPrice().add(totalOptionsPrice);
        }

        priceWithoutDiscount = price.subtract(cartEntry.getDiscount());
        totalPrice = price.multiply(BigDecimal.valueOf(cartEntry.getQuantity()));
        totalPriceWithoutDiscount = totalPrice.subtract(totalDiscount);


        cartEntry.setPrice(price);
        cartEntry.setPriceWithoutDiscount(priceWithoutDiscount);
        cartEntry.setTotalPrice(totalPrice);
        cartEntry.setTotalPriceWithoutDiscount(totalPriceWithoutDiscount);
        cartEntry.setTotalDiscount(totalDiscount);

        return cartEntry;
    }

    private BigDecimal calculateAllOptions(CartEntryEntity cartEntry)
    {
        BigDecimal totalOptionPrice = BigDecimal.ZERO;
        List<SelectedOptionEntity> options = selectedOptionRepository.findAllByCartEntryEntity(cartEntry);

        for (SelectedOptionEntity option : options) {
            totalOptionPrice = totalOptionPrice.add(option.getProductOptionValueEntity().getPrice());
        }

        return totalOptionPrice;
    }
}
