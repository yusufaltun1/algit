package com.altnsoft.algit.webservices.events.cancel;

import com.altnsft.algit.payment.Options;
import com.altnsft.algit.payment.model.Cancel;
import com.altnsft.algit.payment.model.Locale;
import com.altnsft.algit.payment.model.Status;
import com.altnsft.algit.payment.request.CreateCancelRequest;
import com.altnsft.algit.processes.thejava.events.EventListener;
import com.altnsft.algit.processes.thejava.events.EventSource;
import com.altnsft.algit.repositories.entities.CancelRequestEntity;
import com.altnsft.algit.repositories.entities.PaymentTransactionEntity;
import com.altnsft.algit.repositories.repository.CancelRequestRepository;
import com.altnsft.algit.repositories.repository.PaymentTransactionRepository;
import com.altnsoft.algit.webservices.Constants;
import com.altnsoft.algit.webservices.service.PaymentTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;


/**
 * Created by yusufaltun on 15.09.2018.
 */
@Component
public class SendCancelRequestToIyzicoEvent implements EventListener {

    @Autowired
    private CancelRequestRepository cancelRequestRepository;

    @Autowired
    private PaymentTransactionRepository paymentTransactionRepository;

    @Autowired
    private PaymentTransactionService paymentTransactionService;
    @Override
    public String getBeanName() {
        return "sendCancelRequestToIyzicoEvent";
    }

    @Override
    public String perform(EventSource eventSource)
    {
        Options options = new Options();

        options.setApiKey("sandbox-KCw1P0nst2aAhDw0lmeggWRYWyWcNZDH");
        options.setSecretKey("sandbox-wHgzDKuYkT5XMWjY00IALBpn6tHI7gFz");
        options.setBaseUrl("https://sandbox-api.iyzipay.com");

        long id = Long.valueOf((String)eventSource.get(Constants.CANCEL_REQUEST_PARAMETER));
        CancelRequestEntity cancelRequestEntity = cancelRequestRepository.findById(id).get();
        long cartId = cancelRequestEntity.getOrder().getCart().getId();

        PaymentTransactionEntity paymentTransactionEntity = paymentTransactionRepository.findByConversationIdAndStatus(String.valueOf(cartId), Status.SUCCESS.getValue());

        CreateCancelRequest request = new CreateCancelRequest();
        request.setLocale(Locale.TR.getValue());
        request.setConversationId(String.valueOf(cartId));
        request.setPaymentId(paymentTransactionEntity.getPaymentId());
        request.setIp("127.0.0.1");

        Cancel cancel = Cancel.create(request, options);
        PaymentTransactionEntity entityForCancel = new PaymentTransactionEntity();
        entityForCancel.setPaymentId(cancel.getPaymentId());
        entityForCancel.setStatus(cancel.getStatus());
        entityForCancel.setSystemTime(new Date(cancel.getSystemTime()));
        entityForCancel.setLocale(cancel.getLocale());
        entityForCancel.setErrorMessage(cancel.getErrorMessage());
        entityForCancel.setErrorGroup(cancel.getErrorGroup());
        entityForCancel.setErrorCode(cancel.getErrorCode());

        paymentTransactionRepository.save(entityForCancel);
        if(Status.SUCCESS.getValue().equals(cancel.getStatus()))
        {
            return "OK";
        }
        else
        {
            return "NOK";
        }
    }
}
