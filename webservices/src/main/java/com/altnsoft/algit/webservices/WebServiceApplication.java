package com.altnsoft.algit.webservices;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EntityScan( basePackages = {"com.altnsft.algit.repositories.entities", "com.altnsft.algit.processes.thejava.entity"} )
@EnableJpaRepositories(basePackages = {"com.altnsft.algit.repositories.repository", "com.altnsft.algit.processes.thejava.repositories"})
@ComponentScan(basePackages = {"com.altnsoft.algit.webservices.adapter",
		"com.altnsoft.algit.webservices.controller",
		"com.altnsoft.algit.webservices.service",
		"com.altnsoft.algit.webservices.converter","com.altnsoft.algit.webservices.cache","com.altnsoft.algit.webservices.events",
		"com.altnsft.algit.processes.thejava.context",
		"com.altnsft.algit.processes.thejava",
		"com.altnsft.algit.processes.thejava.controller",
		"com.altnsft.algit.processes.thejava.service",
		"com.altnsft.algit.processes.thejava"
})
@EnableScheduling
//@EnableElasticsearchRepositories(basePackages = "com.altnsoft.algit.webservices.elasticsearchrepo")
public class WebServiceApplication {

	public static void main(String[] args) {
		SpringApplication.run(WebServiceApplication.class, args);
	}
}
