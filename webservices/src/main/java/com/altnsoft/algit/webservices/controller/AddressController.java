package com.altnsoft.algit.webservices.controller;

import com.altnsft.algit.models.dtos.CreateAddressDTO;
import com.altnsft.algit.models.dtos.CustomerDTO;
import com.altnsft.algit.repositories.repository.CustomerRepository;
import com.altnsoft.algit.webservices.adapter.ConverterAdapter;
import com.altnsoft.algit.webservices.service.AddressService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by yusufaltun on 16.09.2018.
 */
@RestController
@RequestMapping("/address")
public class AddressController
{
    @Autowired
    private AddressService addressService;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ConverterAdapter converterAdapter;

    @PostMapping("/save")
    public ResponseEntity<CustomerDTO> createAddress(@RequestBody CreateAddressDTO createAddressDTO)
    {
        addressService.createAddress(createAddressDTO);
        CustomerDTO customerDTO = converterAdapter.convert(customerRepository.findById(createAddressDTO.getCustomerId()).get());

        return ResponseEntity.ok(customerDTO);
    }
}
