package com.altnsoft.algit.webservices.controller;

import com.altnsft.algit.models.dtos.PaymentTransactionDTO;
import com.altnsoft.algit.webservices.service.PaymentTransactionService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by yusufaltun on 15.09.2018.
 */
@RestController
@RequestMapping("/payment-transaction")
public class PaymentTransactionController
{
    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @PostMapping("/create")
    public ResponseEntity create(@RequestBody  PaymentTransactionDTO paymentTransactionDTO)
    {
        paymentTransactionService.createPaymentTransaction(paymentTransactionDTO);
        return ResponseEntity.ok().build();
    }
}
