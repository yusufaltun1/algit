package com.altnsoft.algit.webservices.controller;

import com.altnsft.algit.models.dtos.CreateCancelRequestDTO;
import com.altnsft.algit.models.dtos.CreateOrderDTO;
import com.altnsft.algit.models.dtos.OrderDTO;
import com.altnsft.algit.models.dtos.OrderWrapper;
import com.altnsoft.algit.webservices.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.xml.ws.Response;
import java.util.List;

/**
 * Created by yusufaltun on 10.07.2018.
 */
@RestController
@RequestMapping("/order")
public class OrderController
{
    @Autowired
    private OrderService orderService;

    @PostMapping("/create")
    public ResponseEntity<OrderDTO> craeteOrder(@RequestBody CreateOrderDTO createOrderDTO)
    {
        OrderDTO dto = orderService.create(createOrderDTO);
        return ResponseEntity.ok(dto);
    }

    @GetMapping("/{user-id}")
    public ResponseEntity<OrderWrapper> getMyOrders(@PathVariable(name = "user-id") long userId)
    {
        OrderWrapper orderWrapper = new OrderWrapper();
        orderWrapper.setOrders(orderService.getAllOrdersByUserId(userId));
        return ResponseEntity.ok(orderWrapper);
    }

    @GetMapping("/order-detail/{order-id}")
    public ResponseEntity<OrderDTO> getOrderDetail(@PathVariable(name = "order-id") long orderId)
    {
        return ResponseEntity.ok(orderService.getOrderById(orderId));
    }

    @GetMapping("/all/order/{store-id}")
    public ResponseEntity<OrderWrapper> getAllOrdersByStore(@PathVariable(name = "store-id") long storeId)
    {
        OrderWrapper orderWrapper = new OrderWrapper();
        orderWrapper.setOrders(orderService.getAllOrderByStore(storeId));
        return ResponseEntity.ok(orderWrapper);
    }

    @GetMapping("/to-prepairing/{order-id}")
    public void toPrepairing(@PathVariable(name = "order-id") long orderId)
    {
        orderService.toPrepairing(orderId);
    }

    @GetMapping("/to-ready/{order-id}")
    public void toReady(@PathVariable(name = "order-id") long orderId)
    {
        orderService.toReady(orderId);
    }

    @GetMapping("/to-delivered/{order-id}")
    public void toDelivered(@PathVariable(name = "order-id") long orderId)
    {
        orderService.toDelivered(orderId);
    }

    @PostMapping("/create/cancel-request")
    public ResponseEntity createCancelRequest(@RequestBody CreateCancelRequestDTO createCancelRequest)
    {
        orderService.createCancelRequest(createCancelRequest);

        return ResponseEntity.ok().build();
    }
}
