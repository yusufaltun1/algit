package com.altnsoft.algit.webservices.service;

import org.springframework.stereotype.Service;

/**
 * Created by yusufaltun on 22.07.2018.
 */
@Service
public class ProcessNameGeneratorService
{
    private static final String ORDER_PROCESS_PATH_KEY = "order_created";

    public String generateForOrder(long orderId)
    {
        StringBuilder processNameBuilder = new StringBuilder(String.valueOf(orderId)).append("_").append(ORDER_PROCESS_PATH_KEY);
        return processNameBuilder.toString();
    }
}
