package com.altnsoft.algit.webservices.controller;

import com.altnsft.algit.models.dtos.CustomerDTO;
import com.altnsft.algit.models.dtos.LoginDTO;
import com.altnsft.algit.models.exception.CustomerAlreadyRegisteredException;
import com.altnsft.algit.models.exception.CustomerNotFoundException;
import com.altnsoft.algit.webservices.service.CustomerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by yusufaltun on 25.06.2018.
 */
@RestController
@RequestMapping("/customer")
public class CustomerController
{
    @Autowired
    private CustomerService customerService;

    @PostMapping("/create")
    public ResponseEntity<CustomerDTO> createCustomer(@RequestBody CustomerDTO customer)
    {
        if (customerService.isCustomerExist(customer.getEmail()))
        {
            throw new CustomerAlreadyRegisteredException(customer.getEmail());
        }

        return ResponseEntity.ok(customerService.createCustomer(customer));
    }

    @PostMapping("/login")
    public ResponseEntity<CustomerDTO> login(@RequestBody LoginDTO login)
    {
        return ResponseEntity.ok(customerService.login(login));
    }

    @PostMapping("/update")
    public ResponseEntity<CustomerDTO> login(@RequestBody CustomerDTO customer)
    {
        if (customerService.isCustomerExist(customer.getEmail()))
        {
            return ResponseEntity.ok(customerService.updateCustomer(customer));
        }
        else
        {
            throw new CustomerNotFoundException(customer.getEmail());
        }
    }

    @GetMapping("/all")
    @ResponseBody
    public ResponseEntity<List<CustomerDTO>> createCustomer()
    {
        return ResponseEntity.ok(customerService.getAll());
    }
}
