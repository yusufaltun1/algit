package com.altnsoft.algit.webservices.converter;

import com.altnsft.algit.repositories.entities.ProductOptionEntity;
import com.altnsft.algit.repositories.entities.ProductOptionValueEntity;
import com.altnsft.algit.repositories.repository.ProductOptionValueRepository;
import com.altnsoft.algit.webservices.adapter.ConverterAdapter;
import com.altnsft.algit.models.dtos.ProductOptionDTO;
import com.altnsft.algit.models.dtos.ProductOptionValueDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 30.06.2018.
 */
@Component
public class ProductOptionConverter implements Converter<ProductOptionEntity,ProductOptionDTO>
{
    @Autowired
    private ProductOptionValueRepository productOptionValueRepository;

    @Autowired
    private ConverterAdapter converterAdapter;
    @Override
    public ProductOptionDTO convert(ProductOptionEntity entity)
    {

        ProductOptionDTO dto = new ProductOptionDTO();
        dto.setId(entity.getId().intValue());
        dto.setName(entity.getName());
        dto.setValues(new ArrayList<>());
        dto.setType(entity.getOptionType().name());
        List<ProductOptionValueEntity> values = productOptionValueRepository.findAllByProductOptionEntity(entity);

        values.stream().forEach(value ->
        {
            ProductOptionValueDTO optionValueDTO = converterAdapter.convert(value);
            dto.getValues().add(optionValueDTO);
        });
        return dto;
    }

    @Override
    public ProductOptionEntity reConvert(ProductOptionDTO productOptionDTO) {
        return null;
    }
}
