package com.altnsoft.algit.webservices.converter;

import com.altnsft.algit.repositories.entities.CartEntryEntity;
import com.altnsft.algit.repositories.entities.SelectedOptionEntity;
import com.altnsft.algit.repositories.repository.SelectedOptionRepository;
import com.altnsoft.algit.webservices.adapter.ConverterAdapter;
import com.altnsft.algit.models.dtos.*;
import com.altnsoft.algit.webservices.service.MenuService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by yusufaltun on 14.06.2018.
 */
@Component
public class CartEntryConverter implements Converter<CartEntryEntity, CartEntryDTO> {

    @Autowired
    private MenuService menuService;

    @Autowired
    private SelectedOptionRepository selectedOptionRepository;

    @Autowired
    private ConverterAdapter converterAdapter;

    @Override
    public CartEntryDTO convert(CartEntryEntity entity) {
        CartEntryDTO dto = new CartEntryDTO();
        dto.setId(entity.getId());
        dto.setPrice(entity.getPrice().doubleValue());
        dto.setPriceWithoutDiscount(entity.getPriceWithoutDiscount().doubleValue());
        dto.setQuantity(entity.getQuantity());
        dto.setTotalPrice(entity.getTotalPrice().doubleValue());
        dto.setTotalPriceWithoutDiscount(entity.getTotalPriceWithoutDiscount().doubleValue());
        if(entity.getProductEntity() != null)
        {
            ProductDTO productDTO = converterAdapter.convert(entity.getProductEntity());
            dto.setProductDTO(productDTO);
        }

        if(entity.getMenuEntity() != null)
        {
            MenuDTO menu = menuService.getMenu(entity.getMenuEntity());
            dto.setMenuDTO(menu);
        }

        dto.setOptions(new ArrayList<>());
        List<SelectedOptionEntity> selectedOptions = selectedOptionRepository.findAllByCartEntryEntity(entity);

        Map<Long, List<SelectedOptionEntity>> optionToValuesMap = groupOptionValuesByOption(selectedOptions);

        for (List<SelectedOptionEntity> selectedOptionsFromMap : optionToValuesMap.values())
        {
            SelectedOptionDTO selectedOptionDTO = new SelectedOptionDTO();
            selectedOptionDTO.setSelectedValues(new ArrayList<>());
            for (SelectedOptionEntity selectedOptionEntity : selectedOptionsFromMap)
            {
                selectedOptionDTO.setOptionId(selectedOptionEntity.getProductOptionValueEntity().getProductOptionEntity().getId());
                SelectedOptionValuDTO selectedOptionValuDTO = new SelectedOptionValuDTO();
                selectedOptionValuDTO.setSelectedValue(selectedOptionEntity.getProductOptionValueEntity().getId());
                selectedOptionValuDTO.setText(selectedOptionEntity.getProductOptionValueEntity().getText());
                selectedOptionValuDTO.setPrice(selectedOptionEntity.getProductOptionValueEntity().getPrice().doubleValue());
                selectedOptionDTO.getSelectedValues().add(selectedOptionValuDTO);
            }
            dto.getOptions().add(selectedOptionDTO);
        }

        return dto;
    }

    private Map<Long, List<SelectedOptionEntity>> groupOptionValuesByOption(List<SelectedOptionEntity> selectedOptions) {
        Map<Long, List<SelectedOptionEntity>> optionToValuesMap = new HashMap<>();

        for (SelectedOptionEntity selectedOption : selectedOptions)
        {
            long optionId = selectedOption.getProductOptionValueEntity().getProductOptionEntity().getId();
            if(optionToValuesMap.containsKey(optionId))
            {
                optionToValuesMap.get(optionId).add(selectedOption);
            }
            else
            {
                optionToValuesMap.put(optionId, new ArrayList<>());
                optionToValuesMap.get(optionId).add(selectedOption);
            }
        }

        return optionToValuesMap;
    }

    @Override
    public CartEntryEntity reConvert(CartEntryDTO cartEntryDTO) {
        return null;
    }
}
