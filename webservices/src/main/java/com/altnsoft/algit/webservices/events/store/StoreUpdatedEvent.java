package com.altnsoft.algit.webservices.events.store;

import com.altnsft.algit.models.dtos.StoreDTO;
import com.altnsft.algit.processes.thejava.events.EventListener;
import com.altnsft.algit.processes.thejava.events.EventSource;
import com.altnsft.algit.repositories.entities.StoreEntity;
import com.altnsft.algit.repositories.repository.StoreRepository;
import com.altnsoft.algit.webservices.adapter.ConverterAdapter;
import com.altnsoft.algit.webservices.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by yusufaltun on 24.07.2018.
 */
@Component
public class StoreUpdatedEvent implements EventListener
{
    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private ConverterAdapter converterAdapter;

    @Autowired
    private Cache cache;

    @Override
    public String getBeanName() {
        return "storeUpdatedEvent";
    }

    @Override
    public String perform(EventSource eventSource)
    {
        StoreEntity storeEntity = storeRepository.findById( Long.valueOf((String) eventSource.get("store_id"))).get();
        //elStoreRepository.deleteAllByStoreId(storeEntity.getId().intValue());

        StoreDTO storeDTO = converterAdapter.convert(storeEntity);
        //elStoreRepository.save(storeDTO);

        cache.update(storeDTO.getStoreId(), storeDTO);

        return "OK";
    }
}
