package com.altnsoft.algit.webservices.converter;

import com.altnsft.algit.models.dtos.*;
import com.altnsft.algit.repositories.entities.CartEntity;
import com.altnsft.algit.repositories.entities.CartEntryEntity;
import com.altnsft.algit.repositories.entities.StoreEntity;
import com.altnsft.algit.repositories.repository.CartEntryRepository;
import com.altnsft.algit.repositories.repository.SelectedOptionRepository;
import com.altnsft.algit.repositories.repository.StoreRepository;
import com.altnsoft.algit.webservices.adapter.ConverterAdapter;
import com.altnsoft.algit.webservices.cache.Cache;
import com.altnsoft.algit.webservices.service.MenuService;
import com.altnsoft.algit.webservices.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;

/**
 * Created by yusufaltun on 14.06.2018.
 */
@Component
public class CartConverter implements Converter<CartEntity, CartDTO>
{
    @Autowired
    private CartEntryRepository cartEntryRepository;

    @Autowired
    private ConverterAdapter converterAdapter;

    @Autowired
    private Cache cache;

    @Override
    public CartDTO convert(CartEntity cartEntity)
    {
        CartDTO dto = new CartDTO();
        dto.setId(cartEntity.getId());
        dto.setTotalDiscount(cartEntity.getTotalDiscount().doubleValue());
        dto.setTotalPrice(cartEntity.getTotalPrice().doubleValue());
        dto.setTotalPriceWithoutDiscount(cartEntity.getTotalPriceWithoutDiscount().doubleValue());
        dto.setCartEntries(new ArrayList<>());

        if(cartEntity.getCustomerEntity() != null)
        {
            CustomerDTO customerDTO = converterAdapter.convert(cartEntity.getCustomerEntity());
            dto.setCustomerDTO(customerDTO);
        }

        java.util.List<CartEntryEntity> entries = cartEntryRepository.findAllByCartId(cartEntity);

        entries.stream().forEach(entry -> {
            CartEntryDTO entryDto = converterAdapter.convert(entry);
            dto.getCartEntries().add(entryDto);
        });

        StoreDTO storeDTO = cache.get(cartEntity.getStoreEntity().getId().intValue());
        dto.setStore(storeDTO);
        if(cartEntity.getTakeAwayTime() != null)
        dto.setTakeAwayTime(DateUtil.formatDate(cartEntity.getTakeAwayTime()));
        return dto;
    }

    @Override
    public CartEntity reConvert(CartDTO cartDTO) {
        return null;
    }
}
