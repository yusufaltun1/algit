package com.altnsoft.algit.webservices.converter;

import com.altnsft.algit.repositories.entities.ProductOptionValueEntity;
import com.altnsft.algit.models.dtos.ProductOptionValueDTO;
import org.springframework.stereotype.Component;

/**
 * Created by yusufaltun on 30.06.2018.
 */
@Component
public class ProductOptionValueConverter implements Converter<ProductOptionValueEntity, ProductOptionValueDTO> {

    @Override
    public ProductOptionValueDTO convert(ProductOptionValueEntity entity)
    {
        ProductOptionValueDTO dto = new ProductOptionValueDTO();
        dto.setCode(entity.getCode());
        dto.setId(entity.getId().intValue());
        dto.setName(entity.getText());
        dto.setPrice(entity.getPrice().doubleValue());

        return dto;
    }

    @Override
    public ProductOptionValueEntity reConvert(ProductOptionValueDTO productOptionValueDTO) {
        return null;
    }
}
