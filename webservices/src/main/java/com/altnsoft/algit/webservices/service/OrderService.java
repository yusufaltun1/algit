package com.altnsoft.algit.webservices.service;

import com.altnsft.algit.models.dtos.CreateCancelRequestDTO;
import com.altnsft.algit.models.dtos.CreateOrderDTO;
import com.altnsft.algit.models.dtos.OrderDTO;
import com.altnsft.algit.models.exception.CartNotFoundException;
import com.altnsft.algit.models.exception.CustomerNotFoundException;
import com.altnsft.algit.processes.thejava.events.Parameter;
import com.altnsft.algit.processes.thejava.service.ParameterService;
import com.altnsft.algit.processes.thejava.service.ProcessService;
import com.altnsft.algit.processes.thejava.service.RequirementService;
import com.altnsft.algit.repositories.entities.*;
import com.altnsft.algit.repositories.repository.*;
import com.altnsoft.algit.webservices.Constants;
import com.altnsoft.algit.webservices.adapter.ConverterAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 10.07.2018.
 */
@Service
public class OrderService
{
    private static final long ORDER_CREATED_PATH_ID = 1;
    private static final long CANCEL_REQUEST_CREATED_PATH_ID = 3;

    @Autowired
    private CartRepository cartRepository;

    @Autowired
    private ConverterAdapter converterAdapter;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private ProcessService processService;

    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private ProcessNameGeneratorService processNameGeneratorService;

    @Autowired
    private RequirementService requirementService;

    @Autowired
    private PaymentTransactionService paymentTransactionService;

    @Autowired
    private CancelRequestRepository cancelRequestRepository;

    public OrderDTO create(CreateOrderDTO createOrder)
    {
        OrderEntity entity = new OrderEntity();
        CartEntity cartEntity = cartRepository.findById(createOrder.getCartId()).orElse(null);
        if(cartEntity == null)
        {
            throw new CartNotFoundException(createOrder.getCartId());
        }

        entity.setCart(cartEntity);
        entity.setCustomer(cartEntity.getCustomerEntity());
        entity.setOrderStatus(OrderStatus.WAITING);
        entity.setStore(cartEntity.getStoreEntity());
        entity.setPaymentMethod(PaymentMethod.valueOf(createOrder.getPaymentMethod().name()));
        entity = orderRepository.save(entity);

        cartEntity.setCartStatus(CartStatus.CONVERTED_TO_ORDER);
        cartRepository.save(cartEntity);
        paymentTransactionService.createPaymentTransaction(createOrder.getPaymentTransactionDTO());
        OrderDTO dto = converterAdapter.convert(entity);

        processService.createProcess(String.valueOf(entity.getId()), ORDER_CREATED_PATH_ID, Parameter.by("order_id", String.valueOf(entity.getId())));
        return dto;
    }

    public List<OrderDTO> getAllOrdersByUserId(long userId)
    {
        CustomerEntity customer = customerRepository.findById(userId).orElse(null);
        if(customer == null)
        {
            throw new CustomerNotFoundException(userId);
        }

        List<OrderEntity> orders = orderRepository.findAllByCustomerOrderByCreatedAtDesc(customer);

        List<OrderDTO> dtos = new ArrayList<>();

        orders.stream().forEach(orderEntity ->
        {
            OrderDTO orderDTO = converterAdapter.convert(orderEntity);
            dtos.add(orderDTO);
        });

        return dtos;
    }

    public List<OrderDTO> getAllOrderByStore(long storeId)
    {
        StoreEntity storeEntity = storeRepository.findById(storeId).get();
        List<OrderEntity> orders = orderRepository.findAllByStoreOrderByCreatedAtDesc(storeEntity);

        List<OrderDTO> dtos = new ArrayList<>();

        orders.stream().forEach(orderEntity ->
        {
            OrderDTO orderDTO = converterAdapter.convert(orderEntity);
            dtos.add(orderDTO);
        });

        return dtos;
    }

    public OrderDTO getOrderById(long orderId)
    {
        OrderEntity orderEntity = orderRepository.findById(orderId).get();

        OrderDTO orderDTO = converterAdapter.convert(orderEntity);

        return orderDTO;
    }

    public void toCompleted(OrderEntity orderEntity)
    {
        orderEntity.setOrderStatus(OrderStatus.COMPLETED);
        orderRepository.save(orderEntity);
    }

    public void toPrepairing(long orderId)
    {
        String processName = processNameGeneratorService.generateForOrder(orderId);
        String key = Constants.REQUIREMENT_STORE_RESPONSE_KEY;

        requirementService.provideRequirement(key, "OK", processName);
    }

    public void toReady(long orderId)
    {
        String processName = processNameGeneratorService.generateForOrder(orderId);

        requirementService.provideRequirement(Constants.REQUIREMENT_STORE_RESPONSE_FOR_READY_KEY, "OK", processName);
    }

    public void toDelivered(long orderId)
    {
        String processName = processNameGeneratorService.generateForOrder(orderId);

        requirementService.provideRequirement(Constants.REQUIREMENT_STORE_RESPONSE_FOR_DELIVERED_KEY, "OK", processName);
    }

    public void createCancelRequest(CreateCancelRequestDTO createCancelRequestDTO)
    {
        OrderEntity orderEntity = orderRepository.findById(createCancelRequestDTO.getOrderId()).get();
        CancelRequestEntity cancelRequestEntity = new CancelRequestEntity();
        cancelRequestEntity.setOrder(orderEntity);

        cancelRequestEntity = cancelRequestRepository.save(cancelRequestEntity);

        processService.createProcess(String.valueOf(cancelRequestEntity.getId()), CANCEL_REQUEST_CREATED_PATH_ID, Parameter.by(Constants.CANCEL_REQUEST_PARAMETER, String.valueOf(cancelRequestEntity.getId())));
    }
}
