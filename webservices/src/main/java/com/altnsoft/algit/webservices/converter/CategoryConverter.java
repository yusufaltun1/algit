package com.altnsoft.algit.webservices.converter;

import com.altnsft.algit.repositories.entities.CategoryEntity;
import com.altnsft.algit.repositories.entities.MenuEntity;
import com.altnsft.algit.repositories.entities.ProductEntity;
import com.altnsft.algit.repositories.repository.MenuRepository;
import com.altnsft.algit.repositories.repository.ProductRepository;
import com.altnsoft.algit.webservices.adapter.ConverterAdapter;
import com.altnsft.algit.models.dtos.CategoryDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 3.07.2018.
 */
@Component
public class CategoryConverter  implements Converter<CategoryEntity, CategoryDTO>
{
    @Autowired
    private ConverterAdapter converterAdapter;

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private ProductRepository productRepository;

    @Override
    public CategoryDTO convert(CategoryEntity entity)
    {
        CategoryDTO categoryDTO = new CategoryDTO();
        categoryDTO.setId(entity.getId());
        categoryDTO.setName(entity.getName());
        categoryDTO.setText(entity.getText());
        categoryDTO.setMenus(new ArrayList<>());
        categoryDTO.setProducts(new ArrayList<>());

        List<MenuEntity> menus = menuRepository.findAllByCategoryEntity(entity);
        menus.stream().forEach(menu -> {
            categoryDTO.getMenus().add(converterAdapter.convert(menu));
        });

        List<ProductEntity> products = productRepository.findAllByCategoryEntity(entity);

        products.stream().forEach(product -> {
            categoryDTO.getProducts().add(converterAdapter.convert(product));
        });

        return categoryDTO;
    }

    @Override
    public CategoryEntity reConvert(CategoryDTO categoryDTO) {
        return null;
    }
}
