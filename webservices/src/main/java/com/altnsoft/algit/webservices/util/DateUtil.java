package com.altnsoft.algit.webservices.util;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by yusufaltun on 15.07.2018.
 */
public class DateUtil
{
    public static String formatDate(Date date)
    {
        SimpleDateFormat formatter = new SimpleDateFormat("dd-MM-yyyy HH:mm");
        return formatter.format(date);
    }
}
