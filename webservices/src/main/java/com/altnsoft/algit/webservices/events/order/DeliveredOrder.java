package com.altnsoft.algit.webservices.events.order;

import com.altnsft.algit.processes.thejava.events.EventListener;
import com.altnsft.algit.processes.thejava.events.EventSource;
import com.altnsft.algit.repositories.entities.OrderEntity;
import com.altnsoft.algit.webservices.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by yusufaltun on 21.07.2018.
 */
@Component
public class DeliveredOrder implements EventListener
{

    @Autowired
    private OrderService orderService;

    @Override
    public String getBeanName() {
        return "deliveredOrder";
    }

    @Override
    public String perform(EventSource eventSource) {
        orderService.toCompleted((OrderEntity) eventSource.get("order_id"));

        return "OK";
    }
}
