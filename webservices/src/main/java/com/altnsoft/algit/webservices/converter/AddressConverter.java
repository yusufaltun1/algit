package com.altnsoft.algit.webservices.converter;

import com.altnsft.algit.models.dtos.AddressDTO;
import com.altnsft.algit.repositories.entities.AddressEntity;
import org.springframework.stereotype.Component;

/**
 * Created by yusufaltun on 12.09.2018.
 */
@Component
public class AddressConverter implements Converter<AddressEntity, AddressDTO>
{
    @Override
    public AddressDTO convert(AddressEntity addressEntity)
    {
        AddressDTO addressDTO = new AddressDTO();
        addressDTO.setAddress(addressEntity.getAddress());
        addressDTO.setCity(addressEntity.getCity());
        addressDTO.setPhone(addressEntity.getPhone());
        addressDTO.setId(addressEntity.getId());
        addressDTO.setFirstName(addressEntity.getFirstName());
        addressDTO.setLastName(addressEntity.getLastName());
        addressDTO.setZipCode(addressEntity.getZipCode());
        addressDTO.setId(addressEntity.getId());
        return addressDTO;
    }

    @Override
    public AddressEntity reConvert(AddressDTO addressDTO) {
        return null;
    }
}
