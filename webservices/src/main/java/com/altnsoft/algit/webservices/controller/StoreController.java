package com.altnsoft.algit.webservices.controller;

import com.altnsft.algit.models.dtos.CommentDTO;
import com.altnsft.algit.models.dtos.StoreDTO;
import com.altnsft.algit.models.dtos.GetStoresRequestData;
import com.altnsft.algit.models.dtos.StoreWrapper;
import com.altnsoft.algit.webservices.service.StoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by yusufaltun on 14.06.2018.
 */
@RestController
@RequestMapping("/store")
public class StoreController
{
    @Autowired
    private StoreService storeService;

    @GetMapping("/{store-id}")
    public ResponseEntity<StoreDTO> getStoreById(@PathVariable("store-id") int storeId)
    {
        StoreDTO dto = storeService.getStore(storeId);
        return ResponseEntity.ok(dto);
    }

    @PostMapping("/by/distance")
    public ResponseEntity<StoreWrapper> getStoresByDistance(@RequestBody GetStoresRequestData storesRequestData)
    {
        StoreWrapper storeWrapper = new StoreWrapper();
        storeWrapper.setStores(storeService.getStoresByDistance(storesRequestData));
        return ResponseEntity.ok(storeWrapper);
    }

    @GetMapping("/update/{store-id}")
    public void update(@PathVariable("store-id") int storeId)
    {
        storeService.updateStoreOnElasticSearch(storeId);
    }

    @GetMapping("/update-all")
    public void update()
    {
        storeService.updateAllStoresToElasticSearch();;
    }

    @PostMapping("/add-comment")
    public void getStoresByDistance(@RequestBody CommentDTO commentDTO)
    {
        storeService.addComment(commentDTO);
    }
}
