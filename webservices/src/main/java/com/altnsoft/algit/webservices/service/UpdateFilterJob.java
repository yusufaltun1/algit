package com.altnsoft.algit.webservices.service;

import com.altnsft.algit.repositories.entities.FoodCategoryEntity;
import com.altnsft.algit.repositories.entities.FoodCategoryToStoreEntity;
import com.altnsft.algit.repositories.entities.MenuEntity;
import com.altnsft.algit.repositories.entities.StoreEntity;
import com.altnsft.algit.repositories.repository.FoodCategoryRepository;
import com.altnsft.algit.repositories.repository.FoodCategoryToStoreRepository;
import com.altnsft.algit.repositories.repository.MenuRepository;
import com.altnsft.algit.repositories.repository.StoreRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import javax.persistence.EntityNotFoundException;
import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by yusufaltun on 20.08.2018.
 */
@Component
@Transactional
public class UpdateFilterJob
{
    @Autowired
    private FoodCategoryRepository foodCategoryRepository;

    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private MenuRepository menuRepository;

    @Autowired
    private FoodCategoryToStoreRepository foodCategoryToStoreRepository;

    @Scheduled(fixedDelay = 1000, initialDelay = 1000)
    public void scheduleTaskUsingCronExpression()
    {
        Iterator<StoreEntity> stores = storeRepository.findAll().iterator();

        List<FoodCategoryToStoreEntity> foodCategories = new ArrayList<>();
        Iterator<FoodCategoryToStoreEntity> foodCategoryToStoreItr = foodCategoryToStoreRepository.findAll().iterator();
        foodCategoryToStoreItr.forEachRemaining(foodCategories::add);

        while(stores.hasNext())
        {
            StoreEntity storeEntity = stores.next();
            List<MenuEntity> menus = menuRepository.findAllByStoreEntity(storeEntity);

            for (MenuEntity menu : menus)
            {
                try
                {
                    FoodCategoryEntity foodCategory = menu.getFoodCategory();
                    if(foodCategory != null && foodCategory.getId().longValue() != 0)
                    {
                        if(foodCategoryIsNotExist(foodCategories, foodCategory, storeEntity))
                        {
                            FoodCategoryToStoreEntity foodCategoryToStore = new FoodCategoryToStoreEntity();
                            foodCategoryToStore.setFoodCategory(foodCategory);
                            foodCategoryToStore.setStore(storeEntity);
                            foodCategoryToStoreRepository.save(foodCategoryToStore);
                        }
                    }
                }catch (EntityNotFoundException e)
                {
                    e.printStackTrace();
                }
            }
        }

    }

    private boolean foodCategoryIsNotExist(List<FoodCategoryToStoreEntity> foodCategories, FoodCategoryEntity foodCategory, StoreEntity storeEntity)
    {
        for(FoodCategoryToStoreEntity entity : foodCategories)
        {
            if(entity.getFoodCategory().getId().longValue() == foodCategory.getId().longValue())
            {
                if(entity.getStore().getId().longValue() == storeEntity.getId().longValue())
                {
                    return false;
                }
            }
        }

        return true;
    }
}
