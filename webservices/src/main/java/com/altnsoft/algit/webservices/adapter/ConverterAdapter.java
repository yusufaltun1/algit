package com.altnsoft.algit.webservices.adapter;

import com.altnsft.algit.repositories.entities.*;
import com.altnsoft.algit.webservices.converter.*;
import com.altnsft.algit.models.dtos.Dto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;

/**
 * Created by yusufaltun on 14.06.2018.
 */
@Component
public class ConverterAdapter
{
    @Autowired
    private CartConverter cartConverter;

    @Autowired
    private CartEntryConverter cartEntryConverter;

    @Autowired
    private ProductConverter productConverter;

    @Autowired
    private CustomerConverter customerConverter;

    @Autowired
    private ProductOptionConverter productOptionConverter;

    @Autowired
    private ProductOptionValueConverter productOptionValueConverter;

    @Autowired
    private MenuConverter menuConverter;

    @Autowired
    private StoreConverter storeConverter;

    @Autowired
    private MenuItemConverter menuItemConverter;

    @Autowired
    private SelectedOptionConverter selectedOptionConverter;

    @Autowired
    private StoreInfoConverter storeInfoConverter;

    @Autowired
    private CategoryConverter categoryConverter;

    @Autowired
    private OrderConverter orderConverter;

    @Autowired
    private CommentConverter commentConverter;

    @Autowired
    private FilteredCategoryConverter filteredCategoryConverter;

    @Autowired
    private StoreAvailableTimesConverter storeAvailableTimesConverter;

    @Autowired
    private AddressConverter addressConverter;

    private static final HashMap<Class<? extends BaseEntity>, Converter> entityToConverter = new HashMap<>();


    public Converter get(BaseEntity baseEntity)
    {
        if(entityToConverter.containsKey(baseEntity.getClz()))
        {
            return entityToConverter.get(baseEntity.getClz());
        }

        return null;
    }

    public <RETURN_TYPE extends Dto> RETURN_TYPE convert(BaseEntity entity)
    {
        return (RETURN_TYPE) get(entity).convert(entity);
    }

    @PostConstruct
    public void initMethod()
    {
        entityToConverter.put(CartEntity.class, cartConverter);
        entityToConverter.put(CartEntryEntity.class, cartEntryConverter);
        entityToConverter.put(ProductEntity.class, productConverter);
        entityToConverter.put(CustomerEntity.class, customerConverter);
        entityToConverter.put(ProductOptionEntity.class, productOptionConverter);
        entityToConverter.put(ProductOptionValueEntity.class, productOptionValueConverter);
        entityToConverter.put(MenuItemEntity.class, menuItemConverter);
        entityToConverter.put(MenuEntity.class, menuConverter);
        entityToConverter.put(StoreEntity.class, storeConverter);
        entityToConverter.put(SelectedOptionEntity.class, selectedOptionConverter);
        entityToConverter.put(StoreInfoEntity.class, storeInfoConverter);
        entityToConverter.put(CategoryEntity.class, categoryConverter);/**/
        entityToConverter.put(OrderEntity.class, orderConverter);
        entityToConverter.put(CommentsEntity.class, commentConverter);
        entityToConverter.put(FoodCategoryEntity.class, filteredCategoryConverter);
        entityToConverter.put(StoreAvailableTimesEntity.class, storeAvailableTimesConverter);
        entityToConverter.put(AddressEntity.class, addressConverter);
    }
}
