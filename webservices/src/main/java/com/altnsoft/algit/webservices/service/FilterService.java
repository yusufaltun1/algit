package com.altnsoft.algit.webservices.service;

import com.altnsft.algit.models.dtos.FilteredCategoryDTO;
import com.altnsft.algit.repositories.entities.FoodCategoryEntity;
import com.altnsft.algit.repositories.repository.FoodCategoryRepository;
import com.altnsoft.algit.webservices.adapter.ConverterAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by yusufaltun on 20.08.2018.
 */
@Service
public class FilterService
{
    @Autowired
    private FoodCategoryRepository foodCategoryRepository;

    @Autowired
    private ConverterAdapter converterAdapter;

    public List<FilteredCategoryDTO> getAllCategories()
    {
        List<FilteredCategoryDTO> categories = new ArrayList<>();

        Iterator<FoodCategoryEntity> entityIterator = foodCategoryRepository.findAll().iterator();
        while (entityIterator.hasNext())
        {
            FoodCategoryEntity foodCategoryEntity = entityIterator.next();
            FilteredCategoryDTO dto = converterAdapter.convert(foodCategoryEntity);
            categories.add(dto);
        }

        return categories;
    }
}
