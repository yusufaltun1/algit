package com.altnsoft.algit.webservices.converter;

import com.altnsft.algit.models.dtos.FilteredCategoryDTO;
import com.altnsft.algit.repositories.entities.FoodCategoryEntity;
import org.springframework.stereotype.Component;

/**
 * Created by yusufaltun on 20.08.2018.
 */
@Component
public class FilteredCategoryConverter implements Converter<FoodCategoryEntity,FilteredCategoryDTO> {

    @Override
    public FilteredCategoryDTO convert(FoodCategoryEntity foodCategoryEntity) {
        FilteredCategoryDTO filteredCategoryDTO = new FilteredCategoryDTO();
        filteredCategoryDTO.setId(foodCategoryEntity.getId());
        filteredCategoryDTO.setName(foodCategoryEntity.getName());

        return filteredCategoryDTO;
    }

    @Override
    public FoodCategoryEntity reConvert(FilteredCategoryDTO filteredCategoryDTO) {
        return null;
    }
}
