package com.altnsoft.algit.webservices.converter;

import com.altnsft.algit.models.dtos.Day;
import com.altnsft.algit.models.dtos.StoreAvailableTimesDTO;
import com.altnsft.algit.repositories.entities.StoreAvailableTimesEntity;
import org.springframework.stereotype.Component;

/**
 * Created by yusufaltun on 26.08.2018.
 */
@Component
public class StoreAvailableTimesConverter implements Converter<StoreAvailableTimesEntity, StoreAvailableTimesDTO>
{
    @Override
    public StoreAvailableTimesDTO convert(StoreAvailableTimesEntity storeAvailableTimes) {

        Day monday = createDay("Pazartesi", storeAvailableTimes.getMonday());
        Day tuesday = createDay("Sali", storeAvailableTimes.getTuesday());
        Day wednesday = createDay("Carsamba", storeAvailableTimes.getWednesday());
        Day thursday = createDay("Persembe", storeAvailableTimes.getThursday());
        Day friday = createDay("Cuma", storeAvailableTimes.getFriday());
        Day saturday = createDay("Cumartesi", storeAvailableTimes.getSaturday());
        Day sunday = createDay("Pazar", storeAvailableTimes.getSunday());

        StoreAvailableTimesDTO storeAvailableTimesDTO = new StoreAvailableTimesDTO();
        storeAvailableTimesDTO.setMonday(monday);
        storeAvailableTimesDTO.setTuesday(tuesday);
        storeAvailableTimesDTO.setWednesday(wednesday);
        storeAvailableTimesDTO.setThursday(thursday);
        storeAvailableTimesDTO.setFriday(friday);
        storeAvailableTimesDTO.setSaturday(saturday);
        storeAvailableTimesDTO.setSunday(sunday);

        return storeAvailableTimesDTO;
    }

    private Day createDay(String dayName, String times)
    {
        Day day = new Day();
        day.setDayName(dayName);
        day.setOpenTime(times.split("-")[0]);
        day.setCloseTime(times.split("-")[1]);
        return day;
    }
    @Override
    public StoreAvailableTimesEntity reConvert(StoreAvailableTimesDTO storeAvailableTimesDTO) {
        return null;
    }
}
