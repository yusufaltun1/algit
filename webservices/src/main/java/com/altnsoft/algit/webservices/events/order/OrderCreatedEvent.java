package com.altnsoft.algit.webservices.events.order;

import com.altnsft.algit.processes.thejava.events.EventListener;
import com.altnsft.algit.processes.thejava.events.EventSource;
import com.altnsft.algit.repositories.entities.OrderEntity;
import com.pusher.rest.Pusher;
import org.springframework.stereotype.Component;

import java.util.Collections;

/**
 * Created by yusufaltun on 21.07.2018.
 */
@Component
public class OrderCreatedEvent implements EventListener
{
    @Override
    public String getBeanName() {
        return "orderCreatedEvent";
    }

    @Override
    public String perform(EventSource eventSource)
    {
        OrderEntity orderEntity = (OrderEntity) eventSource.get("order_id");
        Pusher pusher = new Pusher("589702", "d560bdd37ff8c5617bbb", "d9f6b2e982f6b9a0d73c");
        pusher.setCluster("eu");
        pusher.setEncrypted(true);

        pusher.trigger("store-channel-"+ orderEntity.getStore().getId(), "my-event", Collections.singletonMap("message", "Bir yeni siparisiniz bulunmaktadir."));

        return "OK";
    }
}
