package com.altnsoft.algit.webservices.service;

import com.altnsft.algit.repositories.entities.MenuEntity;
import com.altnsft.algit.repositories.repository.MenuItemRepository;
import com.altnsoft.algit.webservices.adapter.ConverterAdapter;
import com.altnsft.algit.models.dtos.MenuDTO;
import com.altnsft.algit.models.dtos.MenuItemDTO;
import com.altnsft.algit.models.dtos.ProductDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by yusufaltun on 30.06.2018.
 */
@Service
public class MenuService
{
    @Autowired
    private ConverterAdapter converterAdapter;

    @Autowired
    private MenuItemRepository menuItemRepository;

    @Autowired
    private ProductService productService;

    public MenuDTO getMenu(MenuEntity entity)
    {
        MenuDTO dto = converterAdapter.convert(entity);

        return dto;
    }

}
