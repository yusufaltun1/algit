package com.altnsoft.algit.webservices.service;

import com.altnsft.algit.models.dtos.CommentDTO;
import com.altnsft.algit.models.dtos.GetStoresRequestData;
import com.altnsft.algit.processes.thejava.events.Parameter;
import com.altnsft.algit.processes.thejava.service.ProcessService;
import com.altnsft.algit.repositories.entities.*;
import com.altnsft.algit.repositories.repository.*;
import com.altnsoft.algit.webservices.adapter.ConverterAdapter;
import com.altnsft.algit.models.dtos.StoreDTO;
import com.altnsoft.algit.webservices.cache.Cache;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 30.06.2018.
 */
@Service
public class StoreService
{

    private static final long STORE_UPDATED_PATH_ID = 2;


    @Autowired
    private StoreRepository storeRepository;

    @Autowired
    private ConverterAdapter converterAdapter;

    @Autowired
    private StoreInfoRepository storeInfoRepository;

//    @Autowired
//    private ElStoreRepository elStoreRepository;

    @Autowired
    private ProcessService processService;

    @Autowired
    private CustomerRepository customerRepository;

    @Autowired
    private Cache cache;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private FoodCategoryToStoreRepository foodCategoryToStoreRepository;

    public StoreDTO getStore(int storeId)
    {
        //return elStoreRepository.findByStoreId(storeId);

        return cache.get(storeId);
    }

    public List<StoreDTO> getStoresByDistance(GetStoresRequestData data)
    {
        List<StoreInfoEntity> stores = storeInfoRepository.getStoreByNearby(data.getLat(), data.getLng(), data.getMin(), data.getMax());
        List<StoreInfoEntity> result = new ArrayList<>();
        List<FoodCategoryEntity> foodCategories = new ArrayList<>();

        for (String id : data.getSelectedCategories().split(","))
        {
            FoodCategoryEntity foodCategoryEntity = new FoodCategoryEntity();
            foodCategoryEntity.setId(Long.valueOf(id));
            foodCategories.add(foodCategoryEntity);
        }

        List<FoodCategoryToStoreEntity> foodCategoryToStores = foodCategoryToStoreRepository.findAllByFoodCategoryIn(foodCategories);
        stores.stream().forEach(storeInfo -> {
            for (FoodCategoryToStoreEntity foodCategoryToStoreEntity : foodCategoryToStores)
            {
                if(storeInfo.getStore().getId().longValue() == foodCategoryToStoreEntity.getStore().getId().longValue())
                {
                    addListIfNeeded(result, storeInfo);
                }
            }
        });

        List<StoreDTO> dto = new ArrayList<>();

        result.stream().forEach(entity ->
        {
            dto.add(getStore(entity.getStore().getId().intValue()));
        });

        return dto;
    }

    private void addListIfNeeded(List<StoreInfoEntity> list, StoreInfoEntity willBeAddInfo)
    {
        boolean founded = false;
        for (StoreInfoEntity storeInfoEntity : list) {
            if(storeInfoEntity.getStore().getId().longValue() == willBeAddInfo.getStore().getId().longValue())
            {
                founded = true;
            }
        }

        if(!founded)
        {
            list.add(willBeAddInfo);
        }
    }

    public void updateStoreOnElasticSearch(int storeId)
    {
        processService.createProcess(String.valueOf(storeId), STORE_UPDATED_PATH_ID, Parameter.by("store_id", String.valueOf(storeId)));
    }

    public void addComment(CommentDTO commentDTO)
    {
        StoreEntity storeEntity = storeRepository.findById(commentDTO.getStore()).get();
        CustomerEntity entity = customerRepository.findById(commentDTO.getCustomer()).get();

        CommentsEntity commentsEntity = new CommentsEntity();
        commentsEntity.setStore(storeEntity);
        commentsEntity.setCustomer(entity);
        commentsEntity.setMessage(commentDTO.getMessage());

        commentsEntity.setCourtesyPoint(commentDTO.getCourtesyPoint());
        commentsEntity.setFoodQualityPoint(commentDTO.getFoodQualityPoint());
        commentsEntity.setPricePoint(commentDTO.getPricePoint());
        commentsEntity.setPunctualityPoint(commentDTO.getPunctualityPoint());

        commentRepository.save(commentsEntity);

        updateStoreOnElasticSearch(storeEntity.getId().intValue());
    }


    public void updateAllStoresToElasticSearch()
    {
        Iterable<StoreEntity> storeEntities = storeRepository.findAll();
        for (StoreEntity storeEntity : storeEntities)
        {
            updateStoreOnElasticSearch(storeEntity.getId().intValue());
        }
    }

    @PostConstruct
    public void setup()
    {
        Iterable<StoreEntity> stores = storeRepository.findAll();

        for(StoreEntity store : stores)
        {
            updateStoreOnElasticSearch(store.getId().intValue());
        }
    }
}
