package com.altnsoft.algit.webservices.converter;

import com.altnsft.algit.models.dtos.OrderDTO;
import com.altnsft.algit.repositories.entities.OrderEntity;
import com.altnsft.algit.repositories.entities.OrderStatus;
import com.altnsft.algit.repositories.entities.StoreInfoEntity;
import com.altnsft.algit.repositories.repository.StoreInfoRepository;
import com.altnsoft.algit.webservices.adapter.ConverterAdapter;
import com.altnsoft.algit.webservices.util.DateUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;

/**
 * Created by yusufaltun on 10.07.2018.
 */
@Component
public class OrderConverter implements Converter<OrderEntity, OrderDTO>
{
    @Autowired
    private ConverterAdapter converterAdapter;

    @Autowired
    private StoreInfoRepository storeInfoRepository;

    @Override
    public OrderDTO convert(OrderEntity orderEntity)
    {
        OrderDTO dto = new OrderDTO();
        dto.setId(orderEntity.getId());
        dto.setStore(converterAdapter.convert(orderEntity.getStore()));
        dto.setStatus(orderEntity.getOrderStatus().name());
        dto.setCart(converterAdapter.convert(orderEntity.getCart()));

        dto.setCreationTime(DateUtil.formatDate(orderEntity.getCreatedAt()));

        StoreInfoEntity storeInfoEntity = storeInfoRepository.findByStore(orderEntity.getStore());
        Date takeAwayTime = orderEntity.getCart().getTakeAwayTime();
        Long storeMaxCancellableTimeAsMinute = storeInfoEntity.getMaxCancelTime();

        long duration  = takeAwayTime.getTime() - new Date().getTime();
        long diffInMinutes = TimeUnit.MILLISECONDS.toMinutes(duration);

        if(orderEntity.getOrderStatus().equals(OrderStatus.CANCELLED))
        {
            dto.setCancellable(false);
        }
        else
        {
            if(storeMaxCancellableTimeAsMinute > diffInMinutes)
            {
                dto.setCancellable(false);
            }
            else
            {
                dto.setCancellable(true);
            }
        }

        return dto;
    }

    @Override
    public OrderEntity reConvert(OrderDTO orderDTO) {
        return null;
    }
}
