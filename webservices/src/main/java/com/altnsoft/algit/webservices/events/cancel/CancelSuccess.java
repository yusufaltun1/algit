package com.altnsoft.algit.webservices.events.cancel;

import com.altnsft.algit.processes.thejava.events.EventListener;
import com.altnsft.algit.processes.thejava.events.EventSource;
import com.altnsft.algit.repositories.entities.CancelRequestEntity;
import com.altnsft.algit.repositories.entities.OrderEntity;
import com.altnsft.algit.repositories.entities.OrderStatus;
import com.altnsft.algit.repositories.repository.CancelRequestRepository;
import com.altnsft.algit.repositories.repository.OrderRepository;
import com.altnsoft.algit.webservices.Constants;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by yusufaltun on 16.09.2018.
 */
@Component
public class CancelSuccess implements EventListener {

    @Autowired
    private CancelRequestRepository cancelRequestRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public String getBeanName() {
        return "cancelSuccess";
    }

    @Override
    public String perform(EventSource eventSource)
    {
        long id = Long.valueOf((String)eventSource.get(Constants.CANCEL_REQUEST_PARAMETER));
        CancelRequestEntity cancelRequestEntity = cancelRequestRepository.findById(id).get();

        OrderEntity orderEntity = cancelRequestEntity.getOrder();
        orderEntity.setOrderStatus(OrderStatus.CANCELLED);
        orderRepository.save(orderEntity);

        return "OK";
    }
}
