package com.altnsoft.algit.webservices.cache;

import com.altnsft.algit.models.dtos.StoreDTO;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.HashMap;

/**
 * Created by yusufaltun on 13.08.2018.
 */
@Component
public class Cache
{
    private static final HashMap<Integer, StoreDTO> stores = new HashMap<>();

    public void update(int id, StoreDTO storeDTO)
    {
        if(stores.containsKey(id))
        {
            stores.put(id, storeDTO);
        }
        else
        {
            stores.put(id, storeDTO);
        }
    }

    public StoreDTO get(int id)
    {
        if(stores.containsKey(id))
        {
            return stores.get(id);
        }

        return null;
    }
}
