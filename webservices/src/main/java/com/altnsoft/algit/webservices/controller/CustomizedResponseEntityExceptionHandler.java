package com.altnsoft.algit.webservices.controller;

import com.altnsft.algit.models.dtos.ExceptionResponse;
import com.altnsft.algit.models.dtos.MessageCodes;
import com.altnsft.algit.models.exception.CustomerAlreadyRegisteredException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.Date;

@ControllerAdvice
@RestController
public class CustomizedResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {
//
//  @ExceptionHandler(Exception.class)
//  public final ResponseEntity<CustomerDTO> handleAllExceptions(Exception ex, WebRequest request) {
//    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(),
//        request.getDescription(false));
//    return new ResponseEntity<>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
//  }

  @ExceptionHandler(CustomerAlreadyRegisteredException.class)
  public final ResponseEntity<ExceptionResponse> handleUserNotFoundException(CustomerAlreadyRegisteredException ex, WebRequest request)
  {
    ExceptionResponse exceptionResponse = new ExceptionResponse(new Date(), ex.getMessage(),
            MessageCodes.CUSTOMER_ALREADY_REGISTERED);
    return new ResponseEntity<>(exceptionResponse, HttpStatus.INTERNAL_SERVER_ERROR);
  }

}