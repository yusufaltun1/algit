package com.altnsoft.algit.webservices.converter;

import com.altnsft.algit.models.dtos.AddressDTO;
import com.altnsft.algit.repositories.entities.AddressEntity;
import com.altnsft.algit.repositories.entities.CustomerEntity;
import com.altnsft.algit.models.dtos.CustomerDTO;
import com.altnsft.algit.repositories.repository.AddressRepository;
import com.altnsoft.algit.webservices.adapter.ConverterAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.Date;

/**
 * Created by yusufaltun on 25.06.2018.
 */
@Component
public class CustomerConverter implements Converter<CustomerEntity, CustomerDTO>
{
    @Autowired
    private AddressRepository addressRepository;

    @Autowired
    private ConverterAdapter converterAdapter;

    @Override
    public CustomerDTO convert(CustomerEntity customerEntity)
    {
        CustomerDTO dto = new CustomerDTO();
        dto.setActive(customerEntity.getActive());
        dto.setEmail(customerEntity.getEmail());
        dto.setId(customerEntity.getId().intValue());
        dto.setLastName(customerEntity.getLastName());
        dto.setName(customerEntity.getName());
        dto.setPhoneNumber(customerEntity.getPhoneNumber());
        dto.setRegistrationDate(customerEntity.getRegistrationDate());
        dto.setLastLoginDate(customerEntity.getLastLoginDate());

        AddressEntity addressEntity = addressRepository.findByCustomer(customerEntity);
        if(addressEntity != null)
        {
            AddressDTO addressDTO = converterAdapter.convert(addressEntity);
            dto.setAddressDTO(addressDTO);
        }

        return dto;
    }

    @Override
    public CustomerEntity reConvert(CustomerDTO customerDTO)
    {
        CustomerEntity entity = new CustomerEntity();
        entity.setActive(customerDTO.getActive());
        entity.setEmail(customerDTO.getEmail());
        if(customerDTO.getId() != null)
        {
            entity.setId(Long.valueOf(customerDTO.getId()));
        }

        entity.setLastName(customerDTO.getLastName());
        entity.setName(customerDTO.getName());
        entity.setPhoneNumber(customerDTO.getPhoneNumber());
        entity.setPassword(customerDTO.getPassword());
        entity.setRegistrationDate(new Date());
        return entity;
    }
}
