package com.altnsoft.algit.webservices;

import java.time.Instant;

/**
 * Created by yusufaltun on 4.09.2018.
 */
public class Super
{

    public Super()
    {
        ovverrideMe();
    }

    public void ovverrideMe()
    {

    }
}

final class Sub extends Super
{
    private final Instant instant;

    Sub()
    {
        instant = Instant.now();
    }

    @Override
    public void ovverrideMe() {
        System.out.println(instant.toString());
    }

    public static void main(String args[])
    {
        Sub s = new Sub();
        s.ovverrideMe();
    }
}
