package com.altnsoft.algit.webservices.converter;

import com.altnsft.algit.repositories.entities.ProductEntity;
import com.altnsft.algit.repositories.entities.ProductOptionEntity;
import com.altnsft.algit.repositories.repository.ProductOptionRepository;
import com.altnsft.algit.repositories.repository.ProductOptionValueRepository;
import com.altnsoft.algit.webservices.adapter.ConverterAdapter;
import com.altnsft.algit.models.dtos.ProductDTO;
import com.altnsft.algit.models.dtos.ProductOptionDTO;
import com.altnsft.algit.models.dtos.ProductOptionValueDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 14.06.2018.
 */
@Component
public class ProductConverter implements Converter<ProductEntity, ProductDTO> {

    @Autowired
    private ProductOptionRepository productOptionRepository;

    @Autowired
    private ProductOptionValueRepository productOptionValueRepository;

    @Autowired
    private ConverterAdapter converterAdapter;

    @Override
    public ProductDTO convert(ProductEntity productEntity)
    {
        ProductDTO dto = new ProductDTO();
        dto.setDescription(productEntity.getDescription());
        dto.setId(productEntity.getId());
        dto.setPrice(productEntity.getPrice().doubleValue());
        dto.setProductCode(productEntity.getCode());
        dto.setTitle(productEntity.getTitle());
        dto.setOptions(new ArrayList<>());

        List<ProductOptionEntity> productOptions = productOptionRepository.findAllByProductEntity(productEntity);

        if(!CollectionUtils.isEmpty(productOptions))
        {
            dto.setOptions(new ArrayList<>());

            productOptions.stream().forEach(option ->
            {
                ProductOptionDTO optionDTO = converterAdapter.convert(option);
                dto.getOptions().add(optionDTO);
            });
        }

        return dto;
    }

    @Override
    public ProductEntity reConvert(ProductDTO productDTO) {
        return null;
    }
}
