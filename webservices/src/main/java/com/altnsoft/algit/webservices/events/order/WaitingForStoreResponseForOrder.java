package com.altnsoft.algit.webservices.events.order;

import com.altnsft.algit.processes.thejava.events.EventListener;
import com.altnsft.algit.processes.thejava.events.EventSource;
import com.altnsft.algit.repositories.entities.OrderEntity;
import com.altnsft.algit.repositories.entities.OrderStatus;
import com.altnsft.algit.repositories.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

/**
 * Created by yusufaltun on 21.07.2018.
 */
@Component
public class WaitingForStoreResponseForOrder implements EventListener {

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public String getBeanName()
    {
        return "waitingForStoreResponseForOrder";
    }

    @Override
    public String perform(EventSource eventSource)
    {
        OrderEntity orderEntity = (OrderEntity) eventSource.get("order_id");
        orderEntity.setOrderStatus(OrderStatus.PREPAIRING);
        orderRepository.save(orderEntity);

        return "OK";
    }
}
