package com.altnsoft.algit.webservices.converter;

import com.altnsft.algit.repositories.entities.MenuEntity;
import com.altnsft.algit.repositories.entities.MenuItemEntity;
import com.altnsft.algit.repositories.entities.ProductEntity;
import com.altnsft.algit.repositories.repository.MenuItemRepository;
import com.altnsoft.algit.webservices.adapter.ConverterAdapter;
import com.altnsft.algit.models.dtos.MenuDTO;
import com.altnsft.algit.models.dtos.MenuItemDTO;
import com.altnsft.algit.models.dtos.ProductDTO;
import com.altnsoft.algit.webservices.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 14.06.2018.
 */
@Component
public class MenuConverter implements Converter<MenuEntity, MenuDTO>
{
    @Autowired
    private MenuItemRepository menuItemRepository;

    @Autowired
    private ConverterAdapter converterAdapter;

    @Autowired
    private ProductService productService;

    @Override
    public MenuDTO convert(MenuEntity entity)
    {
        MenuDTO dto = new MenuDTO();
        dto.setTitle(entity.getTitle());
        dto.setName(entity.getName());
        dto.setPrice(entity.getPrice().doubleValue());
        dto.setDescription(entity.getDescription());
        dto.setItems(new ArrayList<>());
        dto.setId(entity.getId().intValue());
        dto.setOptions(new ArrayList<>());
        dto.setMaxMakeTime(entity.getMaxMakeTime());
        List<MenuItemEntity> menuItems = menuItemRepository.findAllByMenuEntity(entity);

        menuItems.stream().forEach(item ->
        {
            MenuItemDTO itemDTO = converterAdapter.convert(item);
            if(item.getProductEntity() != null)
            {
                ProductEntity productEntity = item.getProductEntity();
                ProductDTO productDTO = productService.getProduct(productEntity);
                itemDTO.setProduct(productDTO);
            }
            dto.getItems().add(itemDTO);
        });

        dto.getItems().stream().forEach(item ->{
            dto.getOptions().addAll(item.getProduct().getOptions());
        });

        dto.setCategoryName(entity.getCategoryEntity().getName());
        return dto;

    }

    @Override
    public MenuEntity reConvert(MenuDTO menuSearchDTO) {
        return null;
    }
}
