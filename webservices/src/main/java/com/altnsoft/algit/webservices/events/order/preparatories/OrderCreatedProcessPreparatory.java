package com.altnsoft.algit.webservices.events.order.preparatories;

import com.altnsft.algit.processes.thejava.entity.ParameterEntity;
import com.altnsft.algit.processes.thejava.events.Parameter;
import com.altnsft.algit.processes.thejava.events.Preparatory;
import com.altnsft.algit.repositories.entities.OrderEntity;
import com.altnsft.algit.repositories.repository.OrderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 21.07.2018.
 */
@Component
public class OrderCreatedProcessPreparatory implements Preparatory{

    @Autowired
    private OrderRepository orderRepository;

    @Override
    public List<Parameter> prepare(List<ParameterEntity> parametersEntities)
    {
        List<Parameter> parameters = new ArrayList<>();
        String orderId = parametersEntities.get(0).getValue();
        OrderEntity orderEntity = orderRepository.findById(Long.valueOf(orderId)).get();

        parameters.add(Parameter.by("order_id", orderEntity));

        return parameters;
    }
}
