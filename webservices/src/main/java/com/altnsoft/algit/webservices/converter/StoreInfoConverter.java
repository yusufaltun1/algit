package com.altnsoft.algit.webservices.converter;

import com.altnsft.algit.repositories.entities.StoreInfoEntity;
import com.altnsft.algit.models.dtos.StoreInfoDTO;
import org.springframework.stereotype.Component;

/**
 * Created by yusufaltun on 2.07.2018.
 */
@Component
public class StoreInfoConverter implements Converter<StoreInfoEntity, StoreInfoDTO>
{
    @Override
    public StoreInfoDTO convert(StoreInfoEntity entity) {
        StoreInfoDTO dto = new StoreInfoDTO();
        dto.setAddress(entity.getAddress());
        dto.setId(entity.getId());
        dto.setImage(entity.getImage());

        dto.setMapLat(String.valueOf(entity.getMapLat()));
        dto.setMapLng(String.valueOf(entity.getMapLng()));
        dto.setDescription(entity.getDescription());

        return dto;
    }

    @Override
    public StoreInfoEntity reConvert(StoreInfoDTO storeInfoDTO) {
        return null;
    }
}
