package com.altnsoft.algit.webservices.controller;

import com.altnsft.algit.models.dtos.ProductDTO;
import com.altnsft.algit.models.dtos.ProductSearchDTO;
import com.altnsoft.algit.webservices.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by yusufaltun on 14.06.2018.
 */
@RestController
@RequestMapping("/products")
public class ProductController
{
    @Autowired
    private ProductService productService;

    @GetMapping("/{product-id}")
    public ResponseEntity<ProductDTO> getProductById(@PathVariable("product-id") int productId)
    {
        return ResponseEntity.ok(productService.getProduct(productId));
    }

    @GetMapping("/store/{store-id}")
    public ResponseEntity<List<ProductDTO>> getProductByStoreId(@PathVariable("store-id") int storeId)
    {
        return ResponseEntity.ok(productService.getProductsByStore(storeId));
    }

    @PostMapping("/search")
    public ResponseEntity<List<ProductDTO>> searchProduct(@RequestBody ProductSearchDTO dto)
    {
        return ResponseEntity.ok(productService.searchProduct(dto));
    }
}
