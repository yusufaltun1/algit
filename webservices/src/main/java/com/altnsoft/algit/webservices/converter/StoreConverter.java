package com.altnsoft.algit.webservices.converter;

import com.altnsft.algit.models.dtos.*;
import com.altnsft.algit.repositories.entities.*;
import com.altnsft.algit.repositories.repository.CategoryRepository;
import com.altnsft.algit.repositories.repository.CommentRepository;
import com.altnsft.algit.repositories.repository.StoreAvailableTimeRepository;
import com.altnsft.algit.repositories.repository.StoreInfoRepository;
import com.altnsoft.algit.webservices.adapter.ConverterAdapter;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by yusufaltun on 30.06.2018.
 */
@Component
public class StoreConverter implements Converter<StoreEntity, StoreDTO>
{
    @Autowired
    private StoreInfoRepository storeInfoRepository;

    @Autowired
    private ConverterAdapter converterAdapter;

    @Autowired
    private CategoryRepository categoryRepository;

    @Autowired
    private CommentRepository commentRepository;

    @Autowired
    private StoreAvailableTimeRepository storeAvailableTimeRepository;

    @Override
    public StoreDTO convert(StoreEntity entity)
    {
        StoreDTO dto = new StoreDTO();
        dto.setStoreId(entity.getId().intValue());
        dto.setName(entity.getName());
        dto.setCategories(new ArrayList<>());

        List<CategoryEntity> cateogories = categoryRepository.findAllByStoreEntity(entity);

        cateogories.stream().forEach(category -> {
            dto.getCategories().add(converterAdapter.convert(category));
        });

        StoreInfoEntity storeInfoEntity = storeInfoRepository.findByStore(entity);
        StoreInfoDTO storeInfoDTO = converterAdapter.convert(storeInfoEntity);
        dto.setStoreInfo(storeInfoDTO);
        dto.setComments(new ArrayList<>());
        List<CommentsEntity> commentEntities = commentRepository.findAllByStore(entity);
        commentEntities.stream().forEach(commentEntity -> {
            CommentDisplayDTO displayDTO = converterAdapter.convert(commentEntity);

            dto.getComments().add(displayDTO);
        });

        StoreReviewSummaryDTO storeReviewSummaryDTO = calculateAndGetSummary(dto);
        dto.setReviewSummary(storeReviewSummaryDTO);

        StoreAvailableTimesEntity availableTimes = storeAvailableTimeRepository.findByStoreEntity(entity);

        if(availableTimes != null)
        {
            StoreAvailableTimesDTO availableTimesDTO = converterAdapter.convert(availableTimes);
            dto.setStoreAvailableTimes(availableTimesDTO);
        }

        return dto;
    }

    private StoreReviewSummaryDTO calculateAndGetSummary(StoreDTO dto) {
        StoreReviewSummaryDTO storeReviewSummaryDTO = new StoreReviewSummaryDTO();
        int totalFoodQualityPoint = 0;
        int totalPricePoint = 0;
        int totalPunctualityPoint = 0;
        int totalCourtesyPoint = 0;
        int totalAvarage = 0;

        if(dto.getComments().size() > 0)
        {
            for (CommentDisplayDTO displayDTO : dto.getComments())
            {
                totalFoodQualityPoint += displayDTO.getFoodQualityPoint();
                totalPricePoint += displayDTO.getPricePoint();
                totalCourtesyPoint += displayDTO.getCourtesyPoint();
                totalPunctualityPoint += displayDTO.getPunctualityPoint();
            }

            totalFoodQualityPoint = totalFoodQualityPoint / dto.getComments().size();
            totalPricePoint = totalPricePoint / dto.getComments().size();
            totalPunctualityPoint = totalPunctualityPoint / dto.getComments().size();
            totalCourtesyPoint = totalCourtesyPoint / dto.getComments().size();

            totalAvarage = (totalCourtesyPoint + totalFoodQualityPoint + totalPricePoint + totalPunctualityPoint) / 4;
        }

        storeReviewSummaryDTO.setTotalAvarage(totalAvarage);
        storeReviewSummaryDTO.setCourtesyPoint(totalCourtesyPoint);
        storeReviewSummaryDTO.setFoodQualityPoint(totalFoodQualityPoint);
        storeReviewSummaryDTO.setPricePoint(totalPricePoint);
        storeReviewSummaryDTO.setPunctualityPoint(totalPunctualityPoint);

        return storeReviewSummaryDTO;
    }

    public StoreConverter() {
    }

    public StoreConverter(StoreInfoRepository storeInfoRepository, ConverterAdapter converterAdapter, CategoryRepository categoryRepository) {
        this.storeInfoRepository = storeInfoRepository;
        this.converterAdapter = converterAdapter;
        this.categoryRepository = categoryRepository;
    }



    @Override
    public StoreEntity reConvert(StoreDTO storeDTO) {
        return null;
    }
}
