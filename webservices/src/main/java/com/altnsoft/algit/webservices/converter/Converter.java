package com.altnsoft.algit.webservices.converter;

/**
 * Created by yusufaltun on 14.06.2018.
 */
public interface Converter <T, S>
{
    S convert(T t);

    T reConvert(S s);
}
