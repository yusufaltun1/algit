package com.altnsoft.algit.webservices.controller;

import com.altnsft.algit.models.dtos.FilteredCategoriesWrapper;
import com.altnsft.algit.models.dtos.FilteredCategoryDTO;
import com.altnsoft.algit.webservices.service.FilterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

/**
 * Created by yusufaltun on 20.08.2018.
 */
@RestController
@RequestMapping("/filter")
public class FilteredCategoryController
{
    @Autowired
    private FilterService filterService;

    @GetMapping("/all-category")
    public ResponseEntity<FilteredCategoriesWrapper> getAllCategories()
    {
        List<FilteredCategoryDTO> dtos = filterService.getAllCategories();
        FilteredCategoriesWrapper filteredCategoriesWrapper = new FilteredCategoriesWrapper();
        filteredCategoriesWrapper.setCategories(dtos);

        return ResponseEntity.ok(filteredCategoriesWrapper);
    }
}
