package com.altnsoft.algit.webservices.service;

import com.altnsft.algit.models.dtos.CreateAddressDTO;
import com.altnsft.algit.repositories.entities.AddressEntity;
import com.altnsft.algit.repositories.entities.CustomerEntity;
import com.altnsft.algit.repositories.repository.AddressRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * Created by yusufaltun on 16.09.2018.
 */
@Service
public class AddressService
{
    @Autowired
    private AddressRepository addressRepository;

    public void createAddress(CreateAddressDTO createAddressDTO)
    {
        CustomerEntity customerEntity = new CustomerEntity();
        customerEntity.setId(createAddressDTO.getCustomerId());

        AddressEntity addressEntity = addressRepository.findByCustomer(customerEntity);

        if(addressEntity == null)
        {
            addressEntity = new AddressEntity();
            addressEntity.setCustomer(customerEntity);
        }

        addressEntity.setAddress(createAddressDTO.getAddressDTO().getAddress());
        addressEntity.setCity(createAddressDTO.getAddressDTO().getCity());
        addressEntity.setZipCode(createAddressDTO.getAddressDTO().getZipCode());
        addressEntity.setFirstName(createAddressDTO.getAddressDTO().getFirstName());
        addressEntity.setLastName(createAddressDTO.getAddressDTO().getLastName());
        addressEntity.setPhone(createAddressDTO.getAddressDTO().getPhone());

        addressRepository.save(addressEntity);
    }
}
